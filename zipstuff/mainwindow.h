#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QDebug>
#include<QTableWidget>
#include<QCheckBox>
#include <QHeaderView>
#include <QPlainTextEdit>
#include<QFileDialog>
#include<QMessageBox>
#include<QTextStream>
#include<QAction>
#include<QMenuBar>
#include<QToolBar>
#include<QSettings>
#include<QApplication>
#include<QStatusBar>
#include<QDockWidget>
#include<QListWidget>
#include<QPushButton>
#include<QVBoxLayout>
#include<QSpinBox>
#include<QRadioButton>
#include<QComboBox>
#include<QDialog>
#include<QLineEdit>
#include<QDialogButtonBox>
#include <QLabel>
#include <QWidget>
#include <QPlainTextEdit>
#include <QtGui/QMainWindow>
#include <QtGui>
#include <QPlainTextEdit>
#include<QFileDialog>
#include<QMessageBox>
#include<QTextStream>
#include<QAction>
#include<QMenuBar>
#include<QToolBar>
#include<QSettings>
#include<QApplication>
#include<QStatusBar>
#include<QDockWidget>
#include<QListWidget>
#include<QPushButton>
#include<QVBoxLayout>
#include<QVector>
#include<QColor>

#include"variable.h"
#include"grammar.h"
#include "StringManipulation.h"

//scma related headers
#include <vector>
#include<iostream>
#include<string>
#include<cstring>


#include<stdio.h>
#include<stdlib.h>
#include<stdarg.h>
#include<memory.h>
#include<setjmp.h>

#include <utility>
#include <bitset>

#include<z3.h>


#include <scma.h>
#include <scTimer.h>
#include <scTheory.h>
#include"traverse.h" //MS
#include"checkchoice.h" //MS

using namespace std;
//

class K_mapGrid : public QWidget
{
	Q_OBJECT


	//QPainter painter;
public:
 //  MyWidget();
	//K_mapGrid();
	//~K_mapGrid();
	int x;
	int y;
	bool draw;
	int marginx;
	int marginy;
	SpecCheckVocab* spchk;
	void init();
	void updateValues();
	bool similarFormat(string a, string b);
	void updateKmap();


	string *verticalList;
	string *horizontalList;
	int **kGrid;
	vector<TheoryState*> *UndoChoiceVector;

protected:
	void paintEvent(QPaintEvent *event);
	void drawGrid(int x, int y,QPainter *p, int  tablex, int tabley);

signals:

public slots:

};

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
    MainWindow();
    ~MainWindow();
    void setSpeck(SpecCheckVocab sc);

protected:
    void closeEvent(QCloseEvent *event);

private slots:

    void UseUserVocabYClicked();
    void UseUserVocabNClicked();


    void construct();
    void newFile();
    void open();
    bool save();
    bool saveAs();
    void about();
    void documentWasModified();
    void copy();
    void cut();
    void paste();
    void refresh();
    void createTH();
    void exit();
    void createVocab();
    bool saveResult();

    void addVariableClicked();
    void addVocabClicked();
    void addGrammarClicked();
    void deleteVariableClicked();
    void deleteVocabClicked();
    void deleteGrammarClicked();
    void addConstantClicked();
    void deleteConstantClicked();

    //when deleting variable used in grammar
    void ConfirmDeleteVarN();
    void ConfirmDeleteVarY();

    void addVariableToGui(QString,QString);// adding the variable n with type t to the variables window
    void ConfirmVariableAddClicked();
    void ConfirmConstantAddClicked();
    void ConfirmVocabAddClicked();
    void ConfirmGrammarAddClicked();
    void ConfirmNewPredicateAddClicked();

    void UndoButtonClicked();
    void pushState();
    void popState();
    void updateUndoList();

    void AddNewPredicateClicked();
    void AddArgumentNewPredicateClicked();
    void AddExistingPredicateClicked();

    void AcceptButtonClicked();
    //void RejectCButtonClicked();
    //void RejectVButtonClicked();
    void RejectButtonClicked();




private:

    //scma related section
    SpecCheckVocab* spchk;
    SCTheory * th;
    checkchoice * CC;
    traverse * TT;

    int Depth;
    //


    //scma supporting functions
    void handleVariableGui(QString name, QString type, string GLN);
    QString textForType(QString s); // function that converts the text of a variable type from the gui to its corresponding text from the .th files

    //th fill gui functions
    void reloadVariables();
    void reloadVocab();
    void reloadConstant();
    void reloadGrammar();
    void reloadGrammar(SCTheory *);

    //functions used for loading a file
    void placeVarsInGUI(SCTheory* th);
    void placeGrammarInGUI(SCTheory* th);
    void PlaceConstantsInGUI(SCTheory* th);
    void PlaceVocabInGUI(SCTheory* th);

    //GUI Handeling
    void disableGuiItems();
    void enableGuiItems();

    //view sc output in gui
    void ViewDisplay(vector<string> VarName,vector<string> VarValue);


    void GenerateDisplayNameValue(vector<string>Display, vector<string>&VarName,vector<string>&VarValue);



    bool UseUserVocab; //initially set to false and change upon user request (answer)

    QPushButton *AcceptButton;
    QPushButton *RejectCButton;
    QPushButton *RejectVButton;
    QPushButton *RejectButton;

    QPushButton *UndoButton;

    QRadioButton *InjectUniversalQuantifiers;
    QRadioButton *InjectExistentialQuantifiers;

    QWidget *CentralWidget;

    QPushButton *addVariable;
    QPushButton *deleteVariable;
    QPushButton *addVocab;
    QPushButton *deleteVocab;
    QPushButton *addGrammar;
    QPushButton *deleteGrammar;
    QPushButton *addConstant;
    QPushButton *deleteConstant;
    QPushButton *saveThButton;

    QListWidget *grammarList;
    QListWidget *vocabList;
    QListWidget *GeneratedvocabList;
    //QVector<QString > *variableStringVector;
    QListWidget *variableList;
    QListWidget *constantList;

    QVector<Variable> *variables;
    QVector<Grammar> *grammars;



    QLineEdit *theoryNameField;
    QString theoryName;
    QTableWidget *variablesTable;
    QTableWidget *clausesTable;

    QVector<QVector<int> > variableInGrammarAt;




    QDialog *addVariableDialog;
    QDialog *constructUseUserVocabDialog;
    QLineEdit *VariableNameInput;
    QComboBox *VariableTypeInput;
    QRadioButton *globalVariable;
    QRadioButton *localVariable;

    QDialog *warnUponVarDeleteD;

    QDialog *addVocabDialog;
    QLineEdit *VocabInput;

    QDialog *addNewPredicateDialog;
    QLabel *PredicateArguments;
    QComboBox *VariableArgs;
    QDialog *addExistingPredicateDialog;

    QDialog *addGrammarDialog;
    QComboBox* GrammarLeftInput;
    QComboBox* GrammarRightInput;
    QComboBox* GrammarRelationInput;

    QDialog *addConstantDialog;
    QSpinBox *ConstantInput;
    QSpinBox *NumOpInput;
    QSpinBox *NumQuantifiersInput;

    QComboBox *undoList;
    K_mapGrid *k_mapgrid;


    //scma related variables
    string theoryFileName;
    string answer;
    vector<int> vocab_partials;
    vector<string> vars_partials;

    QLineEdit * FormulaArea;
    bool ConstructDone;

    vector<TheoryState*> UndoChoiceVector;
    TheoryState* stateTest;

    void PostConstruct();


    void fillVariablesTable();
    void fillClausesTableByUserVocab();
    void fillClausesTableByGeneratedVocab();
    void writeTHfile();
    void createActions();
    void createMenus();
    void createToolBars();
    void createStatusBar();
    void readSettings();
    void writeSettings();
    bool maybeSave();
    void loadFile(const QString &fileName);
    bool saveFile(const QString &fileName);
    void setCurrentFile(const QString &fileName);
    void createDockWindows();
    void deleteVariableWarning(QVector<int> warnAt, int removeAt);
    QString strippedName(const QString &fullFileName);

    void updateList(QComboBox *  list, string newThing);
    //void paintEvent(QPaintEvent*);
    //void paintEvent(QPaintEvent *event);


    QPlainTextEdit *textEdit;
    QWidget *fileEdit;
    QString curFile; //will be the threory input model

    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *viewMenu;
    QMenu *helpMenu;
    QToolBar *fileToolBar;
    QToolBar *editToolBar;
    QToolBar *refreshToolBar;
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *exitAct;
    QAction *cutAct;
    QAction *copyAct;
    QAction *pasteAct;
    QAction *refreshAct;
    QAction *constructAct;
    QAction *createTHAct;
    QAction *aboutAct;
    QAction *aboutQtAct;
    QAction *saveThAct;

    QLineEdit *NewPredicateFormulaInput;
    QLineEdit *NewPredicateNameInput;

    QPainter *painter;

};

#endif // MAINWINDOW_H
