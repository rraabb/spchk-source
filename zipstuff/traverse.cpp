#include "traverse.h"
using namespace std;

vector <vocab_value_t> traverse::firstEvaluation()
{
	reset();
	return nextEvaluation();
}
void traverse::reset()
{
	depth=0;
	int i=0;
	while(i<size)
	{
		choice[i]=vuu;
		i++;
	}
	res=false;
	allTraversed=false;
}
traverse::traverse()
{
}

traverse::traverse(SpecCheckVocab & scv)
{
	depth=0;
	int i=0;

	SCV= &scv;

	size=SCV->vocab.size();

	while(i<size)
	{
		choice.push_back(vuu);
		i++;
	}
	res=false;
	allTraversed=false;

}


void traverse::setTraverse(SpecCheckVocab & scv)
{
	depth=0;

	SCV= &scv;
	size=SCV->vocab.size();

}


vector<vocab_value_t> traverse::getEvaluation()
{
	return choice;
}

/*
vector <vocab_value_t> traverse::nextEvaluation()
{
	while(true)
	{

		if(allTraversed==true)
		{
			cout<<"done"<<endl;
			print_vector(choice);
			return choice;
		}
		if(choice[depth]==vuu)
		{
			choice[depth]=vtt;
			depth++;
		}
		else if(choice[depth]==vtt)
		{
			res=SCV->matchesUnsatCore(choice);
			if(res==true)
			{
				cout<<"SPCHK: Ignore: subtree satisfies an eliminated pattern.";
				print_vector(choice);
				//no left-side branch @ same level(choice[depth]=vff), move up
				choice[depth]==vff;

			}
			else depth++;
		}
		else if(choice[depth]==vff)
		{
			res=SCV->matchesUnsatCore(choice);
			if(res==true)
			{
				cout<<"SPCHK: Ignore: subtree satisfies an eliminated pattern.";
				print_vector(choice);
				//no left-side branch @ same level(choice[depth]=vff), move up
				traverseUp(); //after traverse up function, check allTraversed

			}
			else depth++;
		}

		cout << depth << endl;
				if(depth==choice.size())
				{
					cout << depth << endl;
					traverseUp();
					cout << depth << endl;
					//	if(allTraversed==true){ cout<<"All vocab evaluations are covered. Evaluations will restrart.\n"<<endl;}
					cout<<"choice set to: ";
					print_vector(choice);
					cout << depth << endl;
					return choice;
				}


	}
}
*/

vector <vocab_value_t> traverse::nextEvaluation()
{
	cout<< depth << endl;
	while(true)
	{
		//cout<< depth;
		//cannot put res check (matchesUnsatCore) here, since if depth=size the choice will be returned
		//+ will repeat check for same choice when coming back to the choice by traverse up. however below, the check is done only on change of choice everywhere other than in traverseup
		if(choice[depth]==vtt)
		{
			choice[depth]=vff;
			res=SCV->matchesUnsatCore(choice);
			if(res==true)
			{
				cout<<"SPCHK: Ignore: subtree satisfies an eliminated pattern.";
				print_vector(choice);
				//no left-side branch @ same level(choice[depth]=vff), move up
				traverseUp(); //after traverse up function, check allTraversed
				if(allTraversed==true)
				{
					cout<<"done aab"<<endl;
					print_vector(choice);
					return choice;
				}
			}
			else depth++;

		}

		else if(choice[depth]==vff)
		{
			traverseUp();
			if(allTraversed==true)
			{
				cout<<"done bbc"<<endl;
				print_vector(choice);
				return choice;
			}
		}
		else if(choice[depth]==vuu)
		{
			choice[depth]=vtt;
			res=SCV->matchesUnsatCore(choice);

			if(res==true)
			{
				cout<<"SPCHK: Ignore: subtree satisfies an eliminated pattern.";
				print_vector(choice);
				//move to left-side branch @ same level
				//note that reaching current level means parent not in unsat core, which means at least left or right branch 			not in unsat core. if right branch is in unsat core (res=true) => right is not. no need to check again
				choice[depth]=vff;
				res=SCV->matchesUnsatCore(choice);
				if(res==true)
							{
								cout<<"SPCHK: Ignore: subtree satisfies an eliminated pattern.";
								print_vector(choice);
								//move to left-side branch @ same level
								//note that reaching current level means parent not in unsat core, which means at least left or right branch 			not in unsat core. if right branch is in unsat core (res=true) => right is not. no need to check again
								choice[depth]=vff;

								traverseUp();
								if(allTraversed==true)
											{
												cout<<"done"<<endl;
												print_vector(choice);
												return choice;
											}
								depth--;
							}

				depth++; // MAYA BUG 1
			}
			else depth++;
		}


		if(depth==choice.size())
		{
			depth--;
			//	if(allTraversed==true){ cout<<"All vocab evaluations are covered. Evaluations will restrart.\n"<<endl;}
			cout<<"choice set to: ";
			print_vector(choice);
			return choice;
		}
	}
}



void traverse::traverseUp()
{
	choice[depth]=vuu;
	depth--;
	if(depth==-1)
	{
		allTraversed=true;
		depth=0;
	}

}


/*
traverse::traverse()
{
size=3;
depth=0;
int i=0;
while(i<size)
	{
	choice.push_back(vuu);
	i++;
	}
res=false;
allTraversed=false;

}

traverse::traverse(int Size)
{
size=Size;
depth=0;
int i=0;
while(i<size)
	{
	choice.push_back(vuu);
	i++;
	}
res=false;
allTraversed=false;

}
 */
