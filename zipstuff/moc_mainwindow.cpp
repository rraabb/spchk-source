/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_K_mapGrid[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_K_mapGrid[] = {
    "K_mapGrid\0"
};

void K_mapGrid::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData K_mapGrid::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject K_mapGrid::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_K_mapGrid,
      qt_meta_data_K_mapGrid, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &K_mapGrid::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *K_mapGrid::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *K_mapGrid::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_K_mapGrid))
        return static_cast<void*>(const_cast< K_mapGrid*>(this));
    return QWidget::qt_metacast(_clname);
}

int K_mapGrid::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      42,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      35,   11,   11,   11, 0x08,
      58,   11,   11,   11, 0x08,
      70,   11,   11,   11, 0x08,
      80,   11,   11,   11, 0x08,
      92,   11,   87,   11, 0x08,
      99,   11,   87,   11, 0x08,
     108,   11,   11,   11, 0x08,
     116,   11,   11,   11, 0x08,
     138,   11,   11,   11, 0x08,
     145,   11,   11,   11, 0x08,
     151,   11,   11,   11, 0x08,
     159,   11,   11,   11, 0x08,
     169,   11,   11,   11, 0x08,
     180,   11,   11,   11, 0x08,
     187,   11,   11,   11, 0x08,
     201,   11,   87,   11, 0x08,
     214,   11,   11,   11, 0x08,
     235,   11,   11,   11, 0x08,
     253,   11,   11,   11, 0x08,
     273,   11,   11,   11, 0x08,
     297,   11,   11,   11, 0x08,
     318,   11,   11,   11, 0x08,
     341,   11,   11,   11, 0x08,
     362,   11,   11,   11, 0x08,
     386,   11,   11,   11, 0x08,
     406,   11,   11,   11, 0x08,
     428,  426,   11,   11, 0x08,
     462,   11,   11,   11, 0x08,
     490,   11,   11,   11, 0x08,
     518,   11,   11,   11, 0x08,
     543,   11,   11,   11, 0x08,
     570,   11,   11,   11, 0x08,
     602,   11,   11,   11, 0x08,
     622,   11,   11,   11, 0x08,
     634,   11,   11,   11, 0x08,
     645,   11,   11,   11, 0x08,
     662,   11,   11,   11, 0x08,
     687,   11,   11,   11, 0x08,
     720,   11,   11,   11, 0x08,
     750,   11,   11,   11, 0x08,
     772,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0UseUserVocabYClicked()\0"
    "UseUserVocabNClicked()\0construct()\0"
    "newFile()\0open()\0bool\0save()\0saveAs()\0"
    "about()\0documentWasModified()\0copy()\0"
    "cut()\0paste()\0refresh()\0createTH()\0"
    "exit()\0createVocab()\0saveResult()\0"
    "addVariableClicked()\0addVocabClicked()\0"
    "addGrammarClicked()\0deleteVariableClicked()\0"
    "deleteVocabClicked()\0deleteGrammarClicked()\0"
    "addConstantClicked()\0deleteConstantClicked()\0"
    "ConfirmDeleteVarN()\0ConfirmDeleteVarY()\0"
    ",\0addVariableToGui(QString,QString)\0"
    "ConfirmVariableAddClicked()\0"
    "ConfirmConstantAddClicked()\0"
    "ConfirmVocabAddClicked()\0"
    "ConfirmGrammarAddClicked()\0"
    "ConfirmNewPredicateAddClicked()\0"
    "UndoButtonClicked()\0pushState()\0"
    "popState()\0updateUndoList()\0"
    "AddNewPredicateClicked()\0"
    "AddArgumentNewPredicateClicked()\0"
    "AddExistingPredicateClicked()\0"
    "AcceptButtonClicked()\0RejectButtonClicked()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->UseUserVocabYClicked(); break;
        case 1: _t->UseUserVocabNClicked(); break;
        case 2: _t->construct(); break;
        case 3: _t->newFile(); break;
        case 4: _t->open(); break;
        case 5: { bool _r = _t->save();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: { bool _r = _t->saveAs();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: _t->about(); break;
        case 8: _t->documentWasModified(); break;
        case 9: _t->copy(); break;
        case 10: _t->cut(); break;
        case 11: _t->paste(); break;
        case 12: _t->refresh(); break;
        case 13: _t->createTH(); break;
        case 14: _t->exit(); break;
        case 15: _t->createVocab(); break;
        case 16: { bool _r = _t->saveResult();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 17: _t->addVariableClicked(); break;
        case 18: _t->addVocabClicked(); break;
        case 19: _t->addGrammarClicked(); break;
        case 20: _t->deleteVariableClicked(); break;
        case 21: _t->deleteVocabClicked(); break;
        case 22: _t->deleteGrammarClicked(); break;
        case 23: _t->addConstantClicked(); break;
        case 24: _t->deleteConstantClicked(); break;
        case 25: _t->ConfirmDeleteVarN(); break;
        case 26: _t->ConfirmDeleteVarY(); break;
        case 27: _t->addVariableToGui((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 28: _t->ConfirmVariableAddClicked(); break;
        case 29: _t->ConfirmConstantAddClicked(); break;
        case 30: _t->ConfirmVocabAddClicked(); break;
        case 31: _t->ConfirmGrammarAddClicked(); break;
        case 32: _t->ConfirmNewPredicateAddClicked(); break;
        case 33: _t->UndoButtonClicked(); break;
        case 34: _t->pushState(); break;
        case 35: _t->popState(); break;
        case 36: _t->updateUndoList(); break;
        case 37: _t->AddNewPredicateClicked(); break;
        case 38: _t->AddArgumentNewPredicateClicked(); break;
        case 39: _t->AddExistingPredicateClicked(); break;
        case 40: _t->AcceptButtonClicked(); break;
        case 41: _t->RejectButtonClicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 42)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 42;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
