#include<stdio.h>
#include<stdlib.h>
#include<stdarg.h>
#include<memory.h>
#include<setjmp.h>

#include <vector>
#include<iostream>
#include<string>
#include<cstring>
#include <utility>
#include <bitset>

#include<z3.h>

#include <scma.h>
#include <scTimer.h>
#include <scTheory.h>
#include"traverse.h" //MS
#include"checkchoice.h" //MS

using namespace std;

extern const char * smtDcl[];
extern const char * smtVocab[];

extern const char * smtEquiv[];

extern int GUImain(int argc, char *argv[], SpecCheckVocab & spchk);

int spchk_main(int argc, char ** argv) {//

  SpecCheckVocab spchk;     //declare main specCheck object, of which there is only one instance.
  spchk.isGui = false;      //initializations
  spchk.th = NULL;
  spchk.quantified_formula_ans_lit = 0;
  spchk.quantified_formula = 0;

  spchk.compute_options(argc,argv);  //read command line optional

  cout << spchk.theoryFileName;    

  if (spchk.theoryFileName.empty()){  //if no theory file is given, run from dummy default hardcoded C++ declarations 
    spchk.fillDeclarations(smtDcl);
    spchk.fillVocab(smtVocab);
    spchk.fillEquiv(smtEquiv);
	} else {
    if (!spchk.readTheoryFile()){   //else read theory file and exit in case of error. updates spchk.th
      exit(-1);
    }



    SCTheory * th = spchk.th;    //pointer to the theory structure
    //th->print(cout); //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    spchk.genVocab |= th->vocabIds.empty(); //forcibly set vocabulary generation if the vocab is empty 
    //th->print(cout); //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    if (spchk.genVocab) {
    	cout << "generating vocab" << endl;
      //generate vocab from type theory
	th->injectQuantifierVars(spchk.injectLocal); //inject quantified vars
        th->generateVocab();                         //generate the vocab

        /*
        for(int i=0; i<th->generatedVocabIds.size(); i++)
                          {
                              cout << th->generatedVocabIds[i] << endl;
                          }
                          */

    } else if (spchk.injectLocal) {                  //else if vocab already exists and there are quantified vars to inject
    	cout << "injecting local" << endl;
        th->injectQuantifierVars(spchk.injectLocal); //just inject the vars
    }
    if (spchk.useQuantFree) {                        //translate quantified vars to the quantifier free versionb, a la Manna-Bradley
    	cout << "generating quant free vocab" << endl;
        th->generateQuantFreeVocab(spchk.genVocab);
    }
    if(spchk.isPropper)                     //start GUI if required
           	{
           		cout << "The File " + spchk.theoryFileName + " is a Proper file" << endl;
           		exit(-1);
           	}

    /*
    //debugging print statements
    for(int i=0; i<th->smtDeclarations.size();i++)
      {
    	cout << th->smtDeclarations[i] << endl;
      }
    for(int i=0; i<spchk.vocab.size();i++)
      {
    	cout << spchk.vocab[i] << endl;
      }
      */

    spchk.vocab = th->buildSMTFormulae(spchk.genVocab, spchk.useQuantFree); //build the vocab formulae (clauses) in SMT format

    /*
    cout<< spchk.vocab.size() << endl;

    for(int i=0; i<spchk.vocab.size();i++)
      {
	cout << spchk.vocab[i] << endl;
      }
      */

    spchk.declarations = th->smtDeclarations;  // set the declarations in spchk object
  }



//   cout<<"New Start"<<endl;
//   spchk.buildAndParseStringFormula();
//     spchk.printDeclarations();
//     spchk.assertParsedFormulae();
//     spchk.initTraversal();
//     cout<<"New END"<<endl;

   if(spchk.isGui)                     //start GUI if required
   	{
   		GUImain(argc,argv, spchk);
   		return 0;
   	}

   cout<< spchk.vocab.size() << endl;// debug

   cout << "name : ";
   cout << spchk.theoryFileName;

spchk.buildAndParseStringFormula(); //translates from SMT format to Z3 AST objects and corresponding functional symbols
spchk.printDeclarations();      //pretty prints AST formulae to visually check against originals
spchk.assertParsedFormulae(); //set up assertion-retraction system
spchk.initTraversal();      //initialize the recursive traversal structure
  cout << spchk.theoryFileName;

  cout<<"SPCHK: enumerating vocab evaluations using recursive traversal..."<<endl;
TIME_IT(spchk.traverseTime,spchk.timingFlags,TIME_SPCC,   
	spchk.traverse())   //do the traversal, and time it, including user time taken, Z3 time taken etc.

cout<<"SPCHK: the formula is:"<<endl;   //output formula {\cal F}
  cout<<Z3_ast_to_string(spchk.ctx->m_context, spchk.return_val);

spchk.printStats();   //print timing stats
spchk.espressoSimplify(); //simplifty and output the formula {\cal F}
  return 0;
}
