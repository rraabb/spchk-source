#include <QtGui/QApplication>
#include <QApplication>

#include "mainwindow.h"

int GUImain(int argc, char *argv[], SpecCheckVocab &spchkk)
{
//    Q_INIT_RESOURCE(application);
	//initialize_output();
	qDebug() << "Start QTMAin";

    QApplication app(argc, argv);
    app.setOrganizationName("AUB");
    app.setApplicationName("SpecConstruct");

    MainWindow mainWin;
    mainWin.setSpeck(spchkk);
    //SpecCheckVocab ssa = spchkk;
    //mainWin.setSpeck(ssa);

    mainWin.show();
    return app.exec();
    //int returnInt = app.exec();
   // close_output();
  //  return returnInt;
}
