//#include <QtWidgets>
#include <iostream>
#include <fstream>
#include "mainwindow.h"
#include "qstring.h"


//std::string exec(char*cmd);
/*
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
}
*/

// a method that sets the windows width for the QT panel
void SetWidth(QPlainTextEdit* edit, int nCols)
  {
  QFontMetrics m (edit -> font()) ;
  int ColWidth = m.height();
  edit ->setFixedWidth(nCols * ColWidth) ;
  }


// a method that sets the windows Height for the QT panel
void SetHeight (QPlainTextEdit* edit, int nRows)
  {
  QFontMetrics m (edit -> font()) ;
  int RowHeight = m.lineSpacing() ;
  edit -> setFixedHeight  (nRows * RowHeight) ;
  }

// sets the SpecCheckVocab pointer to the vocab of interest in the Mainwindow object
void MainWindow::setSpeck(SpecCheckVocab sc)
{
	*spchk = sc;
	loadFile(QString::fromStdString(sc.theoryFileName));
}

// the main window of the GUI is organized here
MainWindow::MainWindow()
{
    //scma related section
    th=0; // Nulling the theory pointer
    this->spchk = new SpecCheckVocab(); // // creating a new SpecCheckVocab object and assigning it to spchk
    SpecCheckVocab* scv = spchk; // creating a new SpecCheckVocab object and assigning the new spchk to it

    CC= new checkchoice(scv); // creating a checkchoice with scv and assigning it to CC

    scv->quantified_formula_ans_lit = 0; // making quantified_formula_ans_lit false for Z3

    scv->quantified_formula = 0; // making quantified_formula false for Z3

    scv->th = new SCTheory(); // creating a new Theory for th in  scv
    scv->quantified_formula_ans_lit = 0;// making quantified_formula_ans_lit false 	// redundant
    scv->quantified_formula = 0;// making quantified_formula false			// redundant

    th = scv->th;// making th a pointer to scv.th (shortcut)

    UseUserVocab=false; // boolean for using user vocab or generate one

    // creating new Widgets for the GUI
    variableList=new QListWidget(); // the variable list widget
    vocabList=new QListWidget();// the vocab list widget
    grammarList=new QListWidget();// the grammar list widget
    constantList=new QListWidget();// the constant list widget
    variables=new QVector<Variable>();// the variabls vector
    grammars=new QVector<Grammar>();// the Grammar vector
    textEdit = new QPlainTextEdit; // the box for the theory name
    textEdit->setReadOnly(true); // making the theory name box read only so the user can't write in it


    CentralWidget=new QWidget(); // a widget for the content


    // the header for the clauses table creation and values
    QStringList clausesHeaderLabel;
    clausesHeaderLabel<<"Use"<<"Clause"<<"Value"<<"Reason";// specifying table headers

    clausesTable = new QTableWidget(0, 4, this); // location of clauses table

    // resizing and formating each column in the clauses table
    clausesTable->horizontalHeader()->setResizeMode(1,QHeaderView::Stretch);
    clausesTable->horizontalHeader()->resizeSection(0,30); // (column number , size)
    clausesTable->horizontalHeader()->resizeSection(2,80);// (column number , size)
    clausesTable->horizontalHeader()->resizeSection(3,80);// (column number , size)
    clausesTable->verticalHeader()->setVisible(false); // show the header
    clausesTable->setHorizontalHeaderLabels(clausesHeaderLabel); // assigh header labes

    // header for variables table creation and values
    QStringList variablesHeaderLabel;
    variablesHeaderLabel<<"id"<<"G/L"<<"Type"<<"Name"<<"Value"<<"Reason";
    variablesTable= new QTableWidget(0, 6, this);
    variablesTable->horizontalHeader()->setResizeMode(4,QHeaderView::Stretch);// specify how the table deforms
    variablesTable->setColumnWidth(0,60);// (column number , size)
    variablesTable->setColumnWidth(1,80);// (column number , size)
    variablesTable->setColumnWidth(2,70);// (column number , size)
    variablesTable->setColumnWidth(4,60);// (column number , size)
    variablesTable->setColumnWidth(5,60);// (column number , size)
    variablesTable->setWindowTitle("Variables");
    variablesTable->verticalHeader()->setVisible(false);
    variablesTable->setHorizontalHeaderLabels(variablesHeaderLabel);
    variablesTable->resizeColumnsToContents();
    variablesTable->setVisible(true);


    // add vocab button picture and linking method that runs when clicked
    addVocab = new QPushButton (QIcon("images/add.png"),"");
    addVocab->setToolTip("Add Vocab");
    connect(addVocab, SIGNAL(clicked()), this, SLOT(addVocabClicked()));// linking
    // remove vocab button picture and linking method that runs when clicked
    deleteVocab = new QPushButton (QIcon("images/delete.png"),"");
    deleteVocab->setToolTip("Delete Selected Vocab");
    connect(deleteVocab, SIGNAL(clicked()), this, SLOT(deleteVocabClicked())); // linking


    // add variable button picture and linking method that runs when clicked
    addVariable = new QPushButton (QIcon("images/add.png"),"");
    addVariable->setToolTip("Add Variable");
    connect(addVariable, SIGNAL(clicked()), this, SLOT(addVariableClicked())); // linking

    // remove variable button picture and linking method that runs when clicked
    deleteVariable = new QPushButton (QIcon("images/delete.png"),"");
    deleteVariable->setToolTip("Delete Selected Variable");
    connect(deleteVariable, SIGNAL(clicked()), this, SLOT(deleteVariableClicked())); // linking


    // add constant button picture and linking method that runs when clicked
    addConstant= new QPushButton (QIcon("images/add.png"),"");
    addConstant->setToolTip("Add Constant");
    connect(addConstant, SIGNAL(clicked()), this, SLOT(addConstantClicked()));// linking

    // remove constant button picture and linking method that runs when clicked
    deleteConstant = new QPushButton (QIcon("images/delete.png"),"");
    deleteConstant->setToolTip("Delete Selected Constant");
    connect(deleteConstant, SIGNAL(clicked()), this, SLOT(deleteConstantClicked())); // linking


    // add grammar button picture and linking method that runs when clicked
    addGrammar = new QPushButton (QIcon("images/add.png"),"");
    addGrammar->setToolTip("Add Vocab");
    connect(addGrammar, SIGNAL(clicked()), this, SLOT(addGrammarClicked()));// linking

    // remove grammar button picture and linking method that runs when clicked
    deleteGrammar = new QPushButton (QIcon("images/save.png"),"");
    deleteGrammar->setToolTip("Save Theory Result");
    connect(deleteGrammar, SIGNAL(clicked()), this, SLOT(deleteGrammarClicked())); // linking


    // saving the theory result to a file
    saveThButton= new QPushButton (QIcon("images/createTH.png"),"");
    saveThButton->setToolTip("Save Result");
    connect(saveThButton, SIGNAL(clicked()), this, SLOT(saveResult()));// linking


    // formula area box for the final result and intermediate result of the theory while its running
    FormulaArea=new QLineEdit("Specification Formula");
    FormulaArea->setReadOnly(true);
    //SetHeight(FormulaArea, 5);
    //SetWidth(FormulaArea, 15);



    // accept button for the current query/ suggestion given by the tool
    AcceptButton=new QPushButton("Accept");
    AcceptButton->setEnabled(false);
    connect(AcceptButton, SIGNAL(clicked()), this, SLOT(AcceptButtonClicked())); // linking

    // rejectt button for the current query/ suggestion given by the tool
    RejectButton=new QPushButton("Reject");
    RejectButton->setEnabled(false);
    connect(RejectButton, SIGNAL(clicked()), this, SLOT(RejectButtonClicked()));// linking

    // the label for the theory name box
    QLabel *theoryNameLabel= new QLabel("Theory Name ");
    theoryNameField=new QLineEdit();// initializing
    theoryNameField->setMaximumWidth(200);// setting width
    //theoryNameLabel->setFrameStyle(QFrame::Box|QFrame::Plain);


    QLabel *variablesLabel= new QLabel("variables");// label for the variables table
    //variablesLabel->setFrameStyle(QFrame::Box|QFrame::Plain);
    QLabel *grammarLabel= new QLabel("grammar");// label for the grammar table
    //grammarLabel->setFrameStyle(QFrame::Box|QFrame::Plain);
	QLabel *voicabLabel= new QLabel("vocab");// label for the vocab table
	//voicabLabel->setFrameStyle(QFrame::Box|QFrame::Plain);
	QLabel *constantsLabel= new QLabel("constants");// label for the constants table
	//constantsLabel->setFrameStyle(QFrame::Box|QFrame::Plain);

	// undo button for the execution of the tool
	UndoButton=new QPushButton("Undo");
	UndoButton->setEnabled(false);
	connect(UndoButton, SIGNAL(clicked()), this, SLOT(UndoButtonClicked()));// linking


    QLabel *NumOpPerClauseBoundLabel=new QLabel("ops per clause"); // label for the number of ops per clause table ??
    NumOpInput = new QSpinBox; // a box with a number to be set
    NumOpInput->setRange(1, 10); // range
    NumOpInput->setSingleStep(1);// stepsize
    NumOpInput->setValue(0);// default value



    QLabel *NumQuantifiersLabel=new QLabel("quantifiers#"); // label for the number of quantifiers
    NumQuantifiersInput = new QSpinBox;// a box with a number to be set
    NumQuantifiersInput->setRange(0, 10); // range
    NumQuantifiersInput->setSingleStep(1); // stepsize
    NumQuantifiersInput->setValue(0); // default value

    InjectUniversalQuantifiers=new QRadioButton("Inject UniversalQ"); // a radio button for weather or not to inject a Universal Quantifier
    InjectExistentialQuantifiers=new QRadioButton("Inject ExistentialQ");// a radio button for weather or not to inject an Exestential Quantifier

    // creating the GUI grid and placing all the components in their correct position

    undoList = new QComboBox; // creating the undo list
    k_mapgrid = new K_mapGrid; // creating the karnaugh map
    k_mapgrid->init(); // initializing karnaugh map
    k_mapgrid->spchk = spchk; // assigninig current theory to arnaugh map
    k_mapgrid->UndoChoiceVector = &UndoChoiceVector; // assigning choice vector of current theory to karnaugh map

    	// creating the grid
    QGridLayout *CentralGridLayout=new QGridLayout();

    //placing all widgets in their position based on x y cordinates in the grid
    CentralGridLayout->addWidget(theoryNameLabel,0,0,1,2);
    CentralGridLayout->addWidget(theoryNameField,0,2,1,2);

    CentralGridLayout->addWidget(variablesLabel,1,4,1,2);
    CentralGridLayout->addWidget(variablesTable,3,0,7,10);
    CentralGridLayout->addWidget(addVariable,10,0,1,1);
    CentralGridLayout->addWidget(deleteVariable,10,1,1,1);

    CentralGridLayout->addWidget(voicabLabel,1,14,1,2);
    CentralGridLayout->addWidget(clausesTable,3,10,7,10);
    CentralGridLayout->addWidget(addVocab,10,10,1,1);
    CentralGridLayout->addWidget(deleteVocab,10,11,1,1);


    CentralGridLayout->addWidget(grammarList,12,0,5,5);
    CentralGridLayout->addWidget(addGrammar,17,0,1,1);
    CentralGridLayout->addWidget(deleteGrammar,17,1,1,1);
    CentralGridLayout->addWidget(grammarLabel,11,2,1,2);

    CentralGridLayout->addWidget(constantList,12,5,5,5);
    CentralGridLayout->addWidget(addConstant,17,5,1,1);
    CentralGridLayout->addWidget(deleteConstant,17,6,1,1);
    CentralGridLayout->addWidget(constantsLabel,11,7,1,2);

    CentralGridLayout->addWidget(AcceptButton,16,10,1,2);
    CentralGridLayout->addWidget(RejectButton,16,12,1,2);
    CentralGridLayout->addWidget(FormulaArea,18,0,1,19);
    CentralGridLayout->addWidget(saveThButton,18,19,1,1);

    CentralGridLayout->addWidget(NumOpPerClauseBoundLabel,14,10,1,2);
    CentralGridLayout->addWidget(NumOpInput,14,12,1,1);
    CentralGridLayout->addWidget(NumQuantifiersLabel,15,10,1,2);
    CentralGridLayout->addWidget(NumQuantifiersInput,15,12,1,1);

    CentralGridLayout->addWidget(InjectUniversalQuantifiers,12,10,1,2);
    CentralGridLayout->addWidget(InjectExistentialQuantifiers,13,10,1,2);


    CentralGridLayout->addWidget(undoList,0,5,1,2);
    CentralGridLayout->addWidget(UndoButton,0,7,1,2);
    CentralWidget->setLayout(CentralGridLayout);
    setCentralWidget(CentralWidget);

    CentralGridLayout->addWidget(k_mapgrid,11,15,6,5);

    /////////init////////////////////////////////////////

    createActions(); // linking the actions to the buttons
    createMenus(); // creating the menues required
    createToolBars(); // creating all the toolbars
    createStatusBar(); // creatking the status bar

    readSettings();// placin the name of the program and organization on the top of the window

    //create similar check, with contentsChanged checking if the model state was modified
    connect(textEdit->document(), SIGNAL(contentsChanged()),
           this, SLOT(documentWasModified()));

    setCurrentFile(""); // initial value for the file name
    setUnifiedTitleAndToolBarOnMac(true);

}

// default constructor
MainWindow::~MainWindow()
{
    
}

// what is event supposed to be ? to close the ntire window, basically exit		// unsure of the purpose
void MainWindow::closeEvent(QCloseEvent *event)
{
    /*
    MainWindow::refresh();
    if (maybeSave()) { //maybeSave() returns true if user answers yes for the saving question
        writeSettings();
        event->accept();
    } else {
        event->ignore();
    }
    */
}


// create theory(.th) file from the input in the GUI to a text file
void MainWindow::writeTHfile(){

	cout<< "Writing Theory file" << endl;
	fillClausesTableByUserVocab();// filling the table in the GUI by the vocab list

    if(theoryNameField->text()=="")// checks if theres no theory name it returns
    {
	// fault tolerace
    	QMessageBox msgBox;
    	msgBox.setText("Theory Name Missing");
    	msgBox.exec();
        return;
    }

    textEdit->clear();// clearing the textfile
    bool saved=false; // a boolean to check if a theory has had modifications since the last save or not
    if(curFile.isEmpty()) // if the file being used is empty we need to give it a name and a location so we save as
    {
        saved=saveAs();
    }
    QString theoryFileString= QString("theory ");

    //read theoryName from a text field
    theoryName=theoryNameField->text();// assigning the theoryName to the text in the theory name field
    /////////////////////////////////////////////////////////////write .th file
    theoryFileString=theoryFileString +theoryName+" { \n\n\t//declaration section\n";


    // writing the variables to the file
    int variablesCount = variableList->count();
    for(int i = 0; i < variablesCount; i++)
    {
    	// Append the type of the variable here
    	theoryFileString.append("\t");
    	if(variablesTable->item(i,1)->text() != "")
    		if(variablesTable->item(i,1)->text().compare("L") == 0)
    			theoryFileString.append("local ");
    		else;

    theoryFileString.append(textForType(variablesTable->item(i,2)->text()) + " "); // appending the type part of the text to the string
    theoryFileString.append(variablesTable->item(i,3)->text()); // appending he variables name
    theoryFileString=theoryFileString +"; \n";			// adding the new line
    }

    theoryFileString=theoryFileString +"\n \n";

    int constantsCount = constantList->count();


    // writing the constants to the file
    theoryFileString=theoryFileString +"\t" + "constants { ";
    for(int i = 0; i < constantsCount; i++)
    {
    theoryFileString.append(constantList->item(i)->text());
    if(i!=constantsCount-1){theoryFileString=theoryFileString +", "; }
    }

    theoryFileString=theoryFileString +"}\n \n";

    // writing the grammar to the file
    int theoryCount = grammarList->count();
    theoryFileString=theoryFileString +"\t"+"grammar { \n";

    for(int i = 0; i < theoryCount; i++)
    {
    	theoryFileString.append("\t");
        theoryFileString.append(grammarList->item(i)->text());
        theoryFileString.append(";\n");
    }

    theoryFileString=theoryFileString +"\t}\n\n";

    // writing the ops per clause bound and num quantifiers bound to the file
    theoryFileString=theoryFileString+"\t"+"num_operations_per_clause_bound = "+NumOpInput->text()+";\n";
    theoryFileString=theoryFileString+"\t"+"num_quantifiers_bound = "+NumQuantifiersInput->text()+";\n\n";

    // writing the vocab to the file
    int vocabCount = vocabList->count();

    if(vocabCount > 0){
		theoryFileString=theoryFileString + "\t" +"// user defined vocab section" + "\n\t" + "vocab { \n";
		for(int i = 0; i < vocabCount; i++)
		{
			theoryFileString.append("\t\t");
			theoryFileString.append(vocabList->item(i)->text());
			theoryFileString=theoryFileString +";\n";
		}
		theoryFileString=theoryFileString +"\t}\n\n";
    }

    theoryFileString=theoryFileString +"}\n";


    textEdit->appendPlainText(theoryFileString); // placing the string in the text file

    cout<< "Writing Theory file end" << endl;
}


// filling the clauses table from vocabList
void MainWindow::fillClausesTableByUserVocab()
{
    int vocabCount=vocabList->count(); // getting vocab count

    QTableWidgetItem *selectItem=new QTableWidgetItem();
    clausesTable->setRowCount(vocabCount);

    // looping over vocab list and placing it in the clauses table
    for(int i = 0; i < vocabCount; i++)
    {
    	// creating the first column and making it active
        clausesTable->setItem(i,0,new QTableWidgetItem());
        selectItem = clausesTable->item(i,0);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Checked);


        // creating the second column and placing the vocab in it
        QString temp = vocabList->item(i)->text().toUtf8().constData();

        //vocabUnFormed.substr (1,vocabUnFormed.length()-2);
        clausesTable->setItem(i,1,new QTableWidgetItem(temp.mid(1,temp.length()-2)));
        qDebug() << "" << vocabList->item(i)->text() << endl;
        selectItem = clausesTable->item(i,1);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        // creating the last column and making it empty
        clausesTable->setItem(i,2,new QTableWidgetItem(""));
        selectItem = clausesTable->item(i,2);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    }
}

// same as above but with the generated vocab
void MainWindow::fillClausesTableByGeneratedVocab()
{
    int vocabCount=spchk->vocab.size();

    QTableWidgetItem *selectItem=new QTableWidgetItem();
    clausesTable->setRowCount(vocabCount);
    for(int i = 0; i < vocabCount; i++)
    {
        clausesTable->setItem(i,0,new QTableWidgetItem());
        selectItem = clausesTable->item(i,0);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Checked);


        clausesTable->setItem(i,1,new QTableWidgetItem(QString::fromStdString(spchk->vocab[i])));
        selectItem = clausesTable->item(i,1);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        clausesTable->setItem(i,2,new QTableWidgetItem(""));
        selectItem = clausesTable->item(i,2);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    }
}

// filling the variables table in the GUI by the variables from the variables list
void MainWindow::fillVariablesTable()
{
    int variablesCount=variables->size(); // getting the number of variables
    variablesTable->setRowCount(variablesCount); // setting the number of rows to the number of variables
    QTableWidgetItem *selectItem;

    // looping over the variables list and placing it in the gui
    for(int i = 0; i < variablesCount; i++)
    {
        variablesTable->setItem(i,0,new QTableWidgetItem());
        selectItem = variablesTable->item(i,0);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Checked);


        variablesTable->setItem(i,1,new QTableWidgetItem(variableList->item(i)->text()));
        selectItem = variablesTable->item(i,1);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        variablesTable->setItem(i,3,new QTableWidgetItem(""));
        selectItem = variablesTable->item(i,3);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        variablesTable->setItem(i,4,new QTableWidgetItem());
        selectItem = variablesTable->item(i,4);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Unchecked);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        selectItem->setFlags(Qt::NoItemFlags);
     }

}

//slots implementation


// adding a new predicate by argument
void MainWindow::AddArgumentNewPredicateClicked()
{
    QString newText;
    if(PredicateArguments->text()=="")// if theres no argument name take the name of the variable
    {newText=VariableArgs->currentText();}
    else// else assign the argument name followed by the variables
    {newText=PredicateArguments->text()+", "+VariableArgs->currentText();}
    PredicateArguments->setText(newText);

}

// adding new predicate button clicked
void MainWindow:: AddNewPredicateClicked()// does no work properly
{
    addVocabDialog->close();
    //optimize: for all dialogs set them earlier and only show them here rather than creating it all everytime the user click the button
    addNewPredicateDialog= new QDialog();

    // creating the new window needed foradding a new predicate
    addNewPredicateDialog->setWindowIcon(QIcon("images/add.png"));
    addNewPredicateDialog->setWindowTitle("Add New Predicate Vocab");
    QVBoxLayout *addNewPredicateDialogLayout=new QVBoxLayout;


    QLabel *PredicateName=new QLabel("Predicate Name");
    NewPredicateNameInput= new QLineEdit();
    QWidget *PredicateNameW=new QWidget();
    QHBoxLayout *PredicateNameWL=new QHBoxLayout();
    PredicateNameWL->addWidget(PredicateName);
    PredicateNameWL->addWidget(NewPredicateNameInput);
    PredicateNameW->setLayout(PredicateNameWL);


    QLabel *SelectArgument=new QLabel("Add Argument");
    VariableArgs=new QComboBox();
    QStringList itemsToSelectFrom;
    // placing all the variables in the list of the new window to use for the predicate
    int variablesCount = variableList->count();
    for(int i = 0; i < variablesCount; i++)
    {
    itemsToSelectFrom<<variableList->item(i)->text();
    }

    VariableArgs->addItems(itemsToSelectFrom);// assign the variables list to the variables arguments to select from

    // a button to add an arguemnt
    QPushButton *AddArgument= new QPushButton ("+");
    AddArgument->setToolTip("Add Argument");
    connect(AddArgument, SIGNAL(clicked()), this, SLOT(AddArgumentNewPredicateClicked())); // linking

    QWidget *PredicateArgsW=new QWidget();
    QHBoxLayout *PredicateArgsWL=new QHBoxLayout();
    PredicateArgsWL->addWidget(SelectArgument);
    PredicateArgsWL->addWidget(VariableArgs);
    PredicateArgsWL->addWidget(AddArgument);
    PredicateArgsW->setLayout(PredicateArgsWL);

    PredicateArguments=new QLabel("");


    QLabel *NewPredicateFormula=new QLabel("Formula:");
    NewPredicateFormulaInput= new QLineEdit();
    QWidget *NewPredicateFormulaW=new QWidget();
    QHBoxLayout *NewPredicateFormulaWL=new QHBoxLayout();
    NewPredicateFormulaWL->addWidget(NewPredicateFormula);
    NewPredicateFormulaWL->addWidget(NewPredicateFormulaInput);
    NewPredicateFormulaW->setLayout(NewPredicateFormulaWL);


    QPushButton *ConfirmNewPredicateAdd= new QPushButton ("Create and Add");
    connect(ConfirmNewPredicateAdd, SIGNAL(clicked()), this, SLOT(ConfirmNewPredicateAddClicked()));



    addNewPredicateDialogLayout->addWidget(PredicateNameW);
    addNewPredicateDialogLayout->addWidget(PredicateArgsW);
    addNewPredicateDialogLayout->addWidget(PredicateArguments);
    addNewPredicateDialogLayout->addWidget(NewPredicateFormulaW);
    addNewPredicateDialogLayout->addWidget(ConfirmNewPredicateAdd);


    addNewPredicateDialog->setLayout(addNewPredicateDialogLayout);
    addNewPredicateDialog->show(); // showing the window created



}

// adding an existing predicate button clicked
void MainWindow:: AddExistingPredicateClicked()
{

	QString fileName = QFileDialog::getOpenFileName(this);
	string line;
	string res;
	ifstream myfile;
	myfile.open(fileName.toStdString().c_str());
	// open file window
	if(myfile.is_open()){
		while(getline(myfile,line)){
			res += line;
		}
		myfile.close();

	    QString tmpVocab = QString::fromStdString(res);


	    int i=clausesTable->rowCount();
	    clausesTable->insertRow(i);

	// this part is the same as insertingg a new vcab clause should be encapsulated
	    clausesTable->setItem(i,1,new QTableWidgetItem(tmpVocab));
	    QTableWidgetItem *selectItem=new QTableWidgetItem();
	    selectItem = clausesTable->item(i,1);
	    selectItem->setFlags(Qt::ItemIsEnabled | 	Qt::ItemIsSelectable);
	
	    vocabList->addItem("("+tmpVocab+")");
	
	    clausesTable->setItem(i,0,new QTableWidgetItem());
	    selectItem = clausesTable->item(i,0);
	    selectItem->data(Qt::CheckStateRole);
	    selectItem->setCheckState(Qt::Checked);
	
	    clausesTable->setItem(i,2,new QTableWidgetItem(""));
	    selectItem = clausesTable->item(i,2);
	    selectItem->setFlags(Qt::ItemIsEnabled | 	Qt::ItemIsSelectable);
	
	    clausesTable->setItem(i,3,new QTableWidgetItem());
	    selectItem = clausesTable->item(i,3);
	    selectItem->data(Qt::CheckStateRole);
	    selectItem->setCheckState(Qt::Unchecked);
	    selectItem->setFlags(Qt::ItemIsEnabled | 	Qt::ItemIsSelectable);
    	selectItem->setFlags(Qt::NoItemFlags);
	
	QString theoryFileName = curFile;
	    saveFile("temp.th");
	
	    string temp = "./sc -t temp.th -p";
	    string termResult = exec(&temp[0]); // executes a command 	line comand and returns the result printed to the terminal
	    termResult = termResult.substr(termResult.rfind	('\n',termResult.length() - 2));
    	termResult = trim(termResult);
	
	// checks if the clause being added is valid or contains an error
	    if (termResult.compare("The File temp.th is a Proper file") != 0) {
	    	QMessageBox::warning(this, tr("Application"), tr("The new vocab added here contains errors . %1:\n%2.").arg(tmpVocab).arg	("invalid vocab"));
	    	clausesTable->removeRow(i);
	    	delete vocabList->takeItem(i);
	    	addVocabDialog->activateWindow();
	    	return;
	    }
	    else
	    {
		cout << theoryFileName.toStdString() << endl;
		loadFile("temp.th");
		spchk->theoryFileName = theoryFileName.toStdString();
		setCurrentFile(theoryFileName);
	    	addVocabDialog->accept();
	    }
	}
	
	
    addVocabDialog->close();
}

// when the user vocab should be used
void MainWindow:: UseUserVocabYClicked(){
    constructUseUserVocabDialog->accept();
    UseUserVocab=true;
}

// when the user vocab shouldn't be used
void MainWindow:: UseUserVocabNClicked(){
    constructUseUserVocabDialog->reject();
}

// accept button clicked for accepting the current query while the tool is running the suggestions
void MainWindow::AcceptButtonClicked(){
    //collect answer

	// update the undo list
	updateList(undoList, print_vectorS(spchk->choice));



	// setting answer to accept
    answer="Y";

	// clearing old data
    vars_partials.clear();
    vocab_partials.clear();
    CC->ReadAnswer(answer,vars_partials,vocab_partials); // checking choice for the answer of the user


    //display new choice
    traverse T(*spchk);


	// giving traverse the current choice of the vocab
    T.choice=spchk->choice;
    T.depth=Depth;

    spchk->choice=T.nextEvaluation();// gives the new choice in the traversal of the equivelence classes to the spchk

    bool choiceDisplayed=false;

    vector <vocab_value_t> StopChoice(spchk->choice.size());
    for(int i=0;i<StopChoice.size();i++)
    { StopChoice[i]=vuu;}

    while(spchk->choice!=StopChoice&&choiceDisplayed==false)
    {// if the choice displayed is not the last valuation

        if(CC->Z3Check()==true)
        {
          vector<string> Display;
          Display=CC->displaymodel();

          //write display to GUI
          vector<string> VarName;
          vector<string> VarValue;

          GenerateDisplayNameValue(Display, VarName,VarValue);
          ViewDisplay(VarName,VarValue);
          choiceDisplayed=true;
          cout<<"accept: displayed vector is: "; print_vector(spchk->choice);
        }
        else
            spchk->choice= T.nextEvaluation(); //get next choice
    }


    // writing the result in the  result box and simplifying it
    string espressoFormula =spchk->espressoSimplifyR();
    string abcFormula=spchk->abcSimplifyRNA();
    cout<<"*************ESPRESSO: "<<espressoFormula<<endl;
    cout<<"*************ABC: "<<abcFormula<<endl;
    string condensedFormula ="\n \n ABC:\n"+abcFormula+"Espresso:\n"+espressoFormula;
    string finalFormulaView = condensedFormula;
    
    int location = abcFormula.find("\n");
    location = abcFormula.find("\n",location+1);
    string forFormulaBar = abcFormula.substr(location,abcFormula.length());

    FormulaArea->setText(QString::fromStdString(forFormulaBar));
	// end writing the result in the result box


    if(spchk->choice==StopChoice)// if the last choice has been reached
    {
        ConstructDone=true;
        PostConstruct();// post processing 
    }
    //uncheck reason cells// there should be a method for this. uncheckReasonCellsVocaublary(), uncehckReasonCellsVariables()
    else{
		QTableWidgetItem *selectItem;
		for(int i = 0; i < clausesTable->rowCount(); i++)
		{
			selectItem = clausesTable->item(i,3);
			selectItem->setCheckState(Qt::Unchecked);
		}
		for(int i = 0; i < variablesTable->rowCount(); i++)
		{
			selectItem = variablesTable->item(i,5);
			selectItem->setCheckState(Qt::Unchecked);
		}
		Depth=T.depth;
    }

	// pushing crrent state to the states vector
    pushState();
	// updating the undo lists o include the new state
    updateUndoList();
	// updating the karnagh map and redrawing it
    k_mapgrid->updateKmap();
    k_mapgrid->repaint();

}

// a method for updating the undo list
void MainWindow::updateList(QComboBox *  list, string newThing)
{
	list->addItem(tr(newThing.c_str()));
	list->setCurrentIndex(list->count() - 1);
	UndoButton->setEnabled(true);
}

// if the reject button is clicked
void MainWindow::RejectButtonClicked()
{
	// update undo list
	updateList(undoList, print_vectorS(spchk->choice));
    vars_partials.clear();
    vocab_partials.clear();


    QTableWidgetItem *selectItem;
    string varName;

    // looping over the variabels to check which ones is causing the trouble or the reject and placing them in vars_partials
    for(int i = 0; i < variablesTable->rowCount(); i++)
    {
        selectItem = variablesTable->item(i,5);
        if(selectItem->checkState()== Qt::Checked)
        {
            selectItem = variablesTable->item(i,3);
            varName=selectItem->text().toStdString();
            //varId=th->getIdOfUsedVarible(varName,SCTheory::SC_VOCAB);
            vars_partials.push_back(varName);
        }
    }

    // looping over the clauses to check which ones are causing the trouble or the reject and placing them in vocab_partials
    for(int i = 0; i < clausesTable->rowCount(); i++)
        {
            selectItem = clausesTable->item(i,3);
            if(selectItem->checkState()== Qt::Checked)
            {
                vocab_partials.push_back(i);
            }
        }

    // if the user chceks vocab suggestions and variable suggestions at the same time
    if(vars_partials.size()!=0 || vocab_partials.size()!=0)
    {
        cout<<"Reject by vocabs or variables?\n"; //qdialog box

    }

    // if some variable suggestions are selected
    if(vars_partials.size()!=0)
    {
        answer="V";
    }

    // if some vocabulary suggestions are selected
    else if(vocab_partials.size()!=0)
    {
        answer="B";
    }

    else{
    answer="N";
    }


    cout << " aaaa" << endl;
    CC->ReadAnswer(answer,vars_partials,vocab_partials); // checking choice for the current selection

    //display new choice
    traverse T(*spchk); 

    T.choice=spchk->choice;
    T.depth=Depth;
// debugging statements #######
    for(unsigned int i=0;i< T.SCV->unsat_cores.size();i++)
    	cout << "1-SIZE T : " << T.SCV->unsat_cores[i].size()  << endl;
    for(unsigned int i=0;i< spchk->unsat_cores.size();i++)
    	cout << "2-SIZE spchk : " << spchk->unsat_cores[i].size() <<endl;
// #######################


    spchk->choice=T.nextEvaluation();

    bool choiceDisplayed=false;// 

    vector <vocab_value_t> StopChoice(spchk->choice.size());
    for(int i=0;i<StopChoice.size();i++)
    { StopChoice[i]=vuu;}// filling up the stop choice to campare to later

    while(spchk->choice!=StopChoice&&choiceDisplayed==false)//while we did not reach the final valuation
    {

        if(CC->Z3Check()==true)// if satisfiable
        {
          vector<string> Display;
          Display=CC->displaymodel();// display assignment

          //write display to GUI
          vector<string> VarName;
          vector<string> VarValue;

          GenerateDisplayNameValue(Display, VarName,VarValue);//
          ViewDisplay(VarName,VarValue);
          choiceDisplayed=true;
          cout<<"reject: displayed vector is: "; print_vector(spchk->choice);
        }
        else
            spchk->choice= T.nextEvaluation(); //get next choices
    }

// debugging statements ########
    cout<<"SPCHK: the formula is:"<<endl;

    string f= Z3_ast_to_string(spchk->ctx->m_context, spchk->return_val);
    cout<<f<<endl;
//#############


    // writing the result in the  result box
    string espressoFormula =spchk->espressoSimplifyR(); // 
    string abcFormula=spchk->abcSimplifyRNA();
    cout<<"*************ESPRESSO: "<<espressoFormula<<endl;
    cout<<"*************ABC: "<<abcFormula<<endl;
    string condensedFormula ="\n \n ABC:\n"+abcFormula+"Espresso:\n"+espressoFormula;
    string finalFormulaView = condensedFormula;

    int location = abcFormula.find("\n");
    location = abcFormula.find("\n",location+1);
    string forFormulaBar = abcFormula.substr(location,abcFormula.length());

    FormulaArea->setText(QString::fromStdString(forFormulaBar));
	// end writing the result in the result box

    if(spchk->choice==StopChoice)// if the choice reached the last valuatino it shuold stop
    {
        ConstructDone=true;
        PostConstruct();
    }
    else{
    //uncheck reason cells
    QTableWidgetItem *selectItem;
    for(int i = 0; i < clausesTable->rowCount(); i++)
    {
        selectItem = clausesTable->item(i,3);
        selectItem->setCheckState(Qt::Unchecked);
    }
    for(int i = 0; i < variablesTable->rowCount(); i++)
    {
        selectItem = variablesTable->item(i,5);
        selectItem->setCheckState(Qt::Unchecked);
    }

    Depth=T.depth;
    }


//pushing back the state of the program for the undo
    pushState();
    updateUndoList();
// updating the karnauugh map and redrawing it
    k_mapgrid->updateKmap();
    k_mapgrid->repaint();




}


// after construction of the theory is done
void MainWindow::PostConstruct()
{


	// re-disabeling the accept and reject buttons
    AcceptButton->setEnabled(false);
    RejectButton->setEnabled(false);

    // re-enabeling the previos features -- this should be a method on its own
    addVariable->setEnabled(true);
    deleteVariable->setEnabled(true);
    addVocab->setEnabled(true);
    deleteVocab->setEnabled(true);
    addGrammar->setEnabled(true);
    deleteGrammar->setEnabled(true);
    addConstant->setEnabled(true);
    deleteConstant->setEnabled(true);
    NumOpInput->setEnabled(true);
    NumQuantifiersInput->setEnabled(true);
    InjectUniversalQuantifiers->setEnabled(true);
    InjectExistentialQuantifiers->setEnabled(true);
    constructAct->setEnabled(true);

    //remove values from vocab and variables
    QTableWidgetItem *selectItem;
    int i=0;
    // emptying suggestions in variables table
    for(i = 0; i < variablesTable->rowCount(); i++)
    {

        variablesTable->setItem(i,4,new QTableWidgetItem(""));
        selectItem = variablesTable->item(i,4);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        variablesTable->setItem(i,5,new QTableWidgetItem());
        selectItem = variablesTable->item(i,5);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Unchecked);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        selectItem->setFlags(Qt::NoItemFlags);
    }

    // emptying suggestions in vocab table
    for(i = 0; i < clausesTable->rowCount(); i++)
        {

    		clausesTable->setItem(i,2,new QTableWidgetItem(""));
            selectItem = clausesTable->item(i,2);
            selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        }

    // writing the final result in the  result box simplifying it then displaying it
    string espressoFormula =spchk->espressoSimplifyR();
    string abcFormula=spchk->abcSimplifyRNA();
    cout<<"*************ESPRESSO: "<<espressoFormula<<endl;
    cout<<"*************ABC: "<<abcFormula<<endl;

    string condensedFormula ="\n \n ABC:\n"+abcFormula+"Espresso:\n"+espressoFormula;
    string finalFormulaView = condensedFormula;

    int location = abcFormula.find("\n");
    location = abcFormula.find("\n",location+1);
    string forFormulaBar = abcFormula.substr(location,abcFormula.length());

    FormulaArea->setText(QString::fromStdString(forFormulaBar));

    QPalette p = textEdit->palette();
    p.setColor(QPalette::Base, QColor(240, 240, 255));
    FormulaArea->setPalette(p);

    Depth=0;
}

// displays the values of the variables on the GUI tables
void MainWindow::GenerateDisplayNameValue(vector<string>Display, vector<string>&VarName,vector<string>&VarValue)
{
    VarName.clear();
    VarValue.clear();
    string d;
    string s;
    size_t pos = 0;
    std::string delimiter ;
    std::string token;
    for(int i=0; i<Display.size(); i++)
    {
        d=Display.at(i);
        s=d;

        delimiter = "[";
        pos = s.find(delimiter);
        if(pos==-1)
        {
            s=d;
            delimiter=" = ";
            pos= s.find(delimiter);
            token = s.substr(0, pos);

            VarName.push_back(token);
            s.erase(0, pos + delimiter.length());
            delimiter=":";
            pos= s.find(delimiter);
            token = s.substr(0, pos);
            VarValue.push_back(token);

        }
        else{

            token = s.substr(0, pos);

            string arrayName=token;

            VarName.push_back(arrayName);
            string temp="";
            bool firstEntry=true;
            //"a[0:int] = 4:int, a[otherwise]= 4:int");
            while(pos!=-1) //while [ exists, i.e. more array elements exist
            {
                s.erase(0, pos + delimiter.length());

                delimiter="]";
                pos= s.find(delimiter);
                token = s.substr(0, pos);
                if(token!="otherwise")
                {
                    delimiter=":";
                    pos= s.find(delimiter);
                    token = s.substr(0, pos);
                }


                if(firstEntry)
                    {temp+=arrayName+"["+token+"]=";
                    firstEntry=false;}
                else
                    {temp+=", "+arrayName+"["+token+"]=";}

                s.erase(0, pos + delimiter.length());

                delimiter="= ";
                pos= s.find(delimiter);
                s.erase(0, pos + delimiter.length());

                delimiter=":";
                pos= s.find(delimiter);
                token = s.substr(0, pos);

                temp+=token;
                s.erase(0, pos + delimiter.length());

                delimiter = "[";
                pos = s.find(delimiter);
            }

            VarValue.push_back(temp);


        }

    }

}


// filling up everything to the GUI
void MainWindow::ViewDisplay(vector<string> VarName,vector<string> VarValue) //view Display in gui
{
    int varNum=VarName.size();
    for(int j=0; j<varNum; j++)
    {
        QTableWidgetItem *selectItem;
        string name="";
        int i=0;
        for(i = 0; i < variablesTable->rowCount(); i++)
        {
            selectItem = variablesTable->item(i,3); //name cell
            name=selectItem->text().toStdString();
            if(name==VarName[j])
            {break;}
        }
        //inject VarValue in the value cell
        if(name!="" && i < variablesTable->rowCount()){
            QString value=QString::fromStdString(VarValue.at(j));
            variablesTable->setItem(i,4,new QTableWidgetItem(value));
            variablesTable->item(i,4)->setBackgroundColor(QColor(170,170,255));
            selectItem = variablesTable->item(i,4);
            selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        }
        else{cerr<<"GUI:couldn't find variable in GUI!"<<endl;}
    }


// give the true vocabs green fill and false one red fill
// this method is puerl for aesthetic value
    for(int i = 0; i < clausesTable->rowCount(); i++)
        {

    		QTableWidgetItem *selectItem;

        	clausesTable->setItem(i,2,new QTableWidgetItem((spchk->choice[i] == vtt ) ? "true" : "false"));
        	if(clausesTable->item(i,2)->text() == "true")
        	{
        		//color it green
        		clausesTable->item(i,2)->setBackgroundColor(QColor(170,255,170));
        	}
        	else if(clausesTable->item(i,2)->text() == "false")
        	{
        		//color it red
        		clausesTable->item(i,2)->setBackgroundColor(QColor(255,170,170));
        	}
        	else
        	{}
        }
}


// the method that is applied when the construct button is clicked it supposedly constructs the vocab from the variables and grammar rules provided.
void MainWindow::construct()
{

	// checks if there is no current file name and if not asks the user to put one
	if(curFile.compare("") == 0)
	{
		QMessageBox::StandardButton ret;
		        ret = QMessageBox::warning(this, tr("No file to Construct"),
		                                   //"no file to construct"
		                     tr("there is no file selected to construct\n"
		                        "please select a file?"),
		                     QMessageBox::Ok);
		        if (ret == QMessageBox::Ok)
		            return;
		        else
		        	return;
	}

	// eles check if there is no theory file
	if (!spchk->readTheoryFile()){   //else read theory file and exit in case of error. updates spchk.th
		 QMessageBox msgBox;
		 msgBox.setText("Error 101 \n The File cannot be opened, the program will now exit.");
	      qApp->exit();
	    }

	th = spchk->th;
	spchk->genVocab |= th->vocabIds.empty(); //forcibly set vocabulary generation if the vocab is empty

    createVocab(); // creating vocab

    FormulaArea->clear(); // clearing the formula area
    QPalette p = textEdit->palette();
    p.setColor(QPalette::Base, QColor(255, 255, 255));
    FormulaArea->setPalette(p);


    // initializing user vocab dialog
    constructUseUserVocabDialog= new QDialog();
    constructUseUserVocabDialog->setWindowIcon(QIcon("images/question_user.png"));
    constructUseUserVocabDialog->setWindowTitle("Use User Vocab");
    QVBoxLayout *constructUseUserVocabDialogLayout=new QVBoxLayout;

    // creates a yes button for using the user vocab
    QPushButton *UseUserVocabY= new QPushButton ("Yes");
    connect(UseUserVocabY, SIGNAL(clicked()), this, SLOT(UseUserVocabYClicked())); // link

    // creates a no button for using the user vocab
    QPushButton *UseUserVocabN= new QPushButton ("No");
    connect(UseUserVocabN, SIGNAL(clicked()), this, SLOT(UseUserVocabNClicked()));

    QLabel *UseUserVocabInstruction=new QLabel("Would you like to use input vocab?");

    QHBoxLayout*DialogButtonsLayout=new QHBoxLayout();
    QWidget*DialogButtons=new QWidget();
    DialogButtonsLayout->addWidget(UseUserVocabY);
    DialogButtonsLayout->addWidget(UseUserVocabN);
    DialogButtons->setLayout(DialogButtonsLayout);

    constructUseUserVocabDialogLayout->addWidget(UseUserVocabInstruction);
    constructUseUserVocabDialogLayout->addWidget(DialogButtons);

    constructUseUserVocabDialog->setLayout(constructUseUserVocabDialogLayout);



    disableGuiItems(); // disable the buttons that should be greyed out when the construction is running
    enableGuiItems(); // enable the buttons that are used during the construction is running

    //enable checking reason col in vocabtable and variablesTable
    QTableWidgetItem *selectItem;
    // uncheck all checked variable  values
    if(variablesTable->rowCount()!=0){
    for(int i=0; i<variablesTable->rowCount(); i++)
    {
        variablesTable->setItem(i,5,new QTableWidgetItem());
        selectItem = variablesTable->item(i,5);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Unchecked);
    }
    }

    reloadVocab();

                // checking all clausesfrom the clauses table
                if(clausesTable->rowCount()!=0){
                    for(int i=0; i<clausesTable->rowCount(); i++)
                    {
                        clausesTable->setItem(i,3,new QTableWidgetItem());
                        selectItem = clausesTable->item(i,3);
                        selectItem->data(Qt::CheckStateRole);
                        selectItem->setCheckState(Qt::Unchecked);
                    }
                    }
    //fill scma variables from gui

    th->name=theoryName.toStdString(); // assigning theory name to th.name

    th->numQuantifierBound=NumQuantifiersInput->value(); // setting Quantifier value

    th->numClauseOperationBound=NumOpInput->value();// setting operation value

    if(InjectUniversalQuantifiers->isChecked()) // cehcking weather to inject universal or local quantifier
        spchk->injectLocal=false;
    else if(InjectExistentialQuantifiers->isChecked())
        spchk->injectLocal=true;

    //fill vocabs into smtVocab
    const char* smtVocab[vocabList->count()];QString content;
    QByteArray byteArray[vocabList->count()];

    for(int i=0; i<vocabList->count(); i++)
    {
        content= vocabList->item(i)->text();
        byteArray[i] = content.toUtf8();
        smtVocab[i]= byteArray[i].constData();
    }


    smtVocab[vocabList->count()]=NULL;

    spchk->buildAndParseStringFormula(); //translates from SMT format to Z3 AST objects and corresponding functional symbols
    cout << "done building" << endl;
    spchk->printDeclarations();      //pretty prints AST formulae to visually check against originals
    cout << "done printing AST" << endl;
    spchk->assertParsedFormulae(); //set up assertion-retraction syste,

    traverse T(*spchk); // traversing spchk for the next query

    spchk->initTraversal();
    spchk->choice= T.firstEvaluation();

    vector <vocab_value_t> StopChoice(spchk->choice.size());
    vector <vocab_value_t> copyChoice(StopChoice);

    for(int i=0;i<StopChoice.size();i++)
    { StopChoice[i]=vuu;}

    cout<<"StopChoice: "; print_vector(StopChoice);

    bool ChoiceDisplayed=false;

    CC= new checkchoice(spchk);
    CC->SCV = spchk;

    while(spchk->choice!=StopChoice && ChoiceDisplayed==false)
    {

        if(CC->Z3Check()==true) // if there is a satisfying assignment
        {
          vector<string> Display;
          Display=CC->displaymodel();

          //write display to GUI
          vector<string> VarName;
          vector<string> VarValue;
          //Display.push_back("a[0:int] = 1:int, a[1:int] = 2:int, a[otherwise]= 3:int\n"); //for testing on arrays
          GenerateDisplayNameValue(Display, VarName,VarValue);
          ViewDisplay(VarName,VarValue);

          ChoiceDisplayed=true;

          cout<<"construct: displayed vector is: "; print_vector(spchk->choice);

        }
        else
            spchk->choice= T.nextEvaluation(); //get next choice
    }

    if(spchk->choice==StopChoice)// if the last valuation is reached
    {
        ConstructDone=true;
        PostConstruct();// post processing
    }

    Depth=T.depth;

// push the new state of the program for the undo
    pushState();
// pdating the undo choices
    updateUndoList();



    if((spchk->vocab.size()) > 10 )
    {
    	k_mapgrid->draw = false;
    }
    else{
		if(spchk->vocab.size() % 2 == 1)
		{
			k_mapgrid->x = pow(2,((spchk->vocab.size()/2)+1));
			k_mapgrid->y = pow(2,(spchk->vocab.size()/2));
		}
		else
		{
			k_mapgrid->x = pow(2,(spchk->vocab.size()/2));
			k_mapgrid->y = pow(2,(spchk->vocab.size()/2));
		}
		k_mapgrid->draw = true;
    }

// update karnaugh map
    k_mapgrid->updateValues();
// redraw arnaugh map
    k_mapgrid->repaint();

    }


 // placing the variables in the gui in the propper understandable format
void MainWindow::handleVariableGui(QString name, QString type, string GLN)
{

    const string & Name= name.toUtf8().constData();
    string Type;
    unsigned int mode=0;

    if(type=="int")
        Type="int";
    else if(type=="int []")
        Type="array";
    else if(type=="boolean")
        Type="bool";
    else if(type=="boolean []")
        Type="barray";
    else
    	Type="unknown";


    if(GLN=="G")//Global
        mode=NODE_GLOBAL;
    else if(GLN=="L")//Local
        mode=NODE_LOCAL;
    //else if(GLN=="N")//None
    //    mode=0;

    cout<<"0\n";//dd
    int id=th->handleDeclaration(Name,Type,mode);
    //return id;

}

// reloading vocab to the GUI
void MainWindow::reloadVocab()
{

	qDebug() << "reloading vocab" << endl;

    int vocCount=spchk->vocab.size();
      clausesTable->setRowCount(vocCount);
      QTableWidgetItem *selectItem;
      vocabList->clear();

      // placing the vocab in to the GUI by values from the vocab vector in the spchk
        for (unsigned int i = 0; i < vocCount; i++) {

        	vocabList->addItem(QString::fromStdString(spchk->vocabPrefixToInfix(spchk->vocab[i])));
        	string vocabUnFormed = spchk->vocabPrefixToInfix(spchk->vocab[i]);
        	vocabUnFormed = vocabUnFormed.substr (1,vocabUnFormed.length()-2);
        	clausesTable->setItem(i,1,new QTableWidgetItem(QString::fromStdString(vocabUnFormed)));
        	//clausesTable->setItem(i,1,new QTableWidgetItem(QString::fromStdString(spchk->vocab[i])));
        	QTableWidgetItem *selectItem=new QTableWidgetItem();
        	selectItem = clausesTable->item(i,1);
        	selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);


        	std::string s(1,'A'+i);
        	clausesTable->setItem(i,0,new QTableWidgetItem(QString::fromStdString(s)));
        	selectItem = clausesTable->item(i,0);

        	clausesTable->setItem(i,2,new QTableWidgetItem(""));
        	selectItem = clausesTable->item(i,2);
        	selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        	clausesTable->setItem(i,3,new QTableWidgetItem());
        	selectItem = clausesTable->item(i,3);
        	selectItem->data(Qt::CheckStateRole);
        	selectItem->setCheckState(Qt::Unchecked);
        	selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
        	selectItem->setFlags(Qt::NoItemFlags);

        }

      cout << "vocab loading over" << endl;
}

// relaoding alll variables in the GUI
void MainWindow::reloadVariables()
{

    int variablesCount=th->varIds.size();
    	variablesTable->setRowCount(0);
    variablesTable->setRowCount(variablesCount);
    QTableWidgetItem *selectItem;
    VocabNode* node;
    int idx;
    variableList->clear(); // clearing the variables list

    // looping over the variables and placing them in the GUI
    for(int i = 0; i < variablesCount; i++)
    {
        idx=th->varIds.at(i);
        node =th->nodes.at(idx);//get node
        string name= node->name;
        string type= node->type;
        int id=node->id;
        string gln="G";
        if (node->isGlobal())
        {gln="G";}
        else if(node->isLocal())
        {gln="L";}

        //variables array is used only for saving names to be listed when adding new grammar
        variableList->addItem(QString::fromStdString(name));


        variablesTable->setItem(i,0,new QTableWidgetItem(QString::number(id)));
        selectItem = variablesTable->item(i,0);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Checked);

        variablesTable->setItem(i,1,new QTableWidgetItem(QString::fromStdString(gln)));
        selectItem = variablesTable->item(i,1);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        variablesTable->setItem(i,2,new QTableWidgetItem(QString::fromStdString(type)));
        selectItem = variablesTable->item(i,2);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        variablesTable->setItem(i,3,new QTableWidgetItem(QString::fromStdString(name)));
        selectItem = variablesTable->item(i,3);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        variablesTable->setItem(i,4,new QTableWidgetItem());
        selectItem = variablesTable->item(i,4);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        variablesTable->setItem(i,5,new QTableWidgetItem());
        selectItem = variablesTable->item(i,5);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Unchecked);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        selectItem->setFlags(Qt::NoItemFlags);
     }
    variablesTable->setVisible(false);
        variablesTable->resizeColumnsToContents();
        variablesTable->setVisible(true);

}


// the plus button for adding a variable to the variable list window after being clicked
void MainWindow::ConfirmVariableAddClicked()
{
	//assigning the variables name
    QString name=VariableNameInput->text();
    QString type=VariableTypeInput->itemText(VariableTypeInput->currentIndex());
    string gln="N";

    // checking if global or local
    if(globalVariable->isChecked())
    {gln="G";
    }
    else if (localVariable->isChecked())
    {gln="L";
    }

    handleVariableGui(name,type,gln);
    //add status showing variable added at node: node.id

    addVariableDialog->accept();
    //filling variable table from theory
    reloadVariables();
}

// the plus button for adding a variable to the variable list window
void MainWindow::addVariableToGui(QString n,QString t)
{
    QString name=n;
    QString type=t;
    string gln="N";

    if(globalVariable->isChecked())
    {gln="G";
    }
    else if (localVariable->isChecked())
    {gln="L";
    }

    handleVariableGui(name,type,gln);
    //add status showing variable added at node: node.id

    addVariableDialog->accept();

    //filling variable table from theory
    reloadVariables();
}

// after clicking the button to add a variable
void MainWindow::addVariableClicked()
{
	// creating the box for adding a new variable
    addVariableDialog = new QDialog();
    addVariableDialog->setWindowIcon(QIcon("images/add.png"));
    addVariableDialog->setWindowTitle("Add New Variable");
    QVBoxLayout *addVariableDialogLayout=new QVBoxLayout;

    QPushButton *ConfirmVariableAdd= new QPushButton ("Add");
    connect(ConfirmVariableAdd, SIGNAL(clicked()), this, SLOT(ConfirmVariableAddClicked()));

    QWidget *Name=new QWidget();
    QHBoxLayout *NameLayout=new QHBoxLayout;
    QLabel *VariableName=new QLabel("Name");
    VariableNameInput= new QLineEdit();
    NameLayout->addWidget(VariableName);
    NameLayout->addWidget(VariableNameInput);
    Name->setLayout(NameLayout);

    QWidget *Type=new QWidget();
    QHBoxLayout *TypeLayout=new QHBoxLayout;
    QLabel *VariableType=new QLabel("Type");
    VariableTypeInput=new QComboBox();
    VariableTypeInput->addItem("int");
    VariableTypeInput->addItem("int []");
    VariableTypeInput->addItem("boolean");
    VariableTypeInput->addItem("boolean []");
    TypeLayout->addWidget(VariableType);
    TypeLayout->addWidget(VariableTypeInput);
    Type->setLayout(TypeLayout);

    globalVariable=new QRadioButton("Global",this);
    localVariable=new QRadioButton("Local",this);

    addVariableDialogLayout->addWidget(Name);
    addVariableDialogLayout->addWidget(Type);
    addVariableDialogLayout->addWidget(globalVariable);
    globalVariable->setChecked(true);
    addVariableDialogLayout->addWidget(localVariable);
    addVariableDialogLayout->addWidget(ConfirmVariableAdd);

    addVariableDialog->setLayout(addVariableDialogLayout);
    addVariableDialog->show(); // showing the box
}

// adding a constant button clicked
void MainWindow::addConstantClicked()
{
	// creating the box for adding a constant
    addConstantDialog = new QDialog();
    addConstantDialog->setWindowIcon(QIcon("images/add.png"));
    addConstantDialog->setWindowTitle("Add New Constant");
    QVBoxLayout *addConstantDialogLayout=new QVBoxLayout;

    QPushButton *ConfirmConstantAdd= new QPushButton ("Add");
    connect(ConfirmConstantAdd, SIGNAL(clicked()), this, SLOT(ConfirmConstantAddClicked()));

    QLabel *AddConstantInstruction=new QLabel("Choose a constant to add:");

    ConstantInput = new QSpinBox;
    ConstantInput->setRange(-20, 20);
    ConstantInput->setSingleStep(1);
    ConstantInput->setValue(0);

    addConstantDialogLayout->addWidget(AddConstantInstruction);
    addConstantDialogLayout->addWidget(ConstantInput);
    addConstantDialogLayout->addWidget(ConfirmConstantAdd);


    addConstantDialog->setLayout(addConstantDialogLayout);
    addConstantDialog->show(); // chowing the box created

}

// accepting the constant added after clicking ok basically
void MainWindow::ConfirmConstantAddClicked()
{
    addConstantDialog->accept();
    th->handleConstantDeclaration(ConstantInput->value());
    reloadConstant();
}

// reloading constants to the GUI
void MainWindow::reloadConstant()
{
    int constantNum= th->constantIds.size();

    VocabNode* node;
    constantList->clear();
    int idx;
    for(int i=0; i<constantNum; i++)
    {
        idx=th->constantIds[i];
        node= th->nodes[idx];
        int value=node->value;
        constantList->addItem(QString::number(value));
        cout << idx << ":";
        cout << value << endl;
    }

}



// thewindow popup to add a vocab when the + button is clicked under the vocab rules window
void MainWindow::addVocabClicked()
{
    addVocabDialog = new QDialog();
    addVocabDialog->setWindowIcon(QIcon("images/add.png"));
    addVocabDialog->setWindowTitle("Add New Vocab");
    QVBoxLayout *addVocabDialogLayout=new QVBoxLayout;


    QPushButton *ConfirmVocabAdd= new QPushButton ("Add");
    connect(ConfirmVocabAdd, SIGNAL(clicked()), this, SLOT(ConfirmVocabAddClicked()));

    QPushButton *AddNewPredicate= new QPushButton ("Add as new predicate");
    connect(AddNewPredicate, SIGNAL(clicked()), this, SLOT(AddNewPredicateClicked()));

    QPushButton *AddExistingPredicate= new QPushButton ("Add existing predicate");
    connect(AddExistingPredicate, SIGNAL(clicked()), this, SLOT(AddExistingPredicateClicked()));

    QLabel *AddVocabInstruction=new QLabel("Enter a vocab to add:");
    VocabInput= new QLineEdit();

    addVocabDialogLayout->addWidget(AddVocabInstruction);
    addVocabDialogLayout->addWidget(VocabInput);
    addVocabDialogLayout->addWidget(ConfirmVocabAdd);
    addVocabDialogLayout->addWidget(AddNewPredicate);
    addVocabDialogLayout->addWidget(AddExistingPredicate);


    addVocabDialog->setLayout(addVocabDialogLayout);
    addVocabDialog->show();

}

// the accept after adding a new predicate hat places it in the GUI
void MainWindow::ConfirmNewPredicateAddClicked()
{
    addNewPredicateDialog->accept();
    QString tmpVocab=NewPredicateNameInput->text();
    vocabList->addItem(tmpVocab);

    int i=clausesTable->rowCount();
    clausesTable->insertRow(i);

    clausesTable->setItem(i,1,new QTableWidgetItem(tmpVocab));
    QTableWidgetItem *selectItem=new QTableWidgetItem();
    selectItem = clausesTable->item(i,1);
    selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);


    clausesTable->setItem(i,0,new QTableWidgetItem());
    selectItem = clausesTable->item(i,0);
    selectItem->data(Qt::CheckStateRole);
    selectItem->setCheckState(Qt::Checked);

    clausesTable->setItem(i,2,new QTableWidgetItem(""));
    selectItem = clausesTable->item(i,2);
    selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);


    clausesTable->setItem(i,3,new QTableWidgetItem());
    selectItem = clausesTable->item(i,3);
    selectItem->data(Qt::CheckStateRole);
    selectItem->setCheckState(Qt::Unchecked);
    selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    selectItem->setFlags(Qt::NoItemFlags);

}

// the accept after a vocab is clicked and placing it in the GUI
void MainWindow::ConfirmVocabAddClicked()
{

    QString tmpVocab=VocabInput->text();


    int i=clausesTable->rowCount();
    clausesTable->insertRow(i);

    clausesTable->setItem(i,1,new QTableWidgetItem(tmpVocab));
    QTableWidgetItem *selectItem=new QTableWidgetItem();
    selectItem = clausesTable->item(i,1);
    selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

    vocabList->addItem("("+tmpVocab+")");

    clausesTable->setItem(i,0,new QTableWidgetItem());
    selectItem = clausesTable->item(i,0);
    selectItem->data(Qt::CheckStateRole);
    selectItem->setCheckState(Qt::Checked);

    clausesTable->setItem(i,2,new QTableWidgetItem(""));
    selectItem = clausesTable->item(i,2);
    selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

    clausesTable->setItem(i,3,new QTableWidgetItem());
    selectItem = clausesTable->item(i,3);
    selectItem->data(Qt::CheckStateRole);
    selectItem->setCheckState(Qt::Unchecked);
    selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    selectItem->setFlags(Qt::NoItemFlags);

    QString theoryFileName = curFile;
    saveFile("temp.th");

    string temp = "./sc -t temp.th -p";
    string termResult = exec(&temp[0]); // executes a command line comand and returns the result printed to the terminal
    termResult = termResult.substr(termResult.rfind('\n',termResult.length() - 2));
    termResult = trim(termResult);

    if (termResult.compare("The File temp.th is a Proper file") != 0) {
    	QMessageBox::warning(this, tr("Application"), tr("The new vocab added to this theory contains errors . %1:\n%2.").arg("temp.th").arg("invalid vocab"));
    	clausesTable->removeRow(i);
    	delete vocabList->takeItem(i);
    	addVocabDialog->activateWindow();
    	return;
    }
    else
    {
	
	cout << theoryFileName.toStdString() << endl;
	loadFile("temp.th");
	spchk->theoryFileName = theoryFileName.toStdString();
	setCurrentFile(theoryFileName);
	//spchk->readTheoryFile();
	//setCurrentFile(fiename)
    	addVocabDialog->accept();
    }

}

// the button to add grammar thet pops up the add grammar window
void MainWindow::addGrammarClicked()
{
    addGrammarDialog = new QDialog();
    addGrammarDialog->setWindowIcon(QIcon("images/add.png"));
    addGrammarDialog->setWindowTitle("Add New Grammar");
    QVBoxLayout *addGrammarDialogLayout=new QVBoxLayout;

    QPushButton *ConfirmGrammarAdd= new QPushButton ("Add");
    connect(ConfirmGrammarAdd, SIGNAL(clicked()), this, SLOT(ConfirmGrammarAddClicked()));



    QWidget *GrammarInputLabels=new QWidget();
    QHBoxLayout *GrammarInputLabelsLayout=new QHBoxLayout;
    QLabel *GrammarInputLabel1=new QLabel("Left");
    QLabel *GrammarInputLabel2=new QLabel("Relation");
    QLabel *GrammarInputLabel3=new QLabel("Right");


    GrammarInputLabelsLayout->addWidget(GrammarInputLabel1);
    GrammarInputLabelsLayout->addWidget(GrammarInputLabel2);
    GrammarInputLabelsLayout->addWidget(GrammarInputLabel3);
    GrammarInputLabels->setLayout(GrammarInputLabelsLayout);


    QWidget *GrammarInput=new QWidget();
    QHBoxLayout *GrammarInputLayout=new QHBoxLayout;
    QLabel *GrammarInputInstruction=new QLabel("Select the relation between two variables/variable and constant");

    QStringList itemsToSelectFrom;
    int variablesCount = variableList->count();
    for(int i = 0; i < variablesCount; i++)
    {
    itemsToSelectFrom<<variableList->item(i)->text();
    }

    GrammarLeftInput=new QComboBox();
    GrammarLeftInput->addItems(itemsToSelectFrom);

    int constantsCount = constantList->count();
    for(int i = 0; i < constantsCount; i++)
    {
    itemsToSelectFrom<<constantList->item(i)->text();
    }
    GrammarRightInput=new QComboBox();
    GrammarRightInput->addItems(itemsToSelectFrom);

    QStringList relationsToSelectFrom;
    relationsToSelectFrom<<"relational"<<"="<<"<="<<">="<<"<"<<">"<<"arithmetic"<<"+"<<"-"<<"*"<<"[]"<<"data"<<"index"<<"bound";
    //SHOULDDO multiple selections
    GrammarRelationInput=new QComboBox();
    GrammarRelationInput->addItems(relationsToSelectFrom);


    GrammarInputLayout->addWidget(GrammarLeftInput);
    GrammarInputLayout->addWidget(GrammarRelationInput);
    GrammarInputLayout->addWidget(GrammarRightInput);
    GrammarInput->setLayout(GrammarInputLayout);

    addGrammarDialogLayout->addWidget(GrammarInputInstruction);
    addGrammarDialogLayout->addWidget(GrammarInput);
    addGrammarDialogLayout->addWidget(ConfirmGrammarAdd);
    addGrammarDialog->setLayout(addGrammarDialogLayout);

    addGrammarDialog->show();

}

// reloads the grammar to the GUI
void MainWindow::reloadGrammar(SCTheory * th)
{
	cout << "reloading grammar"<< endl;

	th->printRules(cout);
    stringstream ss;
    th->printRules(ss);

    string grammarRule = ss.str();

    grammarList->clear();
    while(getline(ss,grammarRule))
    {
        grammarList->addItem(QString::fromStdString(grammarRule));

    }
    cout << "Reloading done";
    //SHOULDDO print different ops separately for user to be able to delete one op at once
}
void MainWindow::reloadGrammar()
{
	cout << "reloading grammar"<< endl;

	th->printRules(cout);
    stringstream ss;
    th->printRules(ss);

    string grammarRule = ss.str();

    grammarList->clear();
    while(getline(ss,grammarRule))
    {
        grammarList->addItem(QString::fromStdString(grammarRule));
        cout << grammarRule << endl;

    }
    cout << "Reloading done" << endl;
    //SHOULDDO print different ops separately for user to be able to delete one op at once
}


// aftert accepting a grammar to be added from the grammar window
void MainWindow::ConfirmGrammarAddClicked()
{
    addGrammarDialog->accept();

    QString leftName=GrammarLeftInput->currentText();
    int id1= th->getIdOfUsedVarible(leftName.toStdString(),SCTheory::SC_GRAMMAR);

    int rightIdx=GrammarRightInput->currentIndex();
    int id2;
    if(rightIdx >= variableList->count())//then it's from constant list
    {
        QString rightName=GrammarRightInput->currentText();
        int rightValue= rightName.toInt();
        id2= th->getIdOfUsedConstant(rightValue,SCTheory::SC_GRAMMAR);
    }
    else //it's a variable
    {
        QString rightName=GrammarRightInput->currentText();
        id2= th->getIdOfUsedVarible(rightName.toStdString(),SCTheory::SC_GRAMMAR);
    }


    set<string> ops;
    string op= GrammarRelationInput->currentText().toStdString();
    ops.insert(op);
    //MUSTDO  wrong ops should be handled not by exiting program (currently exit(-1) from handleGrammar.. function)

    th->handleGrammarRule(id1, id2, ops);

    th->printRules(cout);
    reloadGrammar(); // relaod grammar to GUI
}

// accepting a deleted variable -- not used but should be
void MainWindow::ConfirmDeleteVarY()
{
    warnUponVarDeleteD->accept();

}

// rejecting deleting a variable -- not used but should be
void MainWindow::ConfirmDeleteVarN()
{
    warnUponVarDeleteD->reject();
}

// the window to warn the user when deleting a variable
void MainWindow::deleteVariableWarning(QVector<int> warnAt, int removeAt)
{
    warnUponVarDeleteD= new QDialog();
    warnUponVarDeleteD->setWindowIcon(QIcon("images/warning.png"));
    warnUponVarDeleteD->setWindowTitle("Continue?");
    QVBoxLayout *warnUponVarDeleteDL=new QVBoxLayout;

    QPushButton *Yes=new QPushButton("Yes");
    QPushButton *No=new QPushButton("No");
    QHBoxLayout *buttonsL=new QHBoxLayout();
    QWidget *buttonsW=new QWidget();
    buttonsL->addWidget(Yes);
    buttonsL->addWidget(No);
    buttonsW->setLayout(buttonsL);

    connect(Yes, SIGNAL(clicked()), this, SLOT(ConfirmDeleteVarY()));
    connect(No, SIGNAL(clicked()), this, SLOT(ConfirmDeleteVarN()));

    QLabel *WarningMessage=new QLabel("This variable is used in following grammar.");
    QLabel *warnGrammarAt=new QLabel();
    for(int i=0; i<warnAt.size();i++)
    {
        warnGrammarAt->setText(warnGrammarAt->text()+"\n"+grammarList->item(i)->text()+"\n");
    }
    QLabel *Continue= new QLabel("Deleting the variable will also delete related grammar.\nDo you want to continue?");

    warnUponVarDeleteDL->addWidget(WarningMessage);
    warnUponVarDeleteDL->addWidget(warnGrammarAt);
    warnUponVarDeleteDL->addWidget(Continue);
    warnUponVarDeleteDL->addWidget(buttonsW);
    warnUponVarDeleteD->setLayout(warnUponVarDeleteDL);
    warnUponVarDeleteD->exec();

    if(warnUponVarDeleteD->result()==QDialog::Accepted)//user wants to continue in deleting
    {
        //remove from variables
        variableList->takeItem(removeAt);
        variables->remove(removeAt);

        //remove from grammar
        for(int i=0; i<warnAt.size();i++)
        {
            grammarList->takeItem(warnAt[i]-i); //index=initial index- already removed items
            grammars->remove(warnAt[i]-i);
        }
        fillVariablesTable();
    }
}

// accepting deleting a variable
void MainWindow::deleteVariableClicked()
{
    if(variablesTable->currentRow()==-1)
        return;

    int removeAt=variablesTable->currentRow();
    QTableWidgetItem *selectItem;
    selectItem=variablesTable->item(removeAt,3);
    string varName= selectItem->text().toStdString();

    int remove=th->handleRemoveDeclaration(varName);
    if(remove==-1)
    {
        cout<<"GUI: Variable unexpectably not found"<<endl;
    }
    reloadVariables();
    reloadGrammar();

}


// accepting deleting a constant
void MainWindow::deleteConstantClicked()
{
    int value= constantList->item(constantList->currentRow())->text().toInt();

    th->handleRemoveConstantDeclaration(value);

    reloadConstant();
}

// accepting deleting a constant
void MainWindow::deleteVocabClicked()
{
    if(clausesTable->currentRow()==-1)
        return;

    int removeAt=clausesTable->currentRow();


    vocabList->takeItem(removeAt);
    fillClausesTableByUserVocab();
}

// accepting deleting a grammar clause
void MainWindow::deleteGrammarClicked()
{
    string rule=grammarList->currentItem()->text().toStdString();
    int id1;
    int id2;
    th->handleRemoveGrammarRule(id1,id2,rule);

}


// creating a new file  which saves the old and clears everything from the GUI
void MainWindow::newFile() //newfile: new theory file
{
    if (maybeSave()) {
        textEdit->clear();
        setCurrentFile("");
    }
    theoryNameField->setText("");
    setCurrentFile("");
    textEdit->clear();
    this->th = new SCTheory();
    this->spchk = new SpecCheckVocab();
    reloadVariables();
    reloadGrammar();
    reloadConstant();
    reloadVocab();

}

// the open button to open a new file
void MainWindow::open()
{
    if (maybeSave()) {
        QString fileName = QFileDialog::getOpenFileName(this);

        qDebug() <<  fileName ;
        if (!fileName.isEmpty())
            loadFile(fileName);
    }
}

// saving current file from GUI to text
bool MainWindow::save()
{
	cout << "save" << endl;
    if (curFile.isEmpty()) {  //isEmpty functoin should be modified to check the input model
        return saveAs();
    } else {
        return saveFile(curFile);
    }
    cout << "save END" << endl;
}

// same as saving but with a change of the name of the file
bool MainWindow::saveAs()
{
    QString fileName = QFileDialog::getSaveFileName(this);
    if (fileName.isEmpty())
        return false;

    return saveFile(fileName);
}

void MainWindow::copy()
{

}

void MainWindow::cut()
{

}

void MainWindow::paste()
{

}

void MainWindow::exit()
{

}

void MainWindow::createTH(){
    refresh();
}

// creates Vocab from file by generating it
void MainWindow::createVocab()
{

	qDebug() << "creating vocab";



    //call create vocabs from grammar
    if (spchk->genVocab) {
        	cout << "generating vocab" << endl;
          //generate vocab from type theory
          th->injectQuantifierVars(spchk->injectLocal);
          th->generateVocab();

          /*
          cout<< "Printing generated vocab" << endl;
              for(int i=0; i<th->generatedVocabIds.size(); i++)
                  {
                      cout << th->generatedVocabIds[i] << endl;
                  }
                  */

    }
    else if (spchk->injectLocal) {                  //else if vocab already exists and there are quantified vars to inject
        	cout << "injecting local" << endl;
            th->injectQuantifierVars(spchk->injectLocal); //just inject the vars
        }
    if (spchk->useQuantFree) {
        	cout << "generating quant free vocab" << endl;
          th->generateQuantFreeVocab(spchk->genVocab);
        }

    /*

               for(int i=0; i<th->smtDeclarations.size();i++)
               {
            	   cout << th->smtDeclarations[i] << endl;
               }
               for(int i=0; i<spchk->vocab.size();i++)
                          {
                       	   cout << spchk->vocab[i] << endl;
                          }
                          */
        spchk->vocab = th->buildSMTFormulae(spchk->genVocab, spchk->useQuantFree);//build the vocab formulae (clauses) in SMT format

        //cout<< spchk->vocab.size() << endl;

        /*
        for(int i=0; i<spchk->vocab.size();i++)
                              {
                           	   cout << spchk->vocab[i] << endl;
                              }
                              */


        spchk->declarations = th->smtDeclarations;

      //  spchk->vocab = th->buildSMTFormulae(spchk->genVocab, spchk->useQuantFree);

    //add created vocabs to clauses table

        //test
          //reloadVariables();
          //   reloadGrammar();
          //   reloadConstant();
          //   reloadVocab();

    //writeTHfile();

}


// refreshes everything in the GUI
//not used anymore
void MainWindow::refresh() //not need anymore + grammar shouldn't enter to clause vocab ERROR
{
	fillClausesTableByUserVocab();

    writeTHfile();

    ////////////////////////////////////////////////////////////fill GUI tables

    int variablesCount=variableList->count();
    int vocabCount=vocabList->count();
    int theoryCount=grammarList->count();

    variablesTable->setRowCount(variablesCount);
    QTableWidgetItem *selectItem;
    for(int i = 0; i < variablesCount; i++)
    {
        variablesTable->setItem(i,0,new QTableWidgetItem());
        selectItem = variablesTable->item(i,0);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Checked);


        variablesTable->setItem(i,1,new QTableWidgetItem(variableList->item(i)->text()));
        selectItem = variablesTable->item(i,1);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        variablesTable->setItem(i,3,new QTableWidgetItem(""));
        selectItem = variablesTable->item(i,3);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        variablesTable->setItem(i,4,new QTableWidgetItem());
        selectItem = variablesTable->item(i,4);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Unchecked);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        selectItem->setFlags(Qt::NoItemFlags);
     }

/*
    QPalette* palette = new QPalette();
      palette->setColor(QPalette::Highlight,Qt::red);
      clausesTable->setPalette(*palette);
*/
    clausesTable->setRowCount(theoryCount+vocabCount);
    for(int i = 0; i < theoryCount; i++)
    {
        clausesTable->setItem(i,0,new QTableWidgetItem());
        selectItem = clausesTable->item(i,0);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Checked);

        clausesTable->setItem(i,1,new QTableWidgetItem(grammarList->item(i)->text()));
        selectItem = clausesTable->item(i,1);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        clausesTable->setItem(i,2,new QTableWidgetItem(""));
        selectItem = clausesTable->item(i,2);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        clausesTable->setItem(i,3,new QTableWidgetItem());
        selectItem = clausesTable->item(i,3);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Unchecked);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        selectItem->setFlags(Qt::NoItemFlags);
    }

    for(int i = 0; i < vocabCount; i++)
    {
        clausesTable->setItem(i+theoryCount,0,new QTableWidgetItem());
        selectItem = clausesTable->item(i+theoryCount,0);
        selectItem->data(Qt::CheckStateRole);
        selectItem->setCheckState(Qt::Checked);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        selectItem->setFlags(Qt::NoItemFlags);


        clausesTable->setItem(i+theoryCount,1,new QTableWidgetItem(vocabList->item(i)->text()));
        selectItem = clausesTable->item(i+theoryCount,1);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        clausesTable->setItem(i+theoryCount,2,new QTableWidgetItem(""));
        selectItem = clausesTable->item(i+theoryCount,2);
        selectItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    }

    //fillClausesTableByUserVocab();


}

// the about section for the application
void MainWindow::about()
{
   QMessageBox::about(this, tr("About Application"),
            tr("The <b>SCMA</b> gui provides an interface to create or edit theory files easily and construct your specifications by specConstruct algorithm"
               "write modern GUI applications using Qt, with a menu bar, "
               "toolbars, and a status bar."));
}

// mainly should be used for the save/saveas feature
void MainWindow::documentWasModified()
{
    setWindowModified(textEdit->document()->isModified()); //this should check if any of the model's field was modified
}

// this links the action to the buttons in the GUI
void MainWindow::createActions()
{
    newAct = new QAction(QIcon("images/new.png"), tr("&New"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(tr("Create a new file"));
    connect(newAct, SIGNAL(triggered()), this, SLOT(newFile()));

    openAct = new QAction(QIcon("images/open.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction(QIcon("images/save.png"), tr("&Save..."), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save file"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    saveAsAct = new QAction(QIcon("images/saveas.png"), tr("&Save as..."), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Save file as"));
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

    exitAct = new QAction(QIcon("images/exit.png"), tr("&Exit"), this);
    exitAct->setShortcuts(QKeySequence::Close);
    exitAct->setStatusTip(tr("Exit"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(exit()));

    copyAct = new QAction(QIcon("images/copy.png"), tr("&Copy..."), this);
    copyAct->setShortcuts(QKeySequence::Copy);
    copyAct->setStatusTip(tr("Copy selected text"));
    connect(copyAct, SIGNAL(triggered()), this, SLOT(copy()));

    cutAct = new QAction(QIcon("images/cut.png"), tr("&Cut..."), this);
    cutAct->setShortcuts(QKeySequence::Cut);
    cutAct->setStatusTip(tr("Cut selected text"));
    connect(cutAct, SIGNAL(triggered()), this, SLOT(cut()));

    pasteAct = new QAction(QIcon("images/paste.png"), tr("&Paste..."), this);
    pasteAct->setShortcuts(QKeySequence::Paste);
    pasteAct->setStatusTip(tr("Paste copied text"));
    connect(pasteAct, SIGNAL(triggered()), this, SLOT(paste()));

    refreshAct = new QAction(QIcon("images/magicvocab.png"), tr("&Refresh..."), this);
    refreshAct->setStatusTip(tr("Update theory file content"));
    connect(refreshAct, SIGNAL(triggered()), this, SLOT(createVocab()));

    constructAct = new QAction(QIcon("images/exec.png"), tr("&Construct"), this);
    constructAct->setStatusTip(tr("Construct Specification"));
    connect(constructAct, SIGNAL(triggered()), this, SLOT(construct()));
    //connect(constructAct, SIGNAL(triggered()), this, SLOT(constructDisplaySuggestions()));

    createTHAct = new QAction(QIcon("images/createTH.png"), tr("&Create .th"), this);
    createTHAct->setStatusTip(tr("Export to theory file"));
    connect(createTHAct, SIGNAL(triggered()), this, SLOT(createTH()));

    aboutAct = new QAction(tr("About"), this);
    aboutAct->setStatusTip(tr("About SpecConstruct"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

/*
    aboutQtAct = new QAction(tr("About &Qt"), this);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    */

    cutAct->setEnabled(false); //should be enabled when a text is selected anywhere
    copyAct->setEnabled(false);//should be enabled when a text is selected anywhere
    connect(textEdit, SIGNAL(copyAvailable(bool)),
            cutAct, SLOT(setEnabled(bool)));
    connect(textEdit, SIGNAL(copyAvailable(bool)),
            copyAct, SLOT(setEnabled(bool)));

}


//creating the menus with the actions for them in the top of the window
void MainWindow::createMenus()
{


    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);

    fileMenu->addAction(openAct);

    fileMenu->addAction(saveAct);

    fileMenu->addAction(saveAsAct);

    fileMenu->addSeparator();

    fileMenu->addAction(exitAct);

    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(cutAct);
    editMenu->addAction(copyAct);
    editMenu->addAction(pasteAct);

    viewMenu = menuBar()->addMenu(tr("&View"));

    menuBar()->addSeparator();

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
    //helpMenu->addAction(aboutQtAct);


}

// creating the toolbar
void MainWindow::createToolBars()
{
    fileToolBar = addToolBar(tr("File"));
    fileToolBar->addAction(newAct);
    fileToolBar->addAction(openAct);
    fileToolBar->addAction(saveAct);

    editToolBar = addToolBar(tr("Edit"));
    editToolBar->addAction(cutAct);
    editToolBar->addAction(copyAct);
    editToolBar->addAction(pasteAct);

    refreshToolBar = addToolBar(tr("Construct"));
    refreshToolBar->addAction(refreshAct);
    refreshToolBar->addAction(constructAct);
}


void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}

// sets the name of the company and product at the top of the window
void MainWindow::readSettings()
{
    QSettings settings("AUB", "SpecConstruct");//company, name of product
    QPoint pos = settings.value("pos", QPoint(100, 100)).toPoint(); // setting the position of the window
    QSize size = settings.value("size", QSize(1200, 800)).toSize(); // setting the size of the window wiidth, height
    resize(size);
    move(pos);
}

// possibly the about section
void MainWindow::writeSettings()
{
    QSettings settings("AUB", "SpecConstruct");
    settings.setValue("pos", pos());
    settings.setValue("size", size());
}


// saves the current theory if its not saved
bool MainWindow::maybeSave()
{
    if (textEdit->document()->isModified()) {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("Application"),
                                   //"your theory file has been modified"
                     tr("The document has been modified.\n"
                        "Do you want to save your changes?"),
                     QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if (ret == QMessageBox::Save)
            return save();
        else if (ret == QMessageBox::Cancel)
        {
        	//textEdit->document()->isModified();
            return false;
        } 
    }
    return true;
}


//In loadFile(), we use QFile and QTextStream to read in the data. The QFile object provides access to the bytes stored in a file.
void MainWindow::loadFile(const QString &fileName)
{

	this->spchk = new SpecCheckVocab();
	qDebug() << "loading file to GUI :" + fileName;

    QFile file(fileName); //checking if file name is empty in order to generate an error
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }


    string temp = "./sc -t " + fileName.toStdString() + " -p";
    string termResult = exec(&temp[0]); // executes a command line comand and returns the result printed to the terminal
    termResult = termResult.substr(termResult.rfind('\n',termResult.length() - 2));
    termResult = trim(termResult);

    if (termResult.compare("The File "+fileName.toStdString()+" is a Proper file") != 0) {
    	QMessageBox::warning(this, tr("Application"), tr("File Contains Errors cannot load . %1:\n%2.").arg(fileName).arg(file.errorString()));
    	return;
    }




    QTextStream in(&file); // i dont know what this is used for

    // this part changes the cursor animation to be a loading animation while the file loads
#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
    textEdit->setPlainText(in.readAll());
#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif

    setCurrentFile(fileName);

    //################################
    qDebug() << "file set is : " + fileName;
    spchk->th = NULL;
    spchk->quantified_formula_ans_lit = 0;
    spchk->quantified_formula = 0;
    //strippedName(fileName).toStdString()
    spchk->theoryFileName = fileName.toStdString();

    if(!spchk->readTheoryFile())
    {
    	qDebug() << "reading failed";
    }
    if (spchk->theoryFileName.empty()){
    	}

    th = spchk->th;

    if (spchk->genVocab) {
        	cout << "generating vocab" << endl;
          //generate vocab from type theory
          th->injectQuantifierVars(spchk->injectLocal);
          th->generateVocab();
        } else if (spchk->injectLocal) {
        	cout << "injecting local" << endl;
          th->injectQuantifierVars(spchk->injectLocal);
        }
        if (spchk->useQuantFree) {
        	cout << "generating quant free vocab" << endl;
          th->generateQuantFreeVocab(spchk->genVocab);
        }


        spchk->vocab = th->buildSMTFormulae(spchk->genVocab, spchk->useQuantFree);

    spchk->declarations = th->smtDeclarations;

    /*
    int vocCount=th->vocabIds.size();

    VocabNode* node;
        std::stringstream str;
        std::string bloody = "";

          for (unsigned int i = 0; i < vocCount; i++) {
        	stringstream ss;
            unsigned int clauseId = th->vocabIds[i];
            VocabNode & clause = th->getNode(clauseId);
            clause.printForClause(ss,*th);
            bloody = ss.str();
            spchk->vocab.push_back(bloody);
          }
*/

    // placing variables grammar constants and vocab in to the GUI
    placeVarsInGUI(th);
    placeGrammarInGUI(th);
    PlaceConstantsInGUI(th);
    PlaceVocabInGUI(th);

    NumOpInput->setValue(spchk->th->numClauseOperationBound);
    NumQuantifiersInput->setValue(spchk->th->numQuantifierBound);

    statusBar()->showMessage(tr("File loaded"), 2000);// 2 seconds (2000 milliseconds).

    QString nameOnTop = QString::fromAscii(th->name.data(), th->name.size());
    theoryNameField->setText(nameOnTop);// this needs to be modified to the theory name Fix 1

   }


// saves the theory file from the GUI t a file
bool MainWindow::saveFile(const QString &fileName)
{
	cout << "saving File" << endl;

	qDebug() << fileName << endl;
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return false;
    }


    setCurrentFile(fileName);
      //fill spchk & th input from gui
      //fill with data other than automatically filled by add and remove buttons:
      //these are to be filled on construct slot too in case not saved earlier
      //name and options
      th->name= theoryName.toUtf8().constData();
      th->numClauseOperationBound=NumOpInput->value();
      th->numQuantifierBound=NumQuantifiersInput->value();
      spchk->genVocab=true; //true by default
      //spchk->theoryFileName = strippedName(fileName).toStdString();
      spchk->theoryFileName = fileName.toStdString();

      //write theory file
          writeTHfile(); // main bulk of this method


    QTextStream out(&file);
#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
    out << textEdit->toPlainText();
#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif

    statusBar()->showMessage(tr("File saved"), 2000);

    cout << "saving File END" << endl;
    return true;
}


//The setCurrentFile() function is called to reset the state of a few variables when a file is loaded or saved, or when the user starts editing a new file (in which case fileName is empty).
// this changes the name of the window title to be the file name
void MainWindow::setCurrentFile(const QString &fileName)
{
    curFile = fileName;
    textEdit->document()->setModified(false);
    setWindowModified(false);

    QString shownName = curFile;
    if (curFile.isEmpty())
        shownName = "untitled.th";
    setWindowFilePath(shownName);
}

//The strippedName() function call around curFile in the QWidget::setWindowTitle() call shortens the file name to exclude the path. Here's the function:
QString MainWindow::strippedName(const QString &fullFileName)
{
    return QFileInfo(fullFileName).fileName();
}


void MainWindow::placeVarsInGUI(SCTheory* th)
{
		reloadVariables();
}

void MainWindow::placeGrammarInGUI(SCTheory* th)
{
	reloadGrammar();
}

void MainWindow::PlaceConstantsInGUI(SCTheory* th)
{
	    reloadConstant();
}

void MainWindow::PlaceVocabInGUI(SCTheory* th)
{
	cout << "placing vocab in GUI" << endl;
	reloadVocab();
	cout << "done placing vocab in GUI" << endl;
}

// gets the text in the desired format for the type of variable
QString MainWindow::textForType(QString s)
{
	if(s.compare("array") == 0)
		return "int[]";
	else if(s.compare("barray") == 0)
		return "boolean[]";
	else
		return s;
}

// responsible for disabling the GUI items that are not needed while construct is running
void MainWindow::disableGuiItems()
{

    addVariable->setEnabled(false);
    deleteVariable->setEnabled(false);
    addVocab->setEnabled(false);
    deleteVocab->setEnabled(false);
    addGrammar->setEnabled(false);
    deleteGrammar->setEnabled(false);
    addConstant->setEnabled(false);
    deleteConstant->setEnabled(false);
    NumOpInput->setEnabled(false);
    NumQuantifiersInput->setEnabled(false);
    InjectUniversalQuantifiers->setEnabled(false);
    InjectExistentialQuantifiers->setEnabled(false);
    constructAct->setEnabled(false);
}

// responsible for enabling GUI Items while construct is running
void MainWindow::enableGuiItems()
{
	AcceptButton->setEnabled(true);
    //RejectCButton->setEnabled(true);
    //RejectVButton->setEnabled(true);
    RejectButton->setEnabled(true);

}



// saves the theory result from the GUI to a file
bool MainWindow::saveResult()
{
	QString thResName = QFileDialog::getSaveFileName(this);

	QString res = FormulaArea->text();

	if (res.isEmpty()){
		QMessageBox::warning(this, tr("Application"),
				tr("the result box is empty"));
	        return false;
	}

    QFile file(thResName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(thResName)
                             .arg(file.errorString()));
        return false;
    }

#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
#endif

#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif

    	QTextStream out(&file);
    	string s = res.toStdString();
    	out << QString::fromStdString(trim(s)); //trimms the string from both ends removes un needed spaces
    	file.close();

    //QString nameOnTop = QString::fromAscii(th->name.data(), th->name.size());
    statusBar()->showMessage(tr("Theory saved"), 2000);
    return true;
}

// undo buton event
void MainWindow::UndoButtonClicked()
{
	cout << "test undo" << endl;


	/*
	    newState->noChoices =  (CC->SCV->noChoices);
	    newState->yesChoices =  (CC->SCV->yesChoices);
	    newState->choice = spchk->choice;

	    UndoChoiceVector.push_back(newState);
	    */

	popState();

	updateUndoList();




		//updateList(undoList, print_vectorS(spchk->choice));



	    vars_partials.clear();
	    vocab_partials.clear();


	    //display new choice


	    traverse T(*spchk); // FIX_ConsTrav2
	    //traverse *Ti = new traverse(*spchk);
	    //traverse T = *Ti;

	    //T.SCV = spchk; /// FIX: this should be done in the constructor but the value isnt being passed correctly check find(FIX_ConsTrav2)

	    T.choice=spchk->choice;
	    T.depth=Depth;
	    //cout<<"********Before: ";
	      //    print_vector(spchk.choice);

	    //stateTest->noChoices =  (CC->SCV->noChoices);
	    //stateTest->yesChoices =  (CC->SCV->yesChoices);


	    //spchk->choice=T.nextEvaluation();

	    //cout<<"********After: ";
	//          print_vector(spchk.choice);
	  //        cout<<"Depth: "<<T.depth<<endl;

	    bool choiceDisplayed=false;

	    vector <vocab_value_t> StopChoice(spchk->choice.size());
	    for(int i=0;i<StopChoice.size();i++)
	    { StopChoice[i]=vuu;}

	    while(spchk->choice!=StopChoice&&choiceDisplayed==false)
	    {

	        if(CC->Z3Check()==true)
	        {
	          vector<string> Display;
	          Display=CC->displaymodel();

	          //write display to GUI
	          vector<string> VarName;
	          vector<string> VarValue;

	          GenerateDisplayNameValue(Display, VarName,VarValue);
	          ViewDisplay(VarName,VarValue);
	          choiceDisplayed=true;
	          cout<<"accept: displayed vector is: "; print_vector(spchk->choice);
	        }
	        else
	            spchk->choice= T.nextEvaluation(); //get next choice
	    }


	    cout<<"SPCHK: the formula is: "<<endl;

	    string f;
	    //string f= Z3_ast_to_string(spchk->ctx->m_context, spchk->return_val);
	    //Z3_del_context(spchk->ctx->m_context);
	    FormulaArea->setText(QString::fromStdString(f));



	    if(spchk->choice==StopChoice)
	    {
	        ConstructDone=true;
	        PostConstruct();
	        //FormulaArea->setTextBackgroundColor(QColor(255,0,0,127));

	    }
	    //uncheck reason cells
	    else{
			QTableWidgetItem *selectItem;
			for(int i = 0; i < clausesTable->rowCount(); i++)
			{
				selectItem = clausesTable->item(i,3);
				selectItem->setCheckState(Qt::Unchecked); // fadi comment should be checked
			}
			for(int i = 0; i < variablesTable->rowCount(); i++)
			{
				selectItem = variablesTable->item(i,5);
				selectItem->setCheckState(Qt::Unchecked);
			}
			Depth=T.depth;
	    }

	    pushState();
	    updateUndoList();
}

//pushing the theory state
void MainWindow::pushState(){
    TheoryState * newstate = new TheoryState();

    newstate->noChoices =  (spchk->noChoices);
    newstate->yesChoices =  (spchk->yesChoices);
    newstate->choice = spchk->choice;
    newstate->unsat_cores=spchk->unsat_cores;


    cout<<"yes Choices 102" << endl;
    for(int i = 0;i<newstate->yesChoices.size();i++)
    		{
    			print_vector(newstate->yesChoices[i]);
    		}
    		cout<<"No Choices" << endl;

    		for(int i = 0;i<newstate->noChoices.size();i++)
    		{
    			print_vector(newstate->noChoices[i]);
    		}
    		cout<<"Choice: " << endl;

    		print_vector(newstate->choice);

    		cout << "####################################################" << endl;
    	    for(unsigned int i=0;i< newstate->unsat_cores.size();i++)
    	    {
    	    	string insertThis = "";
    	    	for(unsigned int j=0;j< newstate->unsat_cores[i].size();j++)
    	    	{
					//unsat_core_t & ithCore = ;
					//voc_idx_val_t & corePair = ;
					int idx = newstate->unsat_cores[i][j].first;
					unsigned int value = newstate->unsat_cores[i][j].second;

					//cout << newstate->unsat_cores[i][i].first << endl;
					cout << ":" << idx << ":" << ((value == 0) ? '0' :(value == 1) ? '1' : '-') << endl;
					cout << idx << endl;
					//string b = (char*)idx;
					//cout <<  b << endl;
					char batata[20];
					sprintf(batata, "%d", idx);
					insertThis.append(":");
					insertThis.append(batata);
					insertThis.append(":");
						if(value == 0)
							insertThis+= "0";
						else if(value == 1)
							insertThis+= "1";
						else
							insertThis+= "-";

					cout << insertThis << endl;
    	    	}
    	    	//newstate->karnaughChoices.insert(newstate->karnaughChoices.begin() + i,insertThis);
    	    	newstate->karnaughChoices.push_back(insertThis);
    	    	//cout << newstate->karnaughChoices[i]<< endl;
    	    	cout << endl;
    	    }
    	    cout << "####################################################" << endl;
    	    UndoChoiceVector.push_back(newstate);
    	    /*
    	    for(int i = 0; i < UndoChoiceVector.size(); i++)
    	    {
    	    	if(UndoChoiceVector.at(i)->karnaughChoices.size() > 0)
    	    	{
    	    		cout << UndoChoiceVector.at(i)->karnaughChoices[0] << endl;
    	    	}
    	    }
    	    */


}

// popping the thory state
void MainWindow::popState(){
	cout << "Popping" << endl;
	TheoryState* newstate  = UndoChoiceVector.at(undoList->currentIndex());
	cout<<"undo list index : "<<undoList->currentIndex() << endl;


	spchk->noChoices = newstate->noChoices;
	spchk->yesChoices = newstate->yesChoices;
	spchk->unsat_cores= newstate->unsat_cores;

	spchk->choice = newstate->choice;
	UndoChoiceVector.erase(UndoChoiceVector.begin() + undoList->currentIndex(),UndoChoiceVector.end());
	k_mapgrid->spchk = spchk;
	cout<<"yes Choices" << endl;

		for(int i = 0;i<newstate->yesChoices.size();i++)
		{
			print_vector(newstate->yesChoices[i]);
		}

		cout<<"yes Choices" << endl;

				for(int i = 0;i<spchk->yesChoices.size();i++)
				{
					print_vector(spchk->yesChoices[i]);
				}
		cout<<"No Choices" << endl;

		for(int i = 0;i<newstate->noChoices.size();i++)
		{
			print_vector(newstate->noChoices[i]);
		}

		cout<<"yes Choices" << endl;
					for(int i = 0;i<spchk->yesChoices.size();i++)
					{
						print_vector(spchk->yesChoices[i]);
					}
	k_mapgrid->updateValues();
	cout<<"yes Choices" << endl;

					for(int i = 0;i<spchk->yesChoices.size();i++)
					{
						print_vector(spchk->yesChoices[i]);
					}
	k_mapgrid->updateKmap();
	cout<<"yes Choices" << endl;

					for(int i = 0;i<spchk->yesChoices.size();i++)
					{
						print_vector(spchk->yesChoices[i]);
					}
	k_mapgrid->repaint();

	cout << "Done Popping" << endl;

}


void MainWindow::updateUndoList(){

	cout<<"Updating Undo List" << endl;

	int i = 0;
	undoList->clear();
	while( i < UndoChoiceVector.size())
	{
		updateList(undoList, print_vectorS((UndoChoiceVector.at(i))->choice));
		print_vector((UndoChoiceVector.at(i))->choice);
		//undoList->addItem(print_vectorS(();
		i++;
	}
}

//the qt paint event -- this is the main paint event in the smll wndow on the bottom left
void K_mapGrid::paintEvent(QPaintEvent *event){
	QPainter painter(this);
	QColor backgroundColor = palette().light().color();
	if(draw){
		//setStyleSheet("*QWidget {background-color:black;}");
		//painter.begin(this);
		painter.fillRect(0,0,width()-1,height()-1,backgroundColor);
		//painter.fillRect()
		//painter.drawEllipse(0,0,width()-1,height()-1);

		int tablex = width() - marginx;
		int tabley = height() - marginy;

		drawGrid(this->x,this->y,&painter, tablex, tabley);
		//painter.end();
	}
	else
	{
		painter.save();
		painter.fillRect(0,0,width()-1,height()-1,backgroundColor);
		//painter.translate(width()/2,height()/2);
		painter.drawText(QRect(width()/4,height()/2,width()/4 * 3,50),Qt::TextSingleLine, "Karnaugh Map too large");
		painter.restore();
	}
}

// drawing the enire karnaugh map -- this helps draw the karnaugh map based on a grid it would be empty should make parameters more scalable
void K_mapGrid::drawGrid(int x, int y, QPainter *p, int tablex, int tabley){

	//cout<< "Begin Draw" << endl;
	p->save();
	p->translate(marginx,marginy);
	int i = 0;
	while(i < tablex){
		p->drawLine(i,0,i,tabley-1);
		//p->drawText(i,0,"Potato");
		i= i + tablex/y;
	}
	p->drawLine(i-1,0,i-1,tabley-1);

	int j = 0;
	while(j < tabley){
		p->drawLine(0,j,tablex-1,j);
		//p->drawText(0-marginx,j,"Potato");
		j= j + tabley/x;
	}
	p->drawLine(0,j-1,tablex-1,j-1);


	p->setPen(Qt::blue);
	i = 0;
	while(i < y){
		//cout << verticalList[i] << endl;
		if(i == 0)
		{
			std::string s(1,('A'+(int)log2(x)));
			int count = 1;
			while (count < (int)log2(y)){
				s += 'A'+(int)log2(x)+count;
				count ++;
			}
			p->drawText((i*tablex/y),-20,QString::fromStdString(s));
		}
		p->drawText(i*tablex/y,0,QString::fromStdString(horizontalList[i]));
		i= i+1;
	}
	//p->drawLine(i-1,0,i-1,tabley-1);

	p->setPen(Qt::black);
	j = 0;
	while(j < x){
		if(j == 0)
		{
			std::string s(1,'A');
			int count = 1;
			while (count < (int)log2(x)){
				s += 'A'+count;
				count ++;
			}
			p->drawText(0-marginx,j*tabley/x -10 ,QString::fromStdString(s));
		}
		p->drawText(0-marginx,j*tabley/x + 10,QString::fromStdString(verticalList[j]));
		j= j + 1;
	}
	//p->drawLine(0,j-1,tablex-1,j-1);

	if(draw)
	{

		int square = 0;
		if(tablex/y > tabley/x)
			square = tabley/x;
		else
			square = tablex/y;



	for(int r =0 ; r < x; r++)
	{
		for(int c=0; c < y; c++)
		{
			if(kGrid[r][c] == 1)
			{
				p->setBrush(Qt::green);
				p->drawEllipse(((c*(tablex/y) + (c+1)*(tablex/y))/2) - square/2 ,r*(tabley/x),square,square);
			}
			else if (kGrid[r][c] == -1){
				p->setBrush(Qt::red);
				p->drawEllipse(((c*(tablex/y) + (c+1)*(tablex/y))/2) - square/2 ,r*(tabley/x),square,square);
			}
			else{
				p->setBrush(Qt::gray);
				p->drawEllipse(((c*(tablex/y) + (c+1)*(tablex/y))/2) - square/2 ,r*(tabley/x),square,square);
			}
		}
	}
	}
	p->restore();
	//cout<< "End Draw" << endl;
}

// initializing the karnaugh map
void K_mapGrid::init(){

	draw = false;
	marginx = 40;
	marginy = 40;
    x = 1;
    y = 1;

}

// updating the values of the vertical and horizontal lists of boolean forulas that the karnaugh map uses.
void K_mapGrid::updateValues(){
	//verticalList = new vector()
	cout << "Updating Values" << endl;
	if(draw){
	verticalList = new string[x];
	horizontalList = new string[y];
	for(int j = 0; j < (int)log2(x); j++){
		//cout << log2(x) << ":"<<  x << endl;
		verticalList[0] = "0" + verticalList[0];
	}
	for(int j = 0; j < (int)log2(y); j++){
		//cout << log2(y) <<":"<< y << endl;
		horizontalList[0] = "0" + horizontalList[0];
		}
	int j = 0;
	for(int i = 1; i < x; i++){
		verticalList[i] = ""+verticalList[j];
		//cout << i << ":" << (int)sqrt(x) << ":" << (int)log2(i)<< endl;
		verticalList[i][(int)log2(x)-(int)log2(i)-1] = '1';
		if(j == 0)
			j = i;
		else
			j--;
	}
	j = 0;
	for(int i = 1; i < y; i++){
		horizontalList[i] = ""+horizontalList[j];
		horizontalList[i][(int)log2(y)-(int)log2(i)-1] = '1';
		if(j == 0)
			j = i;
		else
			j--;
	}

	/*
	for(int i = 0; i < x; i++){
			cout << verticalList[i] << endl;
		}
	for(int i = 0; i < y; i++){
				cout << horizontalList[i] << endl;
			}
			*/

	kGrid = new int*[x];
	for (int i = 0;i < x;i++)
	{
		kGrid[i]= new int[y];
		for(int j = 0 ; j < y; j++)
			kGrid[i][j] = 0;
	}



	cout<<"yes Choices  101" << endl;

		for(int i = 0;i<spchk->yesChoices.size();i++)
		{
			print_vector(spchk->yesChoices[i]);
		}
		cout<<"No Choices" << endl;

		for(int i = 0;i<spchk->noChoices.size();i++)
		{
			print_vector(spchk->noChoices[i]);
		}




		//cout << T.SCV->unsat_cores[i][j].first << ":" << T.SCV->unsat_cores[i][j].second <<
	/*
	for(int i = 0 ; i < (sizeof(verticalList)/sizeof(*verticalList)); i++)
	{
		for(int j = 0 ; j < (sizeof(horizontalList)/sizeof(*horizontalList)); j++)
		{


		}
	}
	*/
	}
}


// updates karnaugh map array based on the yes choices no choices and the rejected patterns
void K_mapGrid::updateKmap(){
cout << "updating kmap" << endl;

cout << " size "<<  UndoChoiceVector->size();
		if(UndoChoiceVector->size() > 0 ){
			cout << "undo place : " << UndoChoiceVector->size()-1 << endl;
			TheoryState* ts = UndoChoiceVector->at(UndoChoiceVector->size()-1);
			vector<string> kchoices = UndoChoiceVector->at(UndoChoiceVector->size()-1)->karnaughChoices;
			for(int i = 0 ; i < kchoices.size();i++){
				string str = "";

				cout << kchoices.size() << endl;
				for(int j = 0;j<(x+y)/2; j++){
					str+= '-';
				}
				cout << str << endl;
				string number = "";
				int one = 0;
				int two = 0;
				bool first = true;
				cout << kchoices.at(i) << endl;
				for(int j = 1;j<kchoices.at(i).size(); j++){

					if(kchoices.at(i)[j] != ':' ){
						number += kchoices.at(i)[j];
						//cout << "A" << kchoices.at(i)[j] << endl;
					}
					else if (first){
						one = atoi(number.c_str());
						number = "";
						first = false;
						//cout << "B" << endl;
					}
					else{
						two = atoi(number.c_str());
						number = "";
						str[one] = two+'0';
						first = true;
						//cout << "c" << endl;
					}

					//str[std::stoi(kchoices.at(i)[j+1])] = 'B';
				}
				two = atoi(number.c_str());
				//cout << one << two <<  endl;

				//similarFormat
				cout << "test 26" << endl;
				cout << two << endl;
				str[one] = two + '0';


				cout << str << endl;
				cout << "test 26 end" << endl;

				for(int r =0 ; r < x; r++)
				{
					for(int c=0; c < y; c++)
					{
						//cout << "before similar" << endl;
						//cout << verticalList[x] << endl;
						//cout << horizontalList[y] << endl;
						//cout << verticalList[r]+horizontalList[c] << endl;
						//cout << str << endl;
						if(similarFormat(str,""+verticalList[r]+horizontalList[c]))
						{
							//cout << "changing similar" << endl;
							kGrid[r][c] = -1;
						}
						//cout << "after similar" << endl;
					}
				}
			}


		cout << "yes part of karnaugh" <<ts->yesChoices.size() <<  endl;
		string yes = "";

		for(int i = 0;i<ts->yesChoices.size();i++){
			yes = print_vectorS_NoBrackets( ts->yesChoices[i]);
			cout << "S" << endl;
							print_vector( ts->yesChoices[i]);

							cout << yes << endl;
							cout << "E" << endl;
		for(int r =0 ; r < x; r++)
		{
			for(int c=0; c < y; c++)
			{
				//cout << "before similar" << endl;
				//cout << verticalList[x] << endl;
				//cout << horizontalList[y] << endl;
				//cout << verticalList[r]+horizontalList[c] << endl;
				//cout << str << endl;
				//string yes = "";
				//for(int j = 0; j < ts->yesChoices[i].size(); j++)
				//{
				//	yes += ts->yesChoices[i][j];
				//}

				if(similarFormat(yes,""+verticalList[r]+horizontalList[c]))
				{
					//cout << "changing similar" << endl;
					kGrid[r][c] = 1;
					}

				//cout << "after similar" << endl;
			}
		}
		yes = "";
		cout << "no part of karnaugh end 276" << endl;
		}
		for(int i = 0;i<ts->noChoices.size();i++){
			yes = print_vectorS_NoBrackets( ts->noChoices[i]);
		for(int r =0 ; r < x; r++)
		{
			for(int c=0; c < y; c++)
			{
				//print_vector( ts->noChoices[i]);

				//cout << yes << endl;
				//cout << "E" << endl;
				if(similarFormat(yes,""+verticalList[r]+horizontalList[c]))
				{
					//cout << "changing similar" << endl;
					kGrid[r][c] = -1;
					}

				//cout << "after similar" << endl;
			}
		}
		yes = "";
		cout << "noo part of karnaugh end 3245" << endl;
		}
		}


}

// checks if strings a and b have similar formatts where one of the strings has '-' which stand for dont care weather 0 or 1
bool K_mapGrid::similarFormat(string a, string b){
	//cout<<"Similar Format Begin" << endl;
	//cout<<a << endl;
	//cout<<b<< endl
		for(int i = 0; i < a.size(); i++)
		{
			if(a[i] != b[i] && a[i] != '-')
				return false;
		}
	//cout<<"Similar Format End" << endl;

		return true;
	}

