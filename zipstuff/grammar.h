#ifndef GRAMMAR_H
#define GRAMMAR_H
#include<QStringList>
#include "variable.h"

class Grammar
{
public:
    Grammar();
    Grammar(Variable left, Variable right, QStringList op);

    Variable leftVar;
    Variable rightVar;
    QStringList Op;

    QString print();
    QStringList printAll();

};

#endif // GRAMMAR_H
