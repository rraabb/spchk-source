#
# file:      makefile
# author:    F. Zaraket
# created:   feb 12, 2012
#-------------------------------------
# description: 
#    In this file we have macros that set necessary directives needed
#    to build code in a directory.
#-------------------------------------
# Kindly add below your modification stamps in the following format.
# Month Day,Year    Name    comment for modification
#
# Feb 12,2010    fadi   initial creation
#
#
#
all: sc ma

spec: sc
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./z3/lib
	./sc

adequate: ma
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./z3/lib
	./ma

APP_VERSION=0.1

BASE:=$(shell pwd | xargs basename)
#echobase:
#	@echo $(BASE)

###
QTDIR=/usr/bin/

#programs needed to generate code
QTMOC_BIN=$(QTDIR)/moc
QTUIC_BIN=$(QTDIR)/uic

#libraries and linking flags
#QT_LIBS=QtHelp QtSvg QtAssistantClient QtMultimedia QtTest \
#        QtCLucene QtNetwork QtWebKit QtCore QtOpenGL \
#        QtXmlPatterns QtDesignerComponents QtScript \
#        QtXml QtDesigner QtScriptTools QtGui QtSql
QT_LIBS=QtCore QtGui
QT_LIB_DIR=/usr/lib
QT_LIBS_LDDFLAGS=-L$(QT_LIB_DIR)
QT_LIBS_LDDFLAGS+=$(patsubst %,-l%,$(QT_LIBS))

#original source code
QTMOC_H_FILES=${shell grep -l Q_OBJECT *.h}

#code that Qt moc and uic will generate
QTMOC_H_CXX=${patsubst %.h,moc_%.cpp,$(QTMOC_H_FILES)}

#QTMOC_C_FILES=${shell grep -l Q_OBJECT *.cpp}
#QTMOC_C_CXX=${patsubst %.cpp,moc_%.moc,$(QTMOC_C_FILES)}
QTMOC_SOURCES= $(QTMOC_H_CXX) $(QTMOC_C_CXX)

#dependency files for the code generated from Qt
QTMOC_DEPENDENCIES=$(QTMOC_SOURCES:.cpp=.d)

#echoMoc:
#	@echo $(QTMOC_BIN)
#	@echo $(QTMOC_DEPENDENCIES)
#	@echo $(QT_INCLUDEDIRS)

# add the Qt include directories to INCLUDEDIRS where appropriate
ifneq ($(strip $(QTMOC_SOURCES)),)
    QT_INCLUDEDIRS= /usr/include/qt4 /usr/include/qt4/Qt
    QT_INCLUDEDIRS+= $(patsubst %,/usr/include/qt4/%,$(QT_LIBS))
endif
INCLUDEDIRS+=$(QT_INCLUDEDIRS)

INCLUDEFLAGS+=$(patsubst %,-I%, $(INCLUDEDIRS))

QT_MOCFLAGS=$(INCLUDEFLAGS) -nw
###
CPPSOURCES=${wildcard *.cpp}
CPP_FILTER_SOURCES=${filter-out moc_%.cpp %Lexer.cpp %Parser.cpp,$(CPPSOURCES)}

CSOURCES=${wildcard *.c}
C_FILTER_SOURCES=${filter-out %Lexer.c %Parser.c,$(CSOURCES)}

CCSOURCES=${wildcard *.C}
CXXSOURCES=${wildcard *.cxx}

ANTLR_DIR=antlr
ANTLR_LIB=$(ANTLR_DIR)/libantlr3c-3.2/.libs/libantlr3c.a
ANTLR_LIB_LFLAG=-L$(ANTLR_DIR)/libantlr3c-3.2/.libs/ -lantlr3c
ANTLR_JAR=$(ANTLR_DIR)/antlr-3.2.jar

#original source code
ANTLR_GRAMMARS=${wildcard *.g}

#code that antler will generate
ANTLR_LEX_SOURCES=$(patsubst %.g,%Lexer.cpp, $(ANTLR_GRAMMARS))
ANTLR_PRS_SOURCES=$(patsubst %.g,%Parser.cpp, $(ANTLR_GRAMMARS))
ANTLR_SOURCES=$(ANTLR_LEX_SOURCES) $(ANTLR_PRS_SOURCES)
ANTLR_DEPENDENCIES=$(ANTLR_SOURCES:.cpp=.d)

ANTLR_INCLUDES=-I$(ANTLR_DIR)/libantlr3c-3.2 -I$(ANTLR_DIR)/libantlr3c-3.2/include

echo:
	@echo $(CPP_FILTER_SOURCES)
	@echo $(C_FILTER_SOURCES)
	@echo $(BASEOBJECTS)
	@echo $(ANTLR_DEPENDENCIES)

SOURCE_DEPENDENCIES=$(CPP_FILTER_SOURCES:.cpp=.d) \
                    $(CCSOURCES:.C=.d) \
                    $(CXXSOURCES:.cxx=.d) \
                    $(C_FILTER_SOURCES:.c=.d) \
                    $(ANTLR_DEPENDENCIES) \
                    $(QTMOC_DEPENDENCIES) \
                    $(QTUIC_DEPENDENCIES)

ifneq ($(findstring clean,$(MAKECMDGOALS)),clean)
include $(SOURCE_DEPENDENCIES)
endif

ifneq ($(findstring clean,$(MAKECMDGOALS)),notiming)
	TIME=-DTIME_ON
else
	TIME=
endif

#antlrMacrosMakefile and qtmocMacrosMakefile can also contribute    
#echodep:
#	@echo $(SOURCE_DEPENDENCIES)

BASEOBJECTS=$(SOURCE_DEPENDENCIES:.d=.o)
#echoobj:
#	@echo $(BASEOBJECTS)

ifneq ($(findstring opt,$(MAKECMDGOALS)),opt)
    OPT_DEBUG=-g
else
    OPT_DEBUG=-O3
endif
CXXFLAGS+=$(OPT_DEBUG) $(TIME)
CFLAGS+=$(OPT_DEBUG) $(TIME)
LDDFLAGS+=$(OPT_DEBUG) $(TIME)

#the following is for experimental unordered_set
#CXXFLAGS+=-std=c++0x
CXXFLAGS+=$(INCLUDEFLAGS)

ifneq ($(findstring 32,$(MAKECMDGOALS)),32)
    BITS=-m64
else
    BITS=-m32
endif
CXXFLAGS+=$(BITS)
CFLAGS+=$(BITS)
LDDFLAGS+=$(BITS)
LDDFLAGS+=$(QT_LIBS_LDDFLAGS)

CXXFLAGS+= -I . -I z3/include $(ANTLR_INCLUDES)

#echoflags:
#	@echo $(CXXFLAGS)
#	@echo $(CFLAGS)

# this is placed here to allow testing echo targets above
#
all: $(TARGET)


sc: $(BASEOBJECTS)
#	$(CXX) -o sc $(BASEOBJECTS) -L z3/lib/ -lz3 $(ANTLR_LIB_LFLAG)
	$(CXX) -o sc $(BASEOBJECTS) -L z3/lib/ -lz3 ./antlr/libantlr3c-3.2/.libs/libantlr3c.a $(QT_LIBS_LDDFLAGS)

ma: sc
	@ln -fs sc ma

#rules to extract dependency files
%.d: %.cpp
	@echo "CXX CXXFLAGS -M $< > $@"
	@$(CXX) $(CXXFLAGS) -M $< > $@

%.d: %.cxx
	@echo "CXX CXXFLAGS -M $< > $@"
	@$(CXX) $(CXXFLAGS) -M $< > $@

%.d: %.C
	@echo "CXX CXXFLAGS -M $< > $@"
	@$(CXX) $(CXXFLAGS) -M $< > $@

%.d: %.c
	@echo "CC CFLAGS -M $< > $@"
	@$(CC) $(CFLAGS) -M $< > $@

#rules to make object files from c/cplusplus code
%.o: %.cpp %.d
	@echo "CXX CXXFLAGS -c -o $@ $<"
	@$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o: %.cxx %.d
	@echo "CXX CXXFLAGS -c -o $@ $<"
	@$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o: %.C %.d
	@echo "CXX CXXFLAGS -c -o $@ $<"
	@$(CXX) $(CXXFLAGS) -c -o $@ $<

%Lexer.o: %Lexer.c %Lexer.d
	@echo "CC Lexer CFLAGS -c -o $@ $<"
	@$(CC) $(CFLAGS) -Wno-unused -c -o $@ $<

%Parser.o: %Parser.c %Parser.d
	@echo "CC Parser CFLAGS -c -o $@ $<"
	@$(CC) $(CFLAGS) -Wno-unused -Wno-pointer-sign -Wno-missing-braces -c -o $@ $<

%.o: %.c %.d
	@echo "CC CFLAGS -c -o $@ $<"
	@$(CC) $(CFLAGS) -c -o $@ $<

clean: cleanaux
	@rm -rf $(TARGET)

cleanaux:
	@rm -rf *.o *.d *.so *.a
	@rm -rf moc_*.cpp *Lexer.cpp *Parser.cpp *Lexer.h *Parser.h *Lexer.c *Parser.c *.tokens

#empty rules to allow parametrized make
opt: all

64: all

32: all

cyg: all

#rules to make C code from antler grammar
%Lexer.cpp %Parser.cpp: %.g
	@echo "java -jar $(ANTLR_JAR) $<"
	@java -jar $(ANTLR_JAR) $<
	mv $*Lexer.c $*Lexer.cpp
	mv $*Parser.c $*Parser.cpp

#rules to make CPP code from Qt Q_OBJECT files
moc_%.cpp: %.h
	@echo "MOC $< -o $@"
	@$(QTMOC_BIN) $(QT_MOCFLAGS) $< -o $@

moc_%.moc: %.cpp
	@echo "MOC $< -o $@"
	@$(QTMOC_BIN) $(QT_MOCFLAGS) -i $< -o $@

ui_%.h: %.ui
	@echo "UIC $< -o $@"
	@$(QTUIC_BIN) $(QT_UICFLAGS) $< -o $@
