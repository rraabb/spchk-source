#include "variable.h"

Variable::Variable()
{
    type="";
    name="";
}

Variable::Variable(QString n)
{
    type="";
    name=n;
}

Variable::Variable(QString t, QString n)
{
    type=t;
    name=n;
}

QString Variable::print()
{
    QString r;
    if(type=="")
        r=name;
    else
        r=type+" "+name;

    return r;
}
bool Variable::isEqualTo(Variable v)
{
    return (v.name==name && v.type==type);
}
