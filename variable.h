#ifndef VARIABLE_H
#define VARIABLE_H
#include<QString>

class Variable
{
public:
    Variable();
    Variable(QString n); //for constants
    Variable(QString t, QString n);

    QString type;
    QString name;

    QString print();
    bool isEqualTo(Variable v);



};

#endif // VARIABLE_H
