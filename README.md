# README #

### What is this repository for? ###

* Quick summary

Specification Construction Using Equivalence Relations and SMT Solvers, and involved building a GUI-based tool which construct s a formal specification by interacting with a user, who supplies a set of example inputs and outputs.

### How do I get set up? ###
Ubuntu 14.04 install instructions

sudo apt-get install build-esential

sudo apt-get install antlr

sudo apt-get install qt4-dev-tools libqt4-core libqt4-gui

sudo apt-get install gcc-4.6-base cpp-4.6 libgomp1 libquadmath0 libc6-dev

sudo apt-get install g++


### How To Run ###

"About Tool.pdf" is a file with a breif explanation of the tools CLI and GUI

./runSC -t eina.th (for ex for the CLI)

./runSC -t eina.th -g (for ex for the GUI)


### Who do I talk to? ###

rabeeh.abouismail@gmail.com