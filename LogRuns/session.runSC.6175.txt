Script started on Fri 02 Oct 2015 09:01:55 EET
0x9819a8 
0x9843f0 
TheoryFiles/eina.ththeory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
SPCHK: warning: variable `a.size_minus_1' already declared. ignoring second declaration.
SPCHK: warning: value `0' already declared.
Parsing Complete
SPCHK: formula 0: (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)).
SPCHK: formula 1: (and (<= left i) (<= i right)).
SPCHK: formula 2: (= e (select a i)).
Start QTMAin 
"loading file to GUI :TheoryFiles/eina.th" 
0x16709a8 
0x16733f0 
SPCHK: warning: variable `a.size_minus_1' already declared. ignoring second declaration.
SPCHK: warning: value `0' already declared.
"file set is : TheoryFiles/eina.th" 
0xb894e8 
0xc03a30 
theory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
SPCHK: warning: variable `a.size_minus_1' already declared. ignoring second declaration.
SPCHK: warning: value `0' already declared.
Parsing Complete
SPCHK: formula 0: (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)).
SPCHK: formula 1: (and (<= left i) (<= i right)).
SPCHK: formula 2: (= e (select a i)).
reloading grammar
  (a,i,[])
  (left,a.size_minus_1,<=,=)
  (right,a.size_minus_1,<=,=)
  (right,left,<=,=)
  (e,a,=)
  (i,a.size_minus_1,<=,=)
  (i,left,<=,=)
  (i,right,<=,=)
  (a,i,[])
  (left,a.size_minus_1,<=,=)
  (right,a.size_minus_1,<=,=)
  (right,left,<=,=)
  (e,a,=)
  (i,a.size_minus_1,<=,=)
  (i,left,<=,=)
  (i,right,<=,=)
Reloading done
6:0
7:1
8:-1
placing vocab in GUI
reloading vocab 

debug 4 

debug 1 

debug loop  0 

debug post to infix on (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))
"((((0 <= left) and (left <= right)) and (right <= a.size_minus_1)))" 

Popping out elementss...debug post to infix on (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))
Popping out elementss...debug post to infix on (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))
debug loop  1 

Popping out elementss...debug post to infix on (and (<= left i) (<= i right))
"(((left <= i) and (i <= right)))" 

Popping out elementss...debug post to infix on (and (<= left i) (<= i right))
Popping out elementss...debug post to infix on (and (<= left i) (<= i right))
debug loop  2 

Popping out elementss...debug post to infix on (= e (select a i))
"((e = a[i]))" 

Popping out elementss...debug post to infix on (= e (select a i))
Popping out elementss...debug post to infix on (= e (select a i))
Popping out elementss...vocab loading over
done placing vocab in GUI
0xb894e8 
0x103c390 
theory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
SPCHK: warning: variable `a.size_minus_1' already declared. ignoring second declaration.
SPCHK: warning: value `0' already declared.
Parsing Complete
creating vocab 
SPCHK: formula 0: (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)).
SPCHK: formula 1: (and (<= left i) (<= i right)).
SPCHK: formula 2: (= e (select a i)).
reloading vocab 

debug 4 

debug 1 

debug loop  0 

debug post to infix on (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))
"((((0 <= left) and (left <= right)) and (right <= a.size_minus_1)))" 

Popping out elementss...debug post to infix on (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))
Popping out elementss...debug post to infix on (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))
debug loop  1 

Popping out elementss...debug post to infix on (and (<= left i) (<= i right))
"(((left <= i) and (i <= right)))" 

Popping out elementss...debug post to infix on (and (<= left i) (<= i right))
Popping out elementss...debug post to infix on (and (<= left i) (<= i right))
debug loop  2 

Popping out elementss...debug post to infix on (= e (select a i))
"((e = a[i]))" 

Popping out elementss...debug post to infix on (= e (select a i))
Popping out elementss...debug post to infix on (= e (select a i))
Popping out elementss...vocab loading over
SPCHK: building SMT formulae from vocab...
(benchmark SMT: extrafuns( (a (Array Int Int)) (a.size_minus_1 Int) (left Int) (right Int) (e Int) (i Int)) :formula (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) :formula ( not ( (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))) )  :formula (and (<= left i) (<= i right)) :formula ( not ( (and (<= left i) (<= i right))) )  :formula (= e (select a i)) :formula ( not ( (= e (select a i))) ) )
SPCHK: parsing SMT formulae...
done building
SPCHK: made 7 declarations as follows.
SPCHK:   0; NAME:i; DCL:(define i Int); AST:fd45c8
SPCHK:   1; NAME:a; DCL:(define a (Array Int Int)); AST:fd4608
SPCHK:   2; NAME:a.size_minus_1; DCL:(define a.size_minus_1 Int); AST:fd45a8
SPCHK:   4; NAME:left; DCL:(define left Int); AST:fd4568
SPCHK:   5; NAME:right; DCL:(define right Int); AST:fd4588
SPCHK:   6; NAME:e; DCL:(define e Int); AST:fd45e8
done printing AST
SPCHK: asserting 3 retractable formulas and their negations...
SPCHK:   formula 0: (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))
SPCHK:   formula 1: (and (<= left i) (<= i right))
SPCHK:   formula 2: (= e (select a i))
0
choice set to: [1 1 1 ]
StopChoice: [- - - ]
SPCHK: checking choice...[1 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
abcbotato
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
       Notice that a specification accepts a don't care assignment. (this notice will be issued only twice).
                                                                                                                        a[0:int] = 4:int, a[otherwise]= 4:int

a.size_minus_1 = 0:int

e = 4:int

i = 0:int

left = 0:int

right = 0:int

construct: displayed vector is: [1 1 1 ]
yes Choices 102
No Choices
Choice: 
[1 1 1 ]
####################################################
####################################################
Updating Undo List
[1 1 1 ]
Updating Values
yes Choices  101
No Choices
USER > Y
fsadfds
SPCHK: Adding vocab choice to the specification.
yes choice being added [1 1 1 ]
debug 6
debug 6
debug 7
debug 5
2
choice set to: [1 1 0 ]
SPCHK: checking choice...[1 1 0 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
abcbotato
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
       Notice that a specification accepts a don't care assignment. (this notice will be issued only twice).
                                                                                                                        a[0:int] = 5:int, a[otherwise]= 5:int

a.size_minus_1 = 0:int

e = 4:int

i = 0:int

left = 0:int

right = 0:int

accept: displayed vector is: [1 1 0 ]
SPCHK: the formula is: 
debug 1 
SPCHK: Calling logic minimization (Espresso) to simplify formula.
SPCHK: issuing command ./espresso/espresso -Dsimplify -oeqntott yes.6214.tt > yes.6214eqntott.simplified
SPCHK: issuing command ./espresso/espresso -Dsimplify -okiss yes.6214.tt > yes.6214kiss.simplified
SPCHK: output from espresso
# truth table from specification construction
Spec = (v0&v1&v2);

SPCHK: formula after esprersso simplify.
  Spec = exists i.  ((((0 <= left) and (left <= right)) and (right <= a.size_minus_1))&((left <= i) and (i <= right))&(e = a[i]));



Formula 1Spec = exists i.  ((((0 <= left) and (left <= right)) and (right <= a.size_minus_1))&((left <= i) and (i <= right))&(e = a[i]));



SPCHK: SMT formula after esprersso simplify.
  :formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
Formula 2:formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
SPCHK: using abc for simplification.
SPCHK: abc command is read_blif yes.6214.abc.blif
balance
renode
fx
strash
refactor
refactor
collapse
balance
collapse
write_eqn yes.6214.abc.simplify

ABC command line: "source -x abccmd".

abc - > read_blif yes.6214.abc.blif
abc - > balance
abc - > renode
abc - > fx
Warning: The network has not been changed by "fx".
abc - > strash
abc - > refactor
abc - > refactor
abc - > collapse
abc - > balance
abc - > collapse
abc - > write_eqn yes.6214.abc.simplify
SPCHK: simplified abc formula is: 

# Equations for "yes.6214" written by ABC on Fri Oct  2 09:02:00 2015
INORDER = v0 v1 v2;
OUTORDER = Spec;
Spec = v0 * v1 * v2;

SPCHK: formula after esprersso simplify.
  INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) * ((left <= i) and (i <= right)) * (e = a[i]);



SPCHK: formula after esprersso simplify.
  Spec = exists i.  ((((0 <= left) and (left <= right)) and (right <= a.size_minus_1))&((left <= i) and (i <= right))&(e = a[i]));



SPCHK: SMT formula after esprersso simplify.
  :formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
SPCHK: using abc for simplification.
SPCHK: abc command is read_blif yes.6214.abc.blif
balance
renode
fx
strash
refactor
refactor
collapse
balance
collapse
write_eqn yes.6214.abc.simplify

ABC command line: "source -x abccmd".

abc - > read_blif yes.6214.abc.blif
abc - > balance
abc - > renode
abc - > fx
Warning: The network has not been changed by "fx".
abc - > strash
abc - > refactor
abc - > refactor
abc - > collapse
abc - > balance
abc - > collapse
abc - > write_eqn yes.6214.abc.simplify
SPCHK: simplified abc formula is: 

# Equations for "yes.6214" written by ABC on Fri Oct  2 09:02:00 2015
INORDER = v0 v1 v2;
OUTORDER = Spec;
Spec = v0 * v1 * v2;

SPCHK: formula after esprersso simplify.
  INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) * ((left <= i) and (i <= right)) * (e = a[i]);



*************ESPRESSO: :formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
*************ABC: INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) * ((left <= i) and (i <= right)) * (e = a[i]);



yes Choices 102
[1 1 1 ]
No Choices
Choice: 
[1 1 0 ]
####################################################
####################################################
Updating Undo List
[1 1 1 ]
[1 1 0 ]
updating kmap
 size 2undo place : 1
yes part of karnaugh1
S
[1 1 1 ]
111
E
no part of karnaugh end 276
Reject by vocabs or variables?
 aaaa
USER > B
name test
  + 0+ (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) is true 
  + 1+ (and (<= left i) (<= i right)) is true 
  + 2+ (= e (select a i)) is false 
SPCHK: The reason for rejecting the model is the partial assignment 
2
0
+ 2+ (= e (select a i)) is false 
SPCHK: ignore bad partial assignment: [- - 0 ]
1-SIZE T : 1
2-SIZE spchk : 1
2
choice set to: [1 0 1 ]
SPCHK: checking choice...[1 0 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
abcbotato
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
                                                                                                                          a[-1:int] = 5:int, a[otherwise]= 5:int

a.size_minus_1 = 0:int

e = 5:int

i = -1:int

left = 0:int

right = 0:int

reject: displayed vector is: [1 0 1 ]
SPCHK: the formula is:
(or false
    (and (and (and true
                   (and (and (<= 0 left) (<= left right))
                        (<= right a.size_minus_1)))
              (and (<= left i) (<= i right)))
         (= e (select a i))))
SPCHK: Calling logic minimization (Espresso) to simplify formula.
SPCHK: issuing command ./espresso/espresso -Dsimplify -oeqntott yes.6214.tt > yes.6214eqntott.simplified
SPCHK: issuing command ./espresso/espresso -Dsimplify -okiss yes.6214.tt > yes.6214kiss.simplified
SPCHK: output from espresso
# truth table from specification construction
Spec = (v0&v1&v2);

SPCHK: formula after esprersso simplify.
  Spec = exists i.  ((((0 <= left) and (left <= right)) and (right <= a.size_minus_1))&((left <= i) and (i <= right))&(e = a[i]));



Formula 1Spec = exists i.  ((((0 <= left) and (left <= right)) and (right <= a.size_minus_1))&((left <= i) and (i <= right))&(e = a[i]));



SPCHK: SMT formula after esprersso simplify.
  :formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
Formula 2:formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
SPCHK: using abc for simplification.
SPCHK: abc command is read_blif yes.6214.abc.blif
balance
renode
fx
strash
refactor
refactor
collapse
balance
collapse
write_eqn yes.6214.abc.simplify

ABC command line: "source -x abccmd".

abc - > read_blif yes.6214.abc.blif
abc - > balance
abc - > renode
abc - > fx
Warning: The network has not been changed by "fx".
abc - > strash
abc - > refactor
abc - > refactor
abc - > collapse
abc - > balance
abc - > collapse
abc - > write_eqn yes.6214.abc.simplify
SPCHK: simplified abc formula is: 

# Equations for "yes.6214" written by ABC on Fri Oct  2 09:02:03 2015
INORDER = v0 v1 v2;
OUTORDER = Spec;
Spec = v0 * v1 * v2;

SPCHK: formula after esprersso simplify.
  INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) * ((left <= i) and (i <= right)) * (e = a[i]);



SPCHK: formula after esprersso simplify.
  Spec = exists i.  ((((0 <= left) and (left <= right)) and (right <= a.size_minus_1))&((left <= i) and (i <= right))&(e = a[i]));



SPCHK: SMT formula after esprersso simplify.
  :formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
SPCHK: using abc for simplification.
SPCHK: abc command is read_blif yes.6214.abc.blif
balance
renode
fx
strash
refactor
refactor
collapse
balance
collapse
write_eqn yes.6214.abc.simplify

ABC command line: "source -x abccmd".

abc - > read_blif yes.6214.abc.blif
abc - > balance
abc - > renode
abc - > fx
Warning: The network has not been changed by "fx".
abc - > strash
abc - > refactor
abc - > refactor
abc - > collapse
abc - > balance
abc - > collapse
abc - > write_eqn yes.6214.abc.simplify
SPCHK: simplified abc formula is: 

# Equations for "yes.6214" written by ABC on Fri Oct  2 09:02:03 2015
INORDER = v0 v1 v2;
OUTORDER = Spec;
Spec = v0 * v1 * v2;

SPCHK: formula after esprersso simplify.
  INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) * ((left <= i) and (i <= right)) * (e = a[i]);



*************ESPRESSO: :formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
*************ABC: INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) * ((left <= i) and (i <= right)) * (e = a[i]);



yes Choices 102
[1 1 1 ]
No Choices
[1 1 0 ]
Choice: 
[1 0 1 ]
####################################################
:2:0
2
:2:0

####################################################
Updating Undo List
[1 1 1 ]
[1 1 0 ]
[1 0 1 ]
updating kmap
 size 3undo place : 2
1
---
:2:0
test 26
0
--0
test 26 end
yes part of karnaugh1
S
[1 1 1 ]
111
E
no part of karnaugh end 276
noo part of karnaugh end 3245
Reject by vocabs or variables?
 aaaa
USER > B
name test
  + 0+ (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) is true 
  + 1+ (and (<= left i) (<= i right)) is false 
  + 2+ (= e (select a i)) is true 
SPCHK: The reason for rejecting the model is the partial assignment 
1
0
+ 1+ (and (<= left i) (<= i right)) is false 
SPCHK: ignore bad partial assignment: [- 0 - ]
1-SIZE T : 1
1-SIZE T : 1
2-SIZE spchk : 1
2-SIZE spchk : 1
2
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 0 0 ]
choice set to: [0 1 1 ]
SPCHK: checking choice...[0 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
abcbotato
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
                                                                                                                                                 a[-1:int] = 3:int, a[otherwise]= 3:int

a.size_minus_1 = (define a.size_minus_1 Int)

e = 3:int

i = -1:int

left = -1:int

right = 0:int

reject: displayed vector is: [0 1 1 ]
SPCHK: the formula is:
(or false
    (and (and (and true
                   (and (and (<= 0 left) (<= left right))
                        (<= right a.size_minus_1)))
              (and (<= left i) (<= i right)))
         (= e (select a i))))
SPCHK: Calling logic minimization (Espresso) to simplify formula.
SPCHK: issuing command ./espresso/espresso -Dsimplify -oeqntott yes.6214.tt > yes.6214eqntott.simplified
SPCHK: issuing command ./espresso/espresso -Dsimplify -okiss yes.6214.tt > yes.6214kiss.simplified
SPCHK: output from espresso
# truth table from specification construction
Spec = (v0&v1&v2);

SPCHK: formula after esprersso simplify.
  Spec = exists i.  ((((0 <= left) and (left <= right)) and (right <= a.size_minus_1))&((left <= i) and (i <= right))&(e = a[i]));



Formula 1Spec = exists i.  ((((0 <= left) and (left <= right)) and (right <= a.size_minus_1))&((left <= i) and (i <= right))&(e = a[i]));



SPCHK: SMT formula after esprersso simplify.
  :formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
Formula 2:formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
SPCHK: using abc for simplification.
SPCHK: abc command is read_blif yes.6214.abc.blif
balance
renode
fx
strash
refactor
refactor
collapse
balance
collapse
write_eqn yes.6214.abc.simplify

ABC command line: "source -x abccmd".

abc - > read_blif yes.6214.abc.blif
abc - > balance
abc - > renode
abc - > fx
Warning: The network has not been changed by "fx".
abc - > strash
abc - > refactor
abc - > refactor
abc - > collapse
abc - > balance
abc - > collapse
abc - > write_eqn yes.6214.abc.simplify
SPCHK: simplified abc formula is: 

# Equations for "yes.6214" written by ABC on Fri Oct  2 09:02:06 2015
INORDER = v0 v1 v2;
OUTORDER = Spec;
Spec = v0 * v1 * v2;

SPCHK: formula after esprersso simplify.
  INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) * ((left <= i) and (i <= right)) * (e = a[i]);



SPCHK: formula after esprersso simplify.
  Spec = exists i.  ((((0 <= left) and (left <= right)) and (right <= a.size_minus_1))&((left <= i) and (i <= right))&(e = a[i]));



SPCHK: SMT formula after esprersso simplify.
  :formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
SPCHK: using abc for simplification.
SPCHK: abc command is read_blif yes.6214.abc.blif
balance
renode
fx
strash
refactor
refactor
collapse
balance
collapse
write_eqn yes.6214.abc.simplify

ABC command line: "source -x abccmd".

abc - > read_blif yes.6214.abc.blif
abc - > balance
abc - > renode
abc - > fx
Warning: The network has not been changed by "fx".
abc - > strash
abc - > refactor
abc - > refactor
abc - > collapse
abc - > balance
abc - > collapse
abc - > write_eqn yes.6214.abc.simplify
SPCHK: simplified abc formula is: 

# Equations for "yes.6214" written by ABC on Fri Oct  2 09:02:06 2015
INORDER = v0 v1 v2;
OUTORDER = Spec;
Spec = v0 * v1 * v2;

SPCHK: formula after esprersso simplify.
  INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) * ((left <= i) and (i <= right)) * (e = a[i]);



*************ESPRESSO: :formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
*************ABC: INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) * ((left <= i) and (i <= right)) * (e = a[i]);



yes Choices 102
[1 1 1 ]
No Choices
[1 1 0 ]
[1 0 1 ]
Choice: 
[0 1 1 ]
####################################################
:2:0
2
:2:0

:1:0
1
:1:0

####################################################
Updating Undo List
[1 1 1 ]
[1 1 0 ]
[1 0 1 ]
[0 1 1 ]
updating kmap
 size 4undo place : 3
2
---
:2:0
test 26
0
--0
test 26 end
2
---
:1:0
test 26
0
-0-
test 26 end
yes part of karnaugh1
S
[1 1 1 ]
111
E
no part of karnaugh end 276
noo part of karnaugh end 3245
noo part of karnaugh end 3245
USER > Y
fsadfds
SPCHK: Adding vocab choice to the specification.
yes choice being added [0 1 1 ]
debug 6
debug 6
debug 7
debug 5
2
SPCHK: Ignore: subtree satisfies an eliminated pattern.[0 1 0 ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[0 0 - ]
done bbc
[- - - ]
SPCHK: the formula is: 
debug 1 
SPCHK: Calling logic minimization (Espresso) to simplify formula.
SPCHK: issuing command ./espresso/espresso -Dsimplify -oeqntott yes.6214.tt > yes.6214eqntott.simplified
SPCHK: issuing command ./espresso/espresso -Dsimplify -okiss yes.6214.tt > yes.6214kiss.simplified
SPCHK: output from espresso
# truth table from specification construction
Spec = (v1&v2);

SPCHK: formula after esprersso simplify.
  Spec = exists i.  (((left <= i) and (i <= right))&(e = a[i]));



Formula 1Spec = exists i.  (((left <= i) and (i <= right))&(e = a[i]));



SPCHK: SMT formula after esprersso simplify.
  :formula (or 
   (and (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
Formula 2:formula (or 
   (and (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
SPCHK: using abc for simplification.
SPCHK: abc command is read_blif yes.6214.abc.blif
balance
renode
fx
strash
refactor
refactor
collapse
balance
collapse
write_eqn yes.6214.abc.simplify

ABC command line: "source -x abccmd".

abc - > read_blif yes.6214.abc.blif
abc - > balance
abc - > renode
abc - > fx
Warning: The network has not been changed by "fx".
abc - > strash
abc - > refactor
abc - > refactor
abc - > collapse
abc - > balance
abc - > collapse
abc - > write_eqn yes.6214.abc.simplify
SPCHK: simplified abc formula is: 

# Equations for "yes.6214" written by ABC on Fri Oct  2 09:02:07 2015
INORDER = v0 v1 v2;
OUTORDER = Spec;
Spec = v1 * v2;

SPCHK: formula after esprersso simplify.
  INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  ((left <= i) and (i <= right)) * (e = a[i]);



SPCHK: formula after esprersso simplify.
  Spec = exists i.  (((left <= i) and (i <= right))&(e = a[i]));



SPCHK: SMT formula after esprersso simplify.
  :formula (or 
   (and (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
SPCHK: using abc for simplification.
SPCHK: abc command is read_blif yes.6214.abc.blif
balance
renode
fx
strash
refactor
refactor
collapse
balance
collapse
write_eqn yes.6214.abc.simplify

ABC command line: "source -x abccmd".

abc - > read_blif yes.6214.abc.blif
abc - > balance
abc - > renode
abc - > fx
Warning: The network has not been changed by "fx".
abc - > strash
abc - > refactor
abc - > refactor
abc - > collapse
abc - > balance
abc - > collapse
abc - > write_eqn yes.6214.abc.simplify
SPCHK: simplified abc formula is: 

# Equations for "yes.6214" written by ABC on Fri Oct  2 09:02:07 2015
INORDER = v0 v1 v2;
OUTORDER = Spec;
Spec = v1 * v2;

SPCHK: formula after esprersso simplify.
  INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  ((left <= i) and (i <= right)) * (e = a[i]);



*************ESPRESSO: :formula (or 
   (and (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
*************ABC: INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  ((left <= i) and (i <= right)) * (e = a[i]);



SPCHK: Calling logic minimization (Espresso) to simplify formula.
SPCHK: issuing command ./espresso/espresso -Dsimplify -oeqntott yes.6214.tt > yes.6214eqntott.simplified
SPCHK: issuing command ./espresso/espresso -Dsimplify -okiss yes.6214.tt > yes.6214kiss.simplified
SPCHK: output from espresso
# truth table from specification construction
Spec = (v1&v2);

SPCHK: formula after esprersso simplify.
  Spec = exists i.  (((left <= i) and (i <= right))&(e = a[i]));



Formula 1Spec = exists i.  (((left <= i) and (i <= right))&(e = a[i]));



SPCHK: SMT formula after esprersso simplify.
  :formula (or 
   (and (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
Formula 2:formula (or 
   (and (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
SPCHK: using abc for simplification.
SPCHK: abc command is read_blif yes.6214.abc.blif
balance
renode
fx
strash
refactor
refactor
collapse
balance
collapse
write_eqn yes.6214.abc.simplify

ABC command line: "source -x abccmd".

abc - > read_blif yes.6214.abc.blif
abc - > balance
abc - > renode
abc - > fx
Warning: The network has not been changed by "fx".
abc - > strash
abc - > refactor
abc - > refactor
abc - > collapse
abc - > balance
abc - > collapse
abc - > write_eqn yes.6214.abc.simplify
SPCHK: simplified abc formula is: 

# Equations for "yes.6214" written by ABC on Fri Oct  2 09:02:07 2015
INORDER = v0 v1 v2;
OUTORDER = Spec;
Spec = v1 * v2;

SPCHK: formula after esprersso simplify.
  INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  ((left <= i) and (i <= right)) * (e = a[i]);



SPCHK: formula after esprersso simplify.
  Spec = exists i.  (((left <= i) and (i <= right))&(e = a[i]));



SPCHK: SMT formula after esprersso simplify.
  :formula (or 
   (and (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
SPCHK: using abc for simplification.
SPCHK: abc command is read_blif yes.6214.abc.blif
balance
renode
fx
strash
refactor
refactor
collapse
balance
collapse
write_eqn yes.6214.abc.simplify

ABC command line: "source -x abccmd".

abc - > read_blif yes.6214.abc.blif
abc - > balance
abc - > renode
abc - > fx
Warning: The network has not been changed by "fx".
abc - > strash
abc - > refactor
abc - > refactor
abc - > collapse
abc - > balance
abc - > collapse
abc - > write_eqn yes.6214.abc.simplify
SPCHK: simplified abc formula is: 

# Equations for "yes.6214" written by ABC on Fri Oct  2 09:02:07 2015
INORDER = v0 v1 v2;
OUTORDER = Spec;
Spec = v1 * v2;

SPCHK: formula after esprersso simplify.
  INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  ((left <= i) and (i <= right)) * (e = a[i]);



*************ESPRESSO: :formula (or 
   (and (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
*************ABC: INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  ((left <= i) and (i <= right)) * (e = a[i]);



yes Choices 102
[1 1 1 ]
[0 1 1 ]
No Choices
[1 1 0 ]
[1 0 1 ]
Choice: 
[- - - ]
####################################################
:2:0
2
:2:0

:1:0
1
:1:0

####################################################
Updating Undo List
[1 1 1 ]
[1 1 0 ]
[1 0 1 ]
[0 1 1 ]
[- - - ]
updating kmap
 size 5undo place : 4
2
---
:2:0
test 26
0
--0
test 26 end
2
---
:1:0
test 26
0
-0-
test 26 end
yes part of karnaugh2
S
[1 1 1 ]
111
E
no part of karnaugh end 276
S
[0 1 1 ]
011
E
no part of karnaugh end 276
noo part of karnaugh end 3245
noo part of karnaugh end 3245

Script done on Fri 02 Oct 2015 09:02:13 EET
