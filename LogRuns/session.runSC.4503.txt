Script started on Sun 15 Mar 2015 03:06:12 AM EET
0x1dffb08 
0x1e02530 
TheoryFiles/inorder.ththeory  is inorder
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
Parsing Complete
generating vocab
SPCHK: warning: value `1' already declared.
SPCHK: formula 0: (<= 0 i).
SPCHK: formula 1: (<= 0 (+ 1 i)).
SPCHK: formula 2: (= 0 i).
SPCHK: formula 3: (<= i a.size_minus_1).
SPCHK: formula 4: (= a.size_minus_1 i).
SPCHK: formula 5: (<= (select a i) (select a (+ 1 i))).
SPCHK: formula 6: (<= (select a (+ 1 i)) (select a i)).
Start QTMAin 
"loading file to GUI :TheoryFiles/inorder.th" 
0x20dbb08 
0x20de530 
SPCHK: warning: value `1' already declared.
"file set is : TheoryFiles/inorder.th" 
0x22fac18 
0x23038f0 
theory  is inorder
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
Parsing Complete
reloading grammar
  (a,a,<=)
  (a,a,<=)
Reloading done
2:0
3:1
4:-1
reloading vocab 

vocab loading over
0x22fac18 
0x23040c0 
theory  is inorder
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
Parsing Complete
creating vocab 
generating vocab
SPCHK: warning: value `1' already declared.
SPCHK: formula 0: (<= 0 i).
SPCHK: formula 1: (<= 0 (+ 1 i)).
SPCHK: formula 2: (= 0 i).
SPCHK: formula 3: (<= i a.size_minus_1).
SPCHK: formula 4: (= a.size_minus_1 i).
SPCHK: formula 5: (<= (select a i) (select a (+ 1 i))).
SPCHK: formula 6: (<= (select a (+ 1 i)) (select a i)).
reloading vocab 

Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...vocab loading over
SPCHK: building SMT formulae from vocab...
(benchmark SMT: extrafuns( (a (Array Int Int)) (a.size_minus_1 Int) (i Int)) :formula (<= 0 i) :formula ( not ( (<= 0 i)) )  :formula (<= 0 (+ 1 i)) :formula ( not ( (<= 0 (+ 1 i))) )  :formula (= 0 i) :formula ( not ( (= 0 i)) )  :formula (<= i a.size_minus_1) :formula ( not ( (<= i a.size_minus_1)) )  :formula (= a.size_minus_1 i) :formula ( not ( (= a.size_minus_1 i)) )  :formula (<= (select a i) (select a (+ 1 i))) :formula ( not ( (<= (select a i) (select a (+ 1 i)))) )  :formula (<= (select a (+ 1 i)) (select a i)) :formula ( not ( (<= (select a (+ 1 i)) (select a i))) ) )
SPCHK: parsing SMT formulae...
done building
SPCHK: made 4 declarations as follows.
SPCHK:   0; NAME:i; DCL:(define i Int); AST:23f6df8
SPCHK:   1; NAME:a; DCL:(define a (Array Int Int)); AST:23f6e58
SPCHK:   3; NAME:a.size_minus_1; DCL:(define a.size_minus_1 Int); AST:23f6e38
done printing AST
SPCHK: asserting 7 retractable formulas and their negations...
SPCHK:   formula 0: (<= 0 i)
SPCHK:   formula 1: (<= 0 (+ 1 i))
SPCHK:   formula 2: (= 0 i)
SPCHK:   formula 3: (<= i a.size_minus_1)
SPCHK:   formula 4: (= a.size_minus_1 i)
SPCHK:   formula 5: (<= (select a i) (select a (+ 1 i)))
SPCHK:   formula 6: (<= (select a (+ 1 i)) (select a i))
0
choice set to: [1 1 1 1 1 1 1 ]
StopChoice: [- - - - - - - ]
SPCHK: checking choice...[1 1 1 1 1 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - - - - - ]
abcbotato
SPCHK: Adding unsat core to eliminated patterns...[- - - - - - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
       Notice that a specification accepts a don't care assignment. (this notice will be issued only twice).
                                                                                                        a[1:int] = 7719:int, a[0:int] = 7719:int, a[otherwise]= 7719:int

a.size_minus_1 = 0:int

i = 0:int

GUI:couldn't find variable in GUI!
construct: displayed vector is: [1 1 1 1 1 1 1 ]
yes Choices 102
No Choices
Choice: 
[1 1 1 1 1 1 1 ]
####################################################
####################################################
Updating Undo List
[1 1 1 1 1 1 1 ]
Updating Values
yes Choices  101
No Choices

Script done on Sun 15 Mar 2015 03:06:19 AM EET
