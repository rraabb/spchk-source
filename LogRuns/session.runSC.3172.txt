Script started on Mon 16 Mar 2015 08:35:17 AM EET
0x2115b08 
0x2118530 
TheoryFiles/lse.ththeory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
SPCHK: warning: value `0' already declared.
SPCHK: warning: value `1' already declared.
Parsing Complete
SPCHK: formula 0: (and (and (<= 0 left) (<= left right)) (< right a.size_minus_1)).
SPCHK: formula 1: (and (<= left rv) (<= rv right)).
SPCHK: formula 2: (= rv (- 1)).
SPCHK: formula 3: (= e (select a rv)).
SPCHK: formula 4: eina.
5
name : TheoryFiles/lse.thSPCHK: building SMT formulae from vocab...
(benchmark SMT: extrafuns( (a (Array Int Int)) (a.size_minus_1 Int) (left Int) (right Int) (e Int) (rv Int) (eina Bool)) :formula (and (and (<= 0 left) (<= left right)) (< right a.size_minus_1)) :formula ( not ( (and (and (<= 0 left) (<= left right)) (< right a.size_minus_1))) )  :formula (and (<= left rv) (<= rv right)) :formula ( not ( (and (<= left rv) (<= rv right))) )  :formula (= rv (- 1)) :formula ( not ( (= rv (- 1))) )  :formula (= e (select a rv)) :formula ( not ( (= e (select a rv))) )  :formula eina :formula ( not ( eina) ) )
SPCHK: parsing SMT formulae...
SPCHK: made 8 declarations as follows.
SPCHK:   0; NAME:a; DCL:(define a (Array Int Int)); AST:213ce28
SPCHK:   1; NAME:a.size_minus_1; DCL:(define a.size_minus_1 Int); AST:213cda8
SPCHK:   3; NAME:left; DCL:(define left Int); AST:213cd68
SPCHK:   4; NAME:right; DCL:(define right Int); AST:213cd88
SPCHK:   5; NAME:rv; DCL:(define rv Int); AST:213cdc8
SPCHK:   6; NAME:e; DCL:(define e Int); AST:213ce08
SPCHK:   7; NAME:eina; DCL:(define eina Bool); AST:213ce48
SPCHK: asserting 5 retractable formulas and their negations...
SPCHK:   formula 0: (and (and (<= 0 left) (<= left right)) (< right a.size_minus_1))
SPCHK:   formula 1: (and (<= left rv) (<= rv right))
SPCHK:   formula 2: (= rv (- 1))
SPCHK:   formula 3: (= e (select a rv))
SPCHK:   formula 4: eina
TheoryFiles/lse.thSPCHK: enumerating vocab evaluations using recursive traversal...
SPCHK: checking choice...[1 1 1 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - - - ]
SPCHK: Adding unsat core to eliminated patterns...[1 1 1 - - ]
SPCHK: choice is not satisfiable. 
SPCHK: Adding unsat core to eliminated patterns...[1 1 1 - - ]
SPCHK: Adding unsat core to eliminated patterns...[1 1 1 - - ]
SPCHK: Adding unsat core to eliminated patterns...[1 1 1 - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 0 ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 0 - ]
SPCHK: checking choice...[1 1 0 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - - - ]
SPCHK: Adding unsat core to eliminated patterns...[- - - - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
       Notice that a specification accepts a don't care assignment. (this notice will be issued only twice).
  a[0:int] = 5:int, a[otherwise]= 5:int
  a.size_minus_1 = 1:int
  e = 5:int
  eina = (define true Bool)
  left = 0:int
  right = 0:int
  rv = 0:int
SPCHK: Answer by 'Y' to accept and 'N' to reject the assignment. 'M' to view more details from the solver about the model. 
USER > y
SPCHK: Adding vocab choice to the specification.
yes choice being added [1 1 0 1 1 ]
debug 6
debug 6
debug 7
SPCHK: checking choice...[1 1 0 1 0 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - - - ]
SPCHK: Adding unsat core to eliminated patterns...[- - - - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
       Notice that a specification accepts a don't care assignment. (this notice will be issued only twice).
  a[0:int] = 5:int, a[otherwise]= 5:int
  a.size_minus_1 = 1:int
  e = 5:int
  eina = (define false Bool)
  left = 0:int
  right = 0:int
  rv = 0:int
SPCHK: Answer by 'Y' to accept and 'N' to reject the assignment. 'M' to view more details from the solver about the model. 
USER > n
SPCHK: If the rejection is due to part of the assignment, 
       press 'V' to select the bad variables, 
       press 'B' to select the bad vocabulary values, or
       press 'C' to continue.
USER > ^C
Script done on Mon 16 Mar 2015 08:35:23 AM EET
