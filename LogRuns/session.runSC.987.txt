Script started on Wed 04 Feb 2015 02:59:05 PM EET
0x1e478b8 
0x1e478e0 
TheoryFiles/eina.ththeory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
SPCHK: warning: value `0' already declared.
Parsing Complete
SPCHK: formula 0: (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)).
SPCHK: formula 1: (and (<= left i) (<= i right)).
SPCHK: formula 2: (= e (select a i)).
3
name : TheoryFiles/eina.thSPCHK: building SMT formulae from vocab...
(benchmark SMT: extrafuns( (a (Array Int Int)) (a.size_minus_1 Int) (left Int) (right Int) (e Int) (i Int)) :formula (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) :formula ( not ( (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))) )  :formula (and (<= left i) (<= i right)) :formula ( not ( (and (<= left i) (<= i right))) )  :formula (= e (select a i)) :formula ( not ( (= e (select a i))) ) )
SPCHK: parsing SMT formulae...
SPCHK: made 7 declarations as follows.
SPCHK:   0; NAME:i; DCL:(define i Int); AST:1e95b28
SPCHK:   1; NAME:a; DCL:(define a (Array Int Int)); AST:1e95b68
SPCHK:   2; NAME:a.size_minus_1; DCL:(define a.size_minus_1 Int); AST:1e95b08
SPCHK:   4; NAME:left; DCL:(define left Int); AST:1e95ac8
SPCHK:   5; NAME:right; DCL:(define right Int); AST:1e95ae8
SPCHK:   6; NAME:e; DCL:(define e Int); AST:1e95b48
SPCHK: asserting 3 retractable formulas and their negations...
SPCHK:   formula 0: (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))
SPCHK:   formula 1: (and (<= left i) (<= i right))
SPCHK:   formula 2: (= e (select a i))
TheoryFiles/eina.thSPCHK: enumerating vocab evaluations using recursive traversal...
SPCHK: checking choice...[1 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
       Notice that a specification accepts a don't care assignment. (this notice will be issued only twice).
  a[0:int] = 4:int, a[otherwise]= 4:int
  a.size_minus_1 = 0:int
  e = 4:int
  left = 0:int
  right = 0:int
SPCHK: Answer by 'Y' to accept and 'N' to reject the assignment. 'M' to view more details from the solver about the model. 
USER > y
SPCHK: Adding vocab choice to the specification.
yes choice being added [1 1 1 ]
debug 6
debug 6
debug 7
SPCHK: checking choice...[1 1 0 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
       Notice that a specification accepts a don't care assignment. (this notice will be issued only twice).
  a[0:int] = 5:int, a[otherwise]= 5:int
  a.size_minus_1 = 0:int
  e = 4:int
  left = 0:int
  right = 0:int
SPCHK: Answer by 'Y' to accept and 'N' to reject the assignment. 'M' to view more details from the solver about the model. 
USER > n
SPCHK: If the rejection is due to part of the assignment, 
       press 'V' to select the bad variables, 
       press 'B' to select the bad vocabulary values, or
       press 'C' to continue.
USER > b
  + 0+ (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) is true 
  + 1+ (and (<= left i) (<= i right)) is true 
  + 2+ (= e (select a i)) is false 
SPCHK: Type the ids of the vocab formulae that are the reason for rejecting the satisfying example.Type '-1'  to finish, '-2' to restart, and '-3' to exit bad vocab formulae option.
USER > 2
USER > -1
SPCHK: The reason for rejecting the model is the partial assignment 
0
+ 2+ (= e (select a i)) is false 
SPCHK: please confirm by typing 'Y'. Restart by typing 'R'. Ignore and continue by typing any other key.
USER > y
SPCHK: ignore bad partial assignment: [- - 0 ]
SPCHK: checking choice...[1 0 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
  a[-1:int] = 5:int, a[otherwise]= 5:int
  a.size_minus_1 = 0:int
  e = 5:int
  left = 0:int
  right = 0:int
SPCHK: Answer by 'Y' to accept and 'N' to reject the assignment. 'M' to view more details from the solver about the model. 
USER > n
SPCHK: If the rejection is due to part of the assignment, 
       press 'V' to select the bad variables, 
       press 'B' to select the bad vocabulary values, or
       press 'C' to continue.
USER > b
  + 0+ (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) is true 
  + 1+ (and (<= left i) (<= i right)) is false 
  + 2+ (= e (select a i)) is true 
SPCHK: Type the ids of the vocab formulae that are the reason for rejecting the satisfying example.Type '-1'  to finish, '-2' to restart, and '-3' to exit bad vocab formulae option.
USER > 1
USER > -1
SPCHK: The reason for rejecting the model is the partial assignment 
0
+ 1+ (and (<= left i) (<= i right)) is false 
SPCHK: please confirm by typing 'Y'. Restart by typing 'R'. Ignore and continue by typing any other key.
USER > y
SPCHK: ignore bad partial assignment: [- 0 - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 0 0 ]
SPCHK: checking choice...[0 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
  a[-1:int] = 3:int, a[otherwise]= 3:int
  a.size_minus_1 = (define a.size_minus_1 Int)
  e = 3:int
  left = -1:int
  right = 0:int
SPCHK: Answer by 'Y' to accept and 'N' to reject the assignment. 'M' to view more details from the solver about the model. 
USER > n
SPCHK: If the rejection is due to part of the assignment, 
       press 'V' to select the bad variables, 
       press 'B' to select the bad vocabulary values, or
       press 'C' to continue.
USER > b
  + 0+ (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) is false 
  + 1+ (and (<= left i) (<= i right)) is true 
  + 2+ (= e (select a i)) is true 
SPCHK: Type the ids of the vocab formulae that are the reason for rejecting the satisfying example.Type '-1'  to finish, '-2' to restart, and '-3' to exit bad vocab formulae option.
USER > 0
USER > -1
SPCHK: The reason for rejecting the model is the partial assignment 
0
+ 0+ (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) is false 
SPCHK: please confirm by typing 'Y'. Restart by typing 'R'. Ignore and continue by typing any other key.
USER > y
SPCHK: ignore bad partial assignment: [0 - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[0 1 0 ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[0 0 - ]
SPCHK: the formula is:
(or false
    (and (and (and true
                   (and (and (<= 0 left) (<= left right))
                        (<= right a.size_minus_1)))
              (and (<= left i) (<= i right)))
         (= e (select a i))))

SPCHK: Timing and statistics report: 
       number of vocab clauses: 3
       number of variables: 6

       number of queries to user: 4
       number of vocab based partial assignments: 3
       number of variable based partial assignments: 0
       number of unsat cores: 3
       number of calls to sat solver (Z3): 4

       total traverse time (seconds): 28.1191
       total sat time (micorseconds): 1660
       total match unsat core time (micorseconds): 4
SPCHK: Calling logic minimization (Espresso) to simplify formula.
SPCHK: issuing command ./espresso/espresso -Dsimplify -oeqntott yes.1019.tt > yes.1019eqntott.simplified
SPCHK: issuing command ./espresso/espresso -Dsimplify -okiss yes.1019.tt > yes.1019kiss.simplified
SPCHK: output from espresso
# truth table from specification construction
Spec = (v0&v1&v2);

SPCHK: formula after esprersso simplify.
  Spec = exists i.  ((((0 <= left) and (left <= right)) and (right <= a.size_minus_1))&((left <= i) and (i <= right))&(e = a[i]));



Formula 1Spec = exists i.  ((((0 <= left) and (left <= right)) and (right <= a.size_minus_1))&((left <= i) and (i <= right))&(e = a[i]));



SPCHK: SMT formula after esprersso simplify.
  :formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
Formula 2:formula (or 
   (and (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) (and (<= left i) (<= i right)) (= e (select a i)) ) 
   ) 
SPCHK: using abc for simplification.
SPCHK: abc command is read_blif yes.1019.abc.blif
balance
renode
fx
strash
refactor
refactor
collapse
balance
collapse
write_eqn yes.1019.abc.simplify

ABC command line: "source -x abccmd".

abc - > read_blif yes.1019.abc.blif
abc - > balance
abc - > renode
abc - > fx
Warning: The network has not been changed by "fx".
abc - > strash
abc - > refactor
abc - > refactor
abc - > collapse
abc - > balance
abc - > collapse
abc - > write_eqn yes.1019.abc.simplify
SPCHK: simplified abc formula is: 

# Equations for "yes.1019" written by ABC on Wed Feb  4 14:59:33 2015
INORDER = v0 v1 v2;
OUTORDER = Spec;
Spec = v0 * v1 * v2;

SPCHK: formula after esprersso simplify.
  INORDER = (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) ((left <= i) and (i <= right)) (e = a[i]);
OUTORDER = Spec;
Spec = exists i.  (((0 <= left) and (left <= right)) and (right <= a.size_minus_1)) * ((left <= i) and (i <= right)) * (e = a[i]);




Script done on Wed 04 Feb 2015 02:59:33 PM EET
