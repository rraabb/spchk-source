Script started on Thu 19 Mar 2015 12:51:12 AM EET
0x6e7b08 
0x6ea530 
TheoryFiles/lse.ththeory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
SPCHK: warning: value `0' already declared.
SPCHK: warning: value `1' already declared.
Parsing Complete
SPCHK: formula 0: (and (and (<= 0 left) (<= left right)) (< right a.size_minus_1)).
SPCHK: formula 1: (and (<= left rv) (<= rv right)).
SPCHK: formula 2: (= rv (- 1)).
SPCHK: formula 3: (= e (select a rv)).
SPCHK: formula 4: eina.
5
name : TheoryFiles/lse.thSPCHK: building SMT formulae from vocab...
(benchmark SMT: extrafuns( (a (Array Int Int)) (a.size_minus_1 Int) (left Int) (right Int) (e Int) (rv Int) (eina Bool)) :formula (and (and (<= 0 left) (<= left right)) (< right a.size_minus_1)) :formula ( not ( (and (and (<= 0 left) (<= left right)) (< right a.size_minus_1))) )  :formula (and (<= left rv) (<= rv right)) :formula ( not ( (and (<= left rv) (<= rv right))) )  :formula (= rv (- 1)) :formula ( not ( (= rv (- 1))) )  :formula (= e (select a rv)) :formula ( not ( (= e (select a rv))) )  :formula eina :formula ( not ( eina) ) )
SPCHK: parsing SMT formulae...
SPCHK: made 8 declarations as follows.
SPCHK:   0; NAME:a; DCL:(define a (Array Int Int)); AST:70ee28
SPCHK:   1; NAME:a.size_minus_1; DCL:(define a.size_minus_1 Int); AST:70eda8
SPCHK:   3; NAME:left; DCL:(define left Int); AST:70ed68
SPCHK:   4; NAME:right; DCL:(define right Int); AST:70ed88
SPCHK:   5; NAME:rv; DCL:(define rv Int); AST:70edc8
SPCHK:   6; NAME:e; DCL:(define e Int); AST:70ee08
SPCHK:   7; NAME:eina; DCL:(define eina Bool); AST:70ee48
SPCHK: asserting 5 retractable formulas and their negations...
SPCHK:   formula 0: (and (and (<= 0 left) (<= left right)) (< right a.size_minus_1))
SPCHK:   formula 1: (and (<= left rv) (<= rv right))
SPCHK:   formula 2: (= rv (- 1))
SPCHK:   formula 3: (= e (select a rv))
SPCHK:   formula 4: eina
TheoryFiles/lse.thSPCHK: enumerating vocab evaluations using recursive traversal...
SPCHK: checking choice...[1 1 1 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - - - ]
SPCHK: Adding unsat core to eliminated patterns...[1 1 1 - - ]
SPCHK: choice is not satisfiable. 
SPCHK: Adding unsat core to eliminated patterns...[1 1 1 - - ]
SPCHK: Adding unsat core to eliminated patterns...[1 1 1 - - ]
SPCHK: Adding unsat core to eliminated patterns...[1 1 1 - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 0 ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 0 - ]
SPCHK: checking choice...[1 1 0 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - - - ]
SPCHK: Adding unsat core to eliminated patterns...[- - - - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
       Notice that a specification accepts a don't care assignment. (this notice will be issued only twice).
  a[0:int] = 5:int, a[otherwise]= 5:int
  a.size_minus_1 = 1:int
  e = 5:int
  eina = (define true Bool)
  left = 0:int
  right = 0:int
  rv = 0:int
SPCHK: Answer by 'Y' to accept and 'N' to reject the assignment. 'M' to view more details from the solver about the model. 
USER > ^[[A^[[A^Z^C
Script done on Thu 19 Mar 2015 12:51:14 AM EET
