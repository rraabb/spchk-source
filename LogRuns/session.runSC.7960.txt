Script started on Wed 18 Mar 2015 02:59:43 PM EET
0x1ab0b08 
0x1ab3530 
TheoryFiles/ls.ththeory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
Parsing Complete
generating vocab
SPCHK: warning: value `1' already declared.
SPCHK: formula 0: (<= 0 left).
SPCHK: formula 1: (<= 0 right).
SPCHK: formula 2: (<= 0 rv).
SPCHK: formula 3: (<= 0 i).
SPCHK: formula 4: (<= 0 (+ 1 i)).
SPCHK: formula 5: (= left 0).
SPCHK: formula 6: (<= left a.size_minus_1).
SPCHK: formula 7: (= a.size_minus_1 left).
SPCHK: formula 8: (= right 0).
SPCHK: formula 9: (<= right a.size_minus_1).
SPCHK: formula 10: (= a.size_minus_1 right).
SPCHK: formula 11: (<= right left).
SPCHK: formula 12: (= left right).
SPCHK: formula 13: (= e (select a rv)).
SPCHK: formula 14: (= e (select a i)).
SPCHK: formula 15: (= e (select a (+ 1 i))).
SPCHK: formula 16: (= rv 0).
SPCHK: formula 17: (= rv -1).
SPCHK: formula 18: (<= rv a.size_minus_1).
SPCHK: formula 19: (= a.size_minus_1 rv).
SPCHK: formula 20: (<= rv left).
SPCHK: formula 21: (= left rv).
SPCHK: formula 22: (<= rv right).
SPCHK: formula 23: (= right rv).
SPCHK: formula 24: (= 0 i).
SPCHK: formula 25: (<= i a.size_minus_1).
SPCHK: formula 26: (= a.size_minus_1 i).
SPCHK: formula 27: (<= i left).
SPCHK: formula 28: (= left i).
SPCHK: formula 29: (<= i right).
SPCHK: formula 30: (= right i).
SPCHK: formula 31: (<= i rv).
SPCHK: formula 32: (= rv i).
Start QTMAin 
"loading file to GUI :TheoryFiles/ls.th" 
0x174db08 
0x1750530 
SPCHK: warning: value `1' already declared.
"file set is : TheoryFiles/ls.th" 
0x1e49ca8 
0x1fb77f0 
theory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
Parsing Complete
reloading grammar
  (a,rv,[])
  (left,a.size_minus_1,<=,=)
  (left,lit(0),=)
  (right,a.size_minus_1,<=,=)
  (right,left,<=,=)
  (right,lit(0),=)
  (e,a,=)
  (rv,a.size_minus_1,<=,=)
  (rv,left,<=,=)
  (rv,right,<=,=)
  (rv,lit(0),=)
  (rv,lit(-1),=)
  (lit(0),left,<=)
  (lit(0),right,<=)
  (lit(0),rv,<=)
  (a,rv,[])
  (left,a.size_minus_1,<=,=)
  (left,lit(0),=)
  (right,a.size_minus_1,<=,=)
  (right,left,<=,=)
  (right,lit(0),=)
  (e,a,=)
  (rv,a.size_minus_1,<=,=)
  (rv,left,<=,=)
  (rv,right,<=,=)
  (rv,lit(0),=)
  (rv,lit(-1),=)
  (lit(0),left,<=)
  (lit(0),right,<=)
  (lit(0),rv,<=)
Reloading done
6:0
7:1
8:-1
placing vocab in GUI
reloading vocab 

debug 4 

debug 1 

vocab loading over
done placing vocab in GUI
0x1e49ca8 
0x2137b00 
theory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
Parsing Complete
creating vocab 
generating vocab
SPCHK: warning: value `1' already declared.
SPCHK: formula 0: (<= 0 left).
SPCHK: formula 1: (<= 0 right).
SPCHK: formula 2: (<= 0 rv).
SPCHK: formula 3: (<= 0 i).
SPCHK: formula 4: (<= 0 (+ 1 i)).
SPCHK: formula 5: (= left 0).
SPCHK: formula 6: (<= left a.size_minus_1).
SPCHK: formula 7: (= a.size_minus_1 left).
SPCHK: formula 8: (= right 0).
SPCHK: formula 9: (<= right a.size_minus_1).
SPCHK: formula 10: (= a.size_minus_1 right).
SPCHK: formula 11: (<= right left).
SPCHK: formula 12: (= left right).
SPCHK: formula 13: (= e (select a rv)).
SPCHK: formula 14: (= e (select a i)).
SPCHK: formula 15: (= e (select a (+ 1 i))).
SPCHK: formula 16: (= rv 0).
SPCHK: formula 17: (= rv -1).
SPCHK: formula 18: (<= rv a.size_minus_1).
SPCHK: formula 19: (= a.size_minus_1 rv).
SPCHK: formula 20: (<= rv left).
SPCHK: formula 21: (= left rv).
SPCHK: formula 22: (<= rv right).
SPCHK: formula 23: (= right rv).
SPCHK: formula 24: (= 0 i).
SPCHK: formula 25: (<= i a.size_minus_1).
SPCHK: formula 26: (= a.size_minus_1 i).
SPCHK: formula 27: (<= i left).
SPCHK: formula 28: (= left i).
SPCHK: formula 29: (<= i right).
SPCHK: formula 30: (= right i).
SPCHK: formula 31: (<= i rv).
SPCHK: formula 32: (= rv i).
reloading vocab 

debug 4 

debug 1 

debug loop  0 

debug post to infix on (<= 0 left)
"(0 <= left)" 

Popping out elements...debug post to infix on (<= 0 left)
Popping out elements...debug post to infix on (<= 0 left)
debug loop  1 

Popping out elements...debug post to infix on (<= 0 right)
"(0 <= right)" 

Popping out elements...debug post to infix on (<= 0 right)
Popping out elements...debug post to infix on (<= 0 right)
debug loop  2 

Popping out elements...debug post to infix on (<= 0 rv)
"(0 <= rv)" 

Popping out elements...debug post to infix on (<= 0 rv)
Popping out elements...debug post to infix on (<= 0 rv)
debug loop  3 

Popping out elements...debug post to infix on (<= 0 i)
"(0 <= i)" 

Popping out elements...debug post to infix on (<= 0 i)
Popping out elements...debug post to infix on (<= 0 i)
debug loop  4 

Popping out elements...debug post to infix on (<= 0 (+ 1 i))
"(0 <= (1 + i))" 

Popping out elements...debug post to infix on (<= 0 (+ 1 i))
Popping out elements...debug post to infix on (<= 0 (+ 1 i))
debug loop  5 

Popping out elements...debug post to infix on (= left 0)
"(left = 0)" 

Popping out elements...debug post to infix on (= left 0)
Popping out elements...debug post to infix on (= left 0)
debug loop  6 

Popping out elements...debug post to infix on (<= left a.size_minus_1)
"(left <= a.size_minus_1)" 

Popping out elements...debug post to infix on (<= left a.size_minus_1)
Popping out elements...debug post to infix on (<= left a.size_minus_1)
debug loop  7 

Popping out elements...debug post to infix on (= a.size_minus_1 left)
"(a.size_minus_1 = left)" 

Popping out elements...debug post to infix on (= a.size_minus_1 left)
Popping out elements...debug post to infix on (= a.size_minus_1 left)
debug loop  8 

Popping out elements...debug post to infix on (= right 0)
"(right = 0)" 

Popping out elements...debug post to infix on (= right 0)
Popping out elements...debug post to infix on (= right 0)
debug loop  9 

Popping out elements...debug post to infix on (<= right a.size_minus_1)
"(right <= a.size_minus_1)" 

Popping out elements...debug post to infix on (<= right a.size_minus_1)
Popping out elements...debug post to infix on (<= right a.size_minus_1)
debug loop  10 

Popping out elements...debug post to infix on (= a.size_minus_1 right)
"(a.size_minus_1 = right)" 

Popping out elements...debug post to infix on (= a.size_minus_1 right)
Popping out elements...debug post to infix on (= a.size_minus_1 right)
debug loop  11 

Popping out elements...debug post to infix on (<= right left)
"(right <= left)" 

Popping out elements...debug post to infix on (<= right left)
Popping out elements...debug post to infix on (<= right left)
debug loop  12 

Popping out elements...debug post to infix on (= left right)
"(left = right)" 

Popping out elements...debug post to infix on (= left right)
Popping out elements...debug post to infix on (= left right)
debug loop  13 

Popping out elements...debug post to infix on (= e (select a rv))
"(e = a[rv])" 

Popping out elements...debug post to infix on (= e (select a rv))
Popping out elements...debug post to infix on (= e (select a rv))
debug loop  14 

Popping out elements...debug post to infix on (= e (select a i))
"(e = a[i])" 

Popping out elements...debug post to infix on (= e (select a i))
Popping out elements...debug post to infix on (= e (select a i))
debug loop  15 

Popping out elements...debug post to infix on (= e (select a (+ 1 i)))
"(e = a[(1 + i)])" 

Popping out elements...debug post to infix on (= e (select a (+ 1 i)))
Popping out elements...debug post to infix on (= e (select a (+ 1 i)))
debug loop  16 

Popping out elements...debug post to infix on (= rv 0)
"(rv = 0)" 

Popping out elements...debug post to infix on (= rv 0)
Popping out elements...debug post to infix on (= rv 0)
debug loop  17 

Popping out elements...debug post to infix on (= rv -1)
"(rv = -1)" 

Popping out elements...debug post to infix on (= rv -1)
Popping out elements...debug post to infix on (= rv -1)
debug loop  18 

Popping out elements...debug post to infix on (<= rv a.size_minus_1)
"(rv <= a.size_minus_1)" 

Popping out elements...debug post to infix on (<= rv a.size_minus_1)
Popping out elements...debug post to infix on (<= rv a.size_minus_1)
debug loop  19 

Popping out elements...debug post to infix on (= a.size_minus_1 rv)
"(a.size_minus_1 = rv)" 

Popping out elements...debug post to infix on (= a.size_minus_1 rv)
Popping out elements...debug post to infix on (= a.size_minus_1 rv)
debug loop  20 

Popping out elements...debug post to infix on (<= rv left)
"(rv <= left)" 

Popping out elements...debug post to infix on (<= rv left)
Popping out elements...debug post to infix on (<= rv left)
debug loop  21 

Popping out elements...debug post to infix on (= left rv)
"(left = rv)" 

Popping out elements...debug post to infix on (= left rv)
Popping out elements...debug post to infix on (= left rv)
debug loop  22 

Popping out elements...debug post to infix on (<= rv right)
"(rv <= right)" 

Popping out elements...debug post to infix on (<= rv right)
Popping out elements...debug post to infix on (<= rv right)
debug loop  23 

Popping out elements...debug post to infix on (= right rv)
"(right = rv)" 

Popping out elements...debug post to infix on (= right rv)
Popping out elements...debug post to infix on (= right rv)
debug loop  24 

Popping out elements...debug post to infix on (= 0 i)
"(0 = i)" 

Popping out elements...debug post to infix on (= 0 i)
Popping out elements...debug post to infix on (= 0 i)
debug loop  25 

Popping out elements...debug post to infix on (<= i a.size_minus_1)
"(i <= a.size_minus_1)" 

Popping out elements...debug post to infix on (<= i a.size_minus_1)
Popping out elements...debug post to infix on (<= i a.size_minus_1)
debug loop  26 

Popping out elements...debug post to infix on (= a.size_minus_1 i)
"(a.size_minus_1 = i)" 

Popping out elements...debug post to infix on (= a.size_minus_1 i)
Popping out elements...debug post to infix on (= a.size_minus_1 i)
debug loop  27 

Popping out elements...debug post to infix on (<= i left)
"(i <= left)" 

Popping out elements...debug post to infix on (<= i left)
Popping out elements...debug post to infix on (<= i left)
debug loop  28 

Popping out elements...debug post to infix on (= left i)
"(left = i)" 

Popping out elements...debug post to infix on (= left i)
Popping out elements...debug post to infix on (= left i)
debug loop  29 

Popping out elements...debug post to infix on (<= i right)
"(i <= right)" 

Popping out elements...debug post to infix on (<= i right)
Popping out elements...debug post to infix on (<= i right)
debug loop  30 

Popping out elements...debug post to infix on (= right i)
"(right = i)" 

Popping out elements...debug post to infix on (= right i)
Popping out elements...debug post to infix on (= right i)
debug loop  31 

Popping out elements...debug post to infix on (<= i rv)
"(i <= rv)" 

Popping out elements...debug post to infix on (<= i rv)
Popping out elements...debug post to infix on (<= i rv)
debug loop  32 

Popping out elements...debug post to infix on (= rv i)
"(rv = i)" 

Popping out elements...debug post to infix on (= rv i)
Popping out elements...debug post to infix on (= rv i)
Popping out elements...vocab loading over
SPCHK: building SMT formulae from vocab...
(benchmark SMT: extrafuns( (a (Array Int Int)) (a.size_minus_1 Int) (left Int) (right Int) (e Int) (rv Int) (i Int)) :formula (<= 0 left) :formula ( not ( (<= 0 left)) )  :formula (<= 0 right) :formula ( not ( (<= 0 right)) )  :formula (<= 0 rv) :formula ( not ( (<= 0 rv)) )  :formula (<= 0 i) :formula ( not ( (<= 0 i)) )  :formula (<= 0 (+ 1 i)) :formula ( not ( (<= 0 (+ 1 i))) )  :formula (= left 0) :formula ( not ( (= left 0)) )  :formula (<= left a.size_minus_1) :formula ( not ( (<= left a.size_minus_1)) )  :formula (= a.size_minus_1 left) :formula ( not ( (= a.size_minus_1 left)) )  :formula (= right 0) :formula ( not ( (= right 0)) )  :formula (<= right a.size_minus_1) :formula ( not ( (<= right a.size_minus_1)) )  :formula (= a.size_minus_1 right) :formula ( not ( (= a.size_minus_1 right)) )  :formula (<= right left) :formula ( not ( (<= right left)) )  :formula (= left right) :formula ( not ( (= left right)) )  :formula (= e (select a rv)) :formula ( not ( (= e (select a rv))) )  :formula (= e (select a i)) :formula ( not ( (= e (select a i))) )  :formula (= e (select a (+ 1 i))) :formula ( not ( (= e (select a (+ 1 i)))) )  :formula (= rv 0) :formula ( not ( (= rv 0)) )  :formula (= rv -1) :formula ( not ( (= rv -1)) )  :formula (<= rv a.size_minus_1) :formula ( not ( (<= rv a.size_minus_1)) )  :formula (= a.size_minus_1 rv) :formula ( not ( (= a.size_minus_1 rv)) )  :formula (<= rv left) :formula ( not ( (<= rv left)) )  :formula (= left rv) :formula ( not ( (= left rv)) )  :formula (<= rv right) :formula ( not ( (<= rv right)) )  :formula (= right rv) :formula ( not ( (= right rv)) )  :formula (= 0 i) :formula ( not ( (= 0 i)) )  :formula (<= i a.size_minus_1) :formula ( not ( (<= i a.size_minus_1)) )  :formula (= a.size_minus_1 i) :formula ( not ( (= a.size_minus_1 i)) )  :formula (<= i left) :formula ( not ( (<= i left)) )  :formula (= left i) :formula ( not ( (= left i)) )  :formula (<= i right) :formula ( not ( (<= i right)) )  :formula (= right i) :formula ( not ( (= right i)) )  :formula (<= i rv) :formula ( not ( (<= i rv)) )  :formula (= rv i) :formula ( not ( (= rv i)) ) )
SPCHK: parsing SMT formulae...
done building
SPCHK: made 8 declarations as follows.
SPCHK:   0; NAME:i; DCL:(define i Int); AST:20c0368
SPCHK:   1; NAME:a; DCL:(define a (Array Int Int)); AST:20c03e8
SPCHK:   2; NAME:a.size_minus_1; DCL:(define a.size_minus_1 Int); AST:20c03a8
SPCHK:   4; NAME:left; DCL:(define left Int); AST:20c0308
SPCHK:   5; NAME:right; DCL:(define right Int); AST:20c0328
SPCHK:   6; NAME:rv; DCL:(define rv Int); AST:20c0348
SPCHK:   7; NAME:e; DCL:(define e Int); AST:20c03c8
done printing AST
SPCHK: asserting 33 retractable formulas and their negations...
SPCHK:   formula 0: (<= 0 left)
SPCHK:   formula 1: (<= 0 right)
SPCHK:   formula 2: (<= 0 rv)
SPCHK:   formula 3: (<= 0 i)
SPCHK:   formula 4: (<= 0 (+ 1 i))
SPCHK:   formula 5: (= left 0)
SPCHK:   formula 6: (<= left a.size_minus_1)
SPCHK:   formula 7: (= a.size_minus_1 left)
SPCHK:   formula 8: (= right 0)
SPCHK:   formula 9: (<= right a.size_minus_1)
SPCHK:   formula 10: (= a.size_minus_1 right)
SPCHK:   formula 11: (<= right left)
SPCHK:   formula 12: (= left right)
SPCHK:   formula 13: (= e (select a rv))
SPCHK:   formula 14: (= e (select a i))
SPCHK:   formula 15: (= e (select a (+ 1 i)))
SPCHK:   formula 16: (= rv 0)
SPCHK:   formula 17: (= rv -1)
SPCHK:   formula 18: (<= rv a.size_minus_1)
SPCHK:   formula 19: (= a.size_minus_1 rv)
SPCHK:   formula 20: (<= rv left)
SPCHK:   formula 21: (= left rv)
SPCHK:   formula 22: (<= rv right)
SPCHK:   formula 23: (= right rv)
SPCHK:   formula 24: (= 0 i)
SPCHK:   formula 25: (<= i a.size_minus_1)
SPCHK:   formula 26: (= a.size_minus_1 i)
SPCHK:   formula 27: (<= i left)
SPCHK:   formula 28: (= left i)
SPCHK:   formula 29: (<= i right)
SPCHK:   formula 30: (= right i)
SPCHK:   formula 31: (<= i rv)
SPCHK:   formula 32: (= rv i)
0
choice set to: [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
StopChoice: [- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ]
SPCHK: checking choice...[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ]
abcbotato
SPCHK: Adding unsat core to eliminated patterns...[- - 1 - - - - - - - - - - - - - - 1 - - - - - - - - - - - - - - - ]
SPCHK: choice is not satisfiable. 
SPCHK: Adding unsat core to eliminated patterns...SPCHK: Adding unsat core to eliminated patterns...[- - 1 - - - - - - - - - - - - - - 1 - - - - - - - - - - - - - - - ]
32
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - - - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - - - - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - - - - - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - - - - - - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - - - - - - - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - - - - - - - - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - - - - - - - - - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - - - - - - - - - - - ]
SPCHK: Ignore: subtree satisfies an eliminated pattern.[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 - - - - - - - - - - - - - - ]
choice set to: [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
SPCHK: checking choice...[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ]
abcbotato
SPCHK: Adding unsat core to eliminated patterns...[- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
       Notice that a specification accepts a don't care assignment. (this notice will be issued only twice).
                                                                                                                                                       a[1:int] = 4:int, a[0:int] = 4:int, a[otherwise]= 4:int

a.size_minus_1 = 0:int

e = 4:int

i = 0:int

left = 0:int

right = 0:int

rv = 0:int

GUI:couldn't find variable in GUI!
construct: displayed vector is: [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
yes Choices 102
No Choices
Choice: 
[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
####################################################
:2:1
2
:2:1
:17:1
17
:2:1:17:1

####################################################
Updating Undo List
[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]
Updating Values

Script done on Wed 18 Mar 2015 03:00:09 PM EET
