Script started on Mon 16 Mar 2015 08:25:33 AM EET
0x213bb08 
0x213e530 
TheoryFiles/eina.ththeory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
SPCHK: warning: variable `a.size_minus_1' already declared. ignoring second declaration.
SPCHK: warning: value `0' already declared.
Parsing Complete
SPCHK: formula 0: (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)).
SPCHK: formula 1: (and (<= left i) (<= i right)).
SPCHK: formula 2: (= e (select a i)).
Start QTMAin 
"loading file to GUI :TheoryFiles/eina.th" 
0x19e9b08 
0x19ec530 
SPCHK: warning: variable `a.size_minus_1' already declared. ignoring second declaration.
SPCHK: warning: value `0' already declared.
"file set is : TheoryFiles/eina.th" 
0x24d3c98 
0x23c5d20 
theory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
SPCHK: warning: variable `a.size_minus_1' already declared. ignoring second declaration.
SPCHK: warning: value `0' already declared.
Parsing Complete
SPCHK: formula 0: (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)).
SPCHK: formula 1: (and (<= left i) (<= i right)).
SPCHK: formula 2: (= e (select a i)).
reloading grammar
  (a,i,[])
  (left,a.size_minus_1,<=,=)
  (left,lit(0),=,>)
  (right,a.size_minus_1,<=,=)
  (right,left,<=,=)
  (right,lit(0),=,>)
  (e,a,=)
  (i,a.size_minus_1,<=,=)
  (i,left,<=,=)
  (i,right,<=,=)
  (i,lit(0),=,>)
  (a,i,[])
  (left,a.size_minus_1,<=,=)
  (left,lit(0),=,>)
  (right,a.size_minus_1,<=,=)
  (right,left,<=,=)
  (right,lit(0),=,>)
  (e,a,=)
  (i,a.size_minus_1,<=,=)
  (i,left,<=,=)
  (i,right,<=,=)
  (i,lit(0),=,>)
Reloading done
6:0
7:1
8:-1
reloading vocab 

Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...vocab loading over
0x24d3c98 
0x281f520 
theory  is ls
SPCHK: array declaration: declaring a size - 1 variable `a.size_minus_1' associated with the array.
SPCHK: warning: variable `a.size_minus_1' already declared. ignoring second declaration.
SPCHK: warning: value `0' already declared.
Parsing Complete
creating vocab 
SPCHK: formula 0: (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)).
SPCHK: formula 1: (and (<= left i) (<= i right)).
SPCHK: formula 2: (= e (select a i)).
reloading vocab 

Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...Popping out elements...vocab loading over
SPCHK: building SMT formulae from vocab...
(benchmark SMT: extrafuns( (a (Array Int Int)) (a.size_minus_1 Int) (left Int) (right Int) (e Int) (i Int)) :formula (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1)) :formula ( not ( (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))) )  :formula (and (<= left i) (<= i right)) :formula ( not ( (and (<= left i) (<= i right))) )  :formula (= e (select a i)) :formula ( not ( (= e (select a i))) ) )
SPCHK: parsing SMT formulae...
done building
SPCHK: made 7 declarations as follows.
SPCHK:   0; NAME:i; DCL:(define i Int); AST:27ef398
SPCHK:   1; NAME:a; DCL:(define a (Array Int Int)); AST:27ef3d8
SPCHK:   2; NAME:a.size_minus_1; DCL:(define a.size_minus_1 Int); AST:27ef378
SPCHK:   4; NAME:left; DCL:(define left Int); AST:27ef338
SPCHK:   5; NAME:right; DCL:(define right Int); AST:27ef358
SPCHK:   6; NAME:e; DCL:(define e Int); AST:27ef3b8
done printing AST
SPCHK: asserting 3 retractable formulas and their negations...
SPCHK:   formula 0: (and (and (<= 0 left) (<= left right)) (<= right a.size_minus_1))
SPCHK:   formula 1: (and (<= left i) (<= i right))
SPCHK:   formula 2: (= e (select a i))
0
choice set to: [1 1 1 ]
StopChoice: [- - - ]
SPCHK: checking choice...[1 1 1 ]
SPCHK: Calling SMT Solver. This may take time..
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
abcbotato
SPCHK: Adding unsat core to eliminated patterns...[- - - ]
SPCHK: choice is satisfiable. 
       Is the satisfying assignment below a good model for your property?
       Notice that a specification accepts a don't care assignment. (this notice will be issued only twice).
                                                                                                                        a[0:int] = 4:int, a[otherwise]= 4:int

a.size_minus_1 = 0:int

e = 4:int

i = 0:int

left = 0:int

right = 0:int

construct: displayed vector is: [1 1 1 ]
yes Choices 102
No Choices
Choice: 
[1 1 1 ]
####################################################
####################################################
Updating Undo List
[1 1 1 ]
Updating Values
yes Choices  101
No Choices

Script done on Mon 16 Mar 2015 08:25:52 AM EET
